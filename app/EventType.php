<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EventType extends Model
{
  use SoftDeletes;

  protected $fillable = ['name', 'icon', 'service_id'];
  protected $table = 'eventTypes';

  public function posts(){
      return $this->hasMany("App\Post","eventtype_id");
  }

  public function service(){
      return $this->belongsTo("App\Service","service_id");
  }
}
