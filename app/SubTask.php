<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubTask extends Model
{
    protected $fillable = ['title', 'tasks_id'];
    protected $table = "subTasks";

    public function task(){
      return $this->belongsTo("App\Task", "tasks_id");
    }
}
