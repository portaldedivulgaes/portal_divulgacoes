<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Service extends Model
{
  use Notifiable;
    protected $table = 'services';

    // name, initial, description, email, phone
    protected $fillable = ['name', 'initial', 'description', 'email', 'phone' ];

    public function eventTypes(){
      return $this->hasMany("App\EventType", "service_id");
    }


    public function newsletters(){
        return $this->hasMany("App\Newsletter", "service_id");
    }

    public function posts(){
        return $this->hasMany("App\Post", "service_id");
    }



    public function serviceToStr()
    {
        if ($this->service_id == null){
            return 'Não atribuido';
        }else{
            $initial = SERVICE::where('id', $this->service_id)->first()->initial;
            return $initial;
        }
    }


}
