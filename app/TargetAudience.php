<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TargetAudience extends Model
{
    use SoftDeletes;
    protected $fillable = [ 'name', 'email' ];
    protected $table = 'targetAudiences';
}
