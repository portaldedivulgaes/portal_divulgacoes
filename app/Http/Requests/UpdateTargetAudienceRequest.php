<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTargetAudienceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
      return [
          "name" => 'required|max:150',
          "email" => 'required|email|unique:targetAudiences,email,'.$this->id.',id,deleted_at,NULL'
      ];
    }

    public function messages()
    {
        return [
            'name.required' => 'A Designação é um campo obrigatório de ser preenchido.',
            'email.required' => 'O Email é obrigatório ser preenchido.',
            'email.email' => 'O campo "Email" deve ser um email válido',
            'email.unique' => 'O email <b>'.$this->email.'</b> já se encontra registado'
        ];
    }
}
