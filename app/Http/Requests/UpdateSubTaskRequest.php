<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSubTaskRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
          "title" => 'required',
          "tasks_id" => 'required'
        ];
    }

    public function messages()
    {
        return [
            'title.required' => 'O Título é necessário ser preenchido.',
            'tasks_id.required' => 'O Número de Indetificação do item têm que ser preenchido.'
        ];
    }
}
