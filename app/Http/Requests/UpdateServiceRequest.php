<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // name    initial     description     email   phone

            'name' => 'required|max:200',
            'initial' =>'required|max:20',
            'description' =>'required|max:150',
            'email' =>'required|max:250',
            'phone'=>'nullable|max:15'

        ];
    }

    public function messages(){
        return [
            'name.required' => 'Insira um nome de gabinete válido',
            'initial.required' => 'Insira iniciais válidas',
            'description.required' => 'Insira uma descrição para o gabinete',
            'description.max' => 'A descrição de gabinete tem de ser mais pequena',
            'email.required' => 'Insira um endereço de e-mail válido'
        ];
    }


}
