<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreNewsletterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "subject" => 'required',
            "selectedPosts" => 'required|min:1',
            "selectedPosts.*" => 'required|exists:posts,id',
            "targetaudience" => 'required|min:1',
            "targetaudience.*" => 'required|exists:targetaudiences,email',
        ];
    }

    public function messages()
    {
        return [
            "subject.required" => 'Assunto não preenchido!',
            "targetaudience.required" => 'Email(s) não preenchido!',
            "targetaudience.*.exists" => 'Email(s) inválido(s)!',
            "selectedPosts.required" => 'Post(s) não preenchido!'
        ];
    }
}
