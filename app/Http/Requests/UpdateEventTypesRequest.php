<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateEventTypesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => 'required|max:150|unique:eventTypes,name,'.$this->id.',id,deleted_at,NULL,service_id,'.$this->service_id,
            "icon" => 'required|max:50',
            "service_id" => 'required|exists:services,id',
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'A Designação é um campo obrigatório de ser preenchido.',
            'name.unique' => 'O Tipo de Divulgação <b>'.$this->name.'</b> já existe.',
            'icon.required' => 'O Ícone é obrigatório ser preenchido.'
        ];
    }
}
