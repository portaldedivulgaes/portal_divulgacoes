<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "eventType" => 'required|exists:eventTypes,id',
            "title" => 'required|max:150',
            "type" => 'required',
            "validity" => 'nullable|date|after_or_equal:today',
            "startDate" => 'nullable|date|after_or_equal:today',
            "endDate" => 'nullable|date|after_or_equal:startDate',
            "image" => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            "poster" => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            "location" => 'nullable',
            "important" => 'required|in:0,1',
            "resume" => 'required|max:150',
            "tags" => 'nullable',
            "description" => 'required|min:3',
            "link" => 'nullable|url',
        ];
    }

    public function messages()
    {
        // $messages = parent::messages();
        // $messages['title.regex'] = 'Title should contain only letters and spaces';
        // return $messages;

        return [
            'title.required' => 'O Título é obrigatório ser preenchido.',
            'eventType.required' => 'O Tipo de Evento é obrigatório ser preenchido.',
            'eventType.exists'=> 'O Tipo de Evento é obrigatório ser preenchido.',
            'validity.date' => 'A Validade tem de ser preenchida desta forma"Y-m-d H:i:s"',
            'resume.required' => 'O Texto Curto é obrigatório ser preenchido.',
            'description.required' => 'A Descrição é obrigatória ser preenchida.',
            'link.url' => 'O Link tem de ser preenchido com um URL.',
            'validity.after_or_equal' => 'A Validade deve ser uma data posterior ou igual à de Hoje.',
            'startDate.after_or_equal' => 'A Data de Início do Evento deve ser uma data posterior ou igual à de Hoje.',
            'endDate.after_or_equal' => 'A Data de fim do Evento deve ser uma data posterior ou igual à Data de Início.'
        ];
    }

}
