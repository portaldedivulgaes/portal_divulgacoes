<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
    * Determine if the user is authorized to make this request.
    *
    * @return bool
    */
    public function authorize()
    {
        return true;
    }

    /**
    * Get the validation rules that apply to the request.
    *
    * @return array
    */
    public function rules()
    {
        return [
            "user" => 'required|unique:users|regex:/^[a-zA-Z0-9._-]+$/',
            'photo' => 'nullable|image|mimes:jpeg,png,jpg,gif|max:2048',
            "role" => 'required|in:Admin,Employer',
            "service_id" => 'nullable'
        ];
    }



    public function messages(){
        return [
            'user.unique' => 'A identificação que inseriu já existe na base de dados.'
        ];
    }



}
