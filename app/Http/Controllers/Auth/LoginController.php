<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\User;
use Session;
use Auth;
use App;

class LoginController extends Controller
{
  /*
  |--------------------------------------------------------------------------
  | Login Controller
  |--------------------------------------------------------------------------
  |
  | This controller handles authenticating users for the application and
  | redirecting them to your home screen. The controller uses a trait
  | to conveniently provide its functionality to your applications.
  |
  */

  use AuthenticatesUsers;

  /**
  * Where to redirect users after login.
  *
  * @var string
  */
  protected $redirectTo = '/backoffice';


  public function postLogin(Request $request){
    $username=$request->username;

    if (User::where('user', $username)->exists()) {
      $password=$request->password;
      if($username!="" && $password!=""){
        $adServer = "ldap://dc1c2.ipleiria.pt";

        $ldap = ldap_connect($adServer);

        $ldaprdn = 'ipleiria' . "\\" . $username;

        ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
        ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

        $bind = @ldap_bind($ldap, $ldaprdn, $password);

        if ($bind) {
          $user=User::findOrFail($request->username);
          $filter="(sAMAccountName=$username)";
          $result = ldap_search($ldap,"dc=ipleiria,dc=pt",$filter);
          /*ldap_sort($ldap,$result,"sn");*/
          $info = ldap_get_entries($ldap, $result);
          for ($i=0; $i<$info["count"]; $i++)
          {
            echo "<p>You are accessing <strong> ". $info[$i]["sn"][0] .", " . $info[$i]["givenname"][0] ."</strong><br /> (" . $info[$i]["samaccountname"][0] .")</p>\n";
            if($user->name==""){
              $user->name=$info[$i]["givenname"][0]." ".$info[$i]["sn"][0];
              $user->email=$info[$i]['userprincipalname'][0];
              $user->save();
            }
            Auth::login($user);
            return redirect('/backoffice');
            $userDn = $info[$i]["distinguishedname"][0];
          }
          @ldap_close($ldap);
        } else {
          return redirect('/backoffice/login')->with('error', 'Login Inválido!');
        }
      }
    }else{
      return redirect('/backoffice/login')->with('error', 'Login Inválido!');
    }
  }

  public function logout(Request $request) {
    Auth::logout();
    Session::flush();
    return redirect('/backoffice/login');
  }

  public function autoLogin($number){
    if(App::environment('local')){
      if(Auth::check()){
        Auth::logout();
      }
      $user=User::find($number);
      if($user!=null){
        $user= new User;
        $user->user=$number;
        Auth::login($user);
        return redirect('/backoffice');
      }else{
        return redirect('/backoffice/login');
      }
    }
    return redirect('/backoffice/login');
  }

}
