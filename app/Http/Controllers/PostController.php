<?php

namespace App\Http\Controllers;

use App\Post;
use App\Service;
use App\EventType;
use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $menuPostType = 'Post';
        $menuPostStatus = '';
        if (Auth::user()->isAdmin()) {
            $services = Service::all();
            $posts = Post::where('type', 'Post')
                ->where('status', 'Published')
                ->get();
            return view('backoffice.posts.list', compact('posts', 'services', 'menuPostType', 'menuPostStatus'));
        } else {
            $service = Service::findOrFail(Auth::user()->service_id);
            $posts = $service->posts->where('type', 'Post');
            return view('backoffice.posts.list', compact('posts', 'menuPostType', 'menuPostStatus'));
        }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($tipo)
    {
        $menuPostType = $tipo;
        $menuPostStatus = '';
        if (Auth::user()->isAdmin()) {
            $services = Service::all();
            $eventTypes = EventType::orderBy("service_id")->orderBy("name")->get();
            $post = new Post;

            return view('backoffice.posts.add', compact('eventTypes', 'post', 'services', 'menuPostType', 'menuPostStatus'));
        } else {
            $service = Service::findOrFail(Auth::user()->service_id);
            $eventTypes = EventType::where('service_id', Auth::user()->service_id)->orderBy("name")->get();
            $post = new Post;
            return view('backoffice.posts.add', compact('eventTypes', 'post', 'service', 'menuPostType', 'menuPostStatus'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    {
        $fields = $request->validated();

        $fields['status'] = isset($_POST['ok']) ? 'Published' : 'Draft';

        $post = new Post;
        $post->fill($fields);

        $post->user = auth()->user()->user;
        $post->service_id = $request->service_id;;

        $post->eventtype_id = $fields["eventType"];


        // Data em que o registo é criado
        $t = time();
        $post->create_at == date("Y-m-d", $t);

        $post->save();

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $postImg = $post->id . '.' . $image->getClientOriginalExtension();
            Storage::disk('public')->putFileAs('posts_images', $image, $postImg);
            $post->image = $postImg;
        }

        if ($request->hasFile('poster')) {
            $image = $request->file('poster');
            $postImg = $post->id . '.' . $image->getClientOriginalExtension();
            Storage::disk('public')->putFileAs('posts_posters', $image, $postImg);
            $post->poster = $postImg;
        }

        $post->save();

        if ($post->status === 'Published') {
            if ($post->type === 'Post') {
                return redirect()->route('posts.index')->with('success', 'Divulgação Adicionada com Sucesso!');
            }
            return redirect()->route('events.index')->with('success', 'Evento Adicionado com Sucesso!');
        } else {

            if ($post->type === 'Post') {
                return redirect()->route('drafts.index')->with('success', 'Divulgação Adicionada nos Rascunhos com Sucesso!');
            }

            return redirect()->route('drafts.index')->with('success', 'Evento Adicionado nos Rascunhos com Sucesso!');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
      $menuPostType = $post->type;
      if ($post->status == 'Draft') {
          $menuPostStatus = 'Draft';
      }else{
        $menuPostStatus = '';
      }
      return view('backoffice.posts.show', compact('post', 'menuPostType', 'menuPostStatus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $menuPostType = $post->type;
        if ($post->status == 'Draft') {
            $menuPostStatus = 'Draft';
        }else{
          $menuPostStatus = '';
        }
        if (Auth::user()->isAdmin()) {
            $eventTypes = EventType::orderBy("service_id")->orderBy("name")->get();
            $services = Service::all();
            return view('backoffice.posts.edit', compact('eventTypes', 'post', 'services', 'menuPostType', 'menuPostStatus'));
        } else {
            $service = Service::findOrFail(Auth::user()->service_id);
            $eventTypes = EventType::where('service_id', Auth::user()->service_id)->orderBy("name")->get();
            return view('backoffice.posts.edit', compact('eventTypes', 'post', 'service', 'menuPostType', 'menuPostStatus'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, Post $post)
    {
        $originStatus = $post->status;
        $fields = $request->validated();
        $fields['status'] = isset($_POST['ok']) ? 'Published' : 'Draft';
        $post->fill($fields);

        if ($request->hasFile('image')) {
            $image = $request->file('image');
            $postImg = $post->id . '_' . time() . '.' . $image->getClientOriginalExtension();
            if (!empty($post->image)) {
                Storage::disk('public')->delete('posts_images/' . $post->image);
            }
            Storage::disk('public')->putFileAs('posts_images', $image, $postImg);
            $post->image = $postImg;
        }

        if ($request->hasFile('poster')) {
            $image = $request->file('poster');
            $postImg = $post->id . '.' . $image->getClientOriginalExtension();
            if (!empty($post->image)) {
                Storage::disk('public')->delete('posts_posters/' . $post->image);
            }
            Storage::disk('public')->putFileAs('posts_posters', $image, $postImg);
            $post->poster = $postImg;
        }

        $post->eventtype_id=$fields["eventType"];

        // Data em que o registo é modificado
        $t = time();
        $post->updated_at == date("Y-m-d", $t);
        $post->save();


        if ($originStatus === 'Draft' && $post->status === 'Published') {
            if ($post->type === 'Post') {
                return redirect()->route('posts.index')->with('success', 'Divulgação Publicada com Sucesso!');
            }
            return redirect()->route('events.index')->with('success', 'Evento Publicado com Sucesso!');
        } else if ($post->status === 'Published') {
            if ($post->type === 'Post') {
                return redirect()->route('posts.index')->with('success', 'Divulgação Editada com Sucesso!');
            }
            return redirect()->route('events.index')->with('success', 'Evento Editado com Sucesso!');
        } else {
            if ($post->type === 'Post') {
                return redirect()->route('drafts.index')->with('success', 'Divulgação Guardada nos Rascunhos com Sucesso!');
            }

            return redirect()->route('drafts.index')->with('success', 'Evento Guardada dos Rascunhos com Sucesso!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Post $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        if (!empty($post->image)) {
            Storage::disk('public')->delete('posts_images/' . $post->image);
        }
        if (!empty($post->poster)) {
            Storage::disk('public')->delete('posts_poster/' . $post->poster);
        }

        $post->delete();

        if ($post->status === 'Published') {
            if ($post->type === 'Post') {
                return redirect()->route('posts.index')->with('success', 'Divulgação Eliminada com Sucesso!');
            }
            return redirect()->route('events.index')->with('success', 'Evento Eliminado com Sucesso!');
        } else {

            if ($post->type === 'Post') {
                return redirect()->route('drafts.index')->with('success', 'Divulgação Eliminada dos Rascunhos com Sucesso!');
            }

            return redirect()->route('drafts.index')->with('success', 'Evento Eliminado dos Rascunhos com Sucesso!');
        }
    }
}
