<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\SoftDeletes;



class UserController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(User::class, 'user');
    }

    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index(Request $request)
    {


        if(Auth::User()->role!='Admin'){
            abort (401);
        }


        /* Utilizadores */
        if (count($request->all()) == 0) {
            $users = User::all();
        } else {
            $users = User::query();
            if ($request->filled('user')) {
                $users->where('user', 'like', '%' . $request->user . '%');
            }
            if ($request->filled('name')) {
                $users->where('name', 'like', '%' . $request->name . '%');
            }
            if ($request->filled('email')) {
                $users->where('email', 'like', '%' . $request->email . '%');
            }
            if ($request->filled('role')) {
                $users->where('role', $request->role);
            }
            if ($request->filled('service_id')) {
                $users->where('service_id', $request->service_id);
            }
            $users = $users->get();
        }


        return view('backoffice.users.list', compact('users'));
    }



    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        $user = new User;
        $services = DB::table('services')->get();
        return view('backoffice.users.add', compact('user','services'));
    }






    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(StoreUserRequest $request)
    {
        $fields = $request->validated();
        $user = new User;
        $user->fill($fields);


        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $profileImg = $user->user . '_' . time() . '.' . $photo->getClientOriginalExtension();
            Storage::disk('public')->putFileAs('users_photos/', $photo, $profileImg);
            $user->photo = $profileImg;
            $user->save();
        }

        // data em que foi criado
        $t=time();
        $user->create_at == date("Y-m-d",$t);

        $user->save();

        return redirect()->route('users.index')->with('success', 'Utilizador criado com sucesso ');
    }





        /***********************************************************************//*
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    * @param  \App\User  $user
    */
    public function show_trashed(User $user)
    {
        $users = User::onlyTrashed()->get();
        return view('backoffice.users.restore',compact("users"));
    }





    /***********************************************************************//*
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    * @param  \App\User  $user
    */
    public function restore($user)
    {
        if ($user!=null){
            User::withTrashed()->where("user",$user)->restore();
            return redirect()->route('users.trashed')->with('success', 'Utilizador recuperado com sucesso');
        }else{
            return redirect()->route('users.trashed')->with('errors', 'Utilizador não identificado');
        }
    }





    /**
    * Display the specified resource.
    *
    * @param  \App\User  $user
    * @return \Illuminate\Http\Response
    */
    public function show(User $user)
    {

        // POSTS do utilizaror
        $posts = DB::table('posts')
        ->select('*')
        ->where('user', $user->user)
        ->get();


        // NEWSLETTERS do utilizador
        $newsletters = DB::table('newsletters')
        ->select('*')
        ->where('user', $user->user)
        ->get();

        return view('backoffice.users.show', compact("user","posts",'newsletters'));


    }




    /**
    * Show the form for editing the specified resource.
    *
    * @param  \App\User  $user
    * @return \Illuminate\Http\Response
    */
    public function edit(User $user)
    {
        $services = DB::table('services')->get();

        return view('backoffice.users.edit', compact('user','services'));
    }






    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \App\User  $user
    * @return \Illuminate\Http\Response
    */
    public function update(UpdateUserRequest $request, User $user)
    {

        $fields = $request->validated();
        $user->fill($fields);


        if ($request->photo==null){
            $user->photo = "default_user.png";
        }

        if ($request->hasFile('photo')) {
            $photo = $request->file('photo');
            $profileImg = $user->user . '_' . time() . '.' . $photo->getClientOriginalExtension();
            if (!empty($user->photo)) {
                Storage::disk('public')->delete('users_photos/' . $user->photo);
            }
            Storage::disk('public')->putFileAs('users_photos/', $photo, $profileImg);
            $user->photo = $profileImg;
        }

        if($user->role=="Admin"){
            $user->service_id=null;
        }

        // data em que foi modificado
        $t=time();
        $user->updated_at == date("Y-m-d",$t);

        $user->save();


        if (Auth::user()->role=="Admin"){
            return redirect()->route('users.index')->with('success', 'Dados do utilizador modificados com sucesso');
        }else{
            return redirect()->route('options',Auth::user()->user)->with('success', 'Dados do utilizador modificados com sucesso');
        }
        

    }




    /*     public function send_reactivate_email(User $user)
    {
        $user->sendEmailVerificationNotification();
        return $this->edit($user);
    } */





    /**
    * Remove the specified resource from storage.
    *
    * @param  \App\User  $user
    * @return \Illuminate\Http\Response
    */
    public function destroy(User $user) //
    {

        //$user = User::findOrFail($request->modalUserid);
        $user->delete();
        return redirect()->route('users.index')->with('success', 'Utilizador eliminado com sucesso');
    }

}
