<?php

namespace App\Http\Controllers;

use Auth;
use App\Post;
use App\Note;
use App\Task;
use App\SubTask;
use App\Service;
use App\Newsletter;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){

      if (Auth::user()->isAdmin()) {
        $postsPublished = Post::where('deleted_at', '=', NULL)
        ->where('status', '=', 'Published')
        ->count();

        $postsDraft = Post::where('deleted_at', '=', NULL)
        ->where('status', '=', 'Draft')
        ->count();

        $newsletters = Newsletter::where('deleted_at', '=', NULL)
        ->count();
      }else {
        $service = Service::findOrFail(Auth::user()->service_id);

        $postsPublished = Post::where('service_id', $service->id)
        ->where('deleted_at', '=', NULL)
        ->where('status', '=', 'Published')
        ->count();

        $postsDraft = Post::where('service_id', $service->id)
        ->where('deleted_at', '=', NULL)
        ->where('status', '=', 'Draft')
        ->count();

        $newsletters = Newsletter::where('service_id', $service->id)
        ->where('deleted_at', '=', NULL)
        ->count();
      }

        $tasks = Auth()->user()->tasks
        ->count();

        $subtasks = SubTask::count();

        $subtasksDone = SubTask::where('status', '=', 'Done')
        ->count();

        $notes = Auth()->user()->notes;

        return view('backoffice.index', compact('postsPublished', 'newsletters', 'postsDraft', 'subtasksDone', 'subtasks', 'tasks', 'notes'));
    }
}
