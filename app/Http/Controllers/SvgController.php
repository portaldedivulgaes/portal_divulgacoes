<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class SvgController extends Controller
{

    public function getSvg($post)
    {
        $post=Post::findOrFail($post);
        return response()->view('layout.svg_card', ['post' => $post], 200)->header('Content-Type', 'image/svg+xml');
    }
    public function getSvgThumb($post)
    {
        $post=Post::findOrFail($post);
        return response()->view('layout.svg_thumbnail', ['post' => $post], 200)->header('Content-Type', 'image/svg+xml');
    }
}
