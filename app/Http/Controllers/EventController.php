<?php

namespace App\Http\Controllers;

use App\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Auth;
use App\Service;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $menuPostType = 'Event';
        $menuPostStatus = '';
        if (Auth::user()->isAdmin()) {
            $services = Service::all();
            $posts = Post::where('type', 'Event')
                ->where ('status', 'Published')
                ->get();
            return view('backoffice.events.list', compact('posts', 'services', 'menuPostType', 'menuPostStatus'));
        } else {
            $service = Service::findOrFail(Auth::user()->service_id);
            $posts = $service->posts->where ('type', 'Event');
            return view('backoffice.events.list', compact('posts', 'menuPostType', 'menuPostStatus'));
        }
    }
}
