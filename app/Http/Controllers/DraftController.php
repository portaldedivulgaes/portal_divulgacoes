<?php

namespace App\Http\Controllers;

use App\Post;
use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Auth;

class DraftController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->isAdmin()) {
            $services = Service::all();
            $posts = Post::where('status', 'Draft')
                ->get();
            return view('backoffice.drafts.list', compact('posts', 'services'));
        } else {
            $service = Service::findOrFail(Auth::user()->service_id);
            $posts = $service->posts->where ('status', 'Draft');
            return view('backoffice.drafts.list', compact('posts'));
        }
    }
}
