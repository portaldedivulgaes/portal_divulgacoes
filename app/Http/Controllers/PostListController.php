<?php

namespace App\Http\Controllers;

use App\Post;
use App\EventType;
use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Route;

class PostListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($service, Request $request)
    {
      $services=Service::all();
        $service_name = '';

        $search = $request->search;

        $service=Service::where('initial','=',$service)->firstOrFail();
        $posts = Post::where('service_id', $service->id)->where('status', 'Published');

        $service_name = $service->initial;

        if (!empty($search)) {
            $posts = $posts->where('title', 'LIKE', "%{$search}%");
        }

        $posts = $posts->paginate(6);
        $posts->appends(['search' => $search]);

        $eventTypes = DB::table('eventTypes')
            ->get();

        return view('divulgacoes', compact('posts', 'eventTypes', 'search', 'service_name', 'services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param \App\PostList $postList
     * @return \Illuminate\Http\Response
     */
    public function show(PostList $postList)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\PostList $postList
     * @return \Illuminate\Http\Response
     */
    public function edit(PostList $postList)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\PostList $postList
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PostList $postList)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\PostList $postList
     * @return \Illuminate\Http\Response
     */
    public function destroy(PostList $postList)
    {
        //
    }
}
