<?php

namespace App\Http\Controllers;

use App\Note;
use Illuminate\Http\Request;
use App\Http\Requests\StoreNoteRequest;
use App\Http\Requests\UpdateNoteRequest;

class NoteController extends Controller
{
    public function store(StoreNoteRequest $request){
        $fields=$request->validated();
        $note=new Note();
        $note->user=auth()->user()->user;
        $note->fill($fields);
        $note->save();
        return redirect()->route('home');
    }

    public function create(){
        $note = new Note;
        return view('backoffice.index', compact('note', 'user'));
    }

    public function update(UpdateNoteRequest $request, Note $note){
        $fields=$request->validated();
        $note->fill($fields);
        $note->save();
        return redirect()->route('home');
    }

    public function edit(Note $note){
        return view('backoffice.index', compact('note'));
    }

    public function destroy(Note $note){
        $note->delete();
        return redirect()->route('home');
    }
}
