<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Service;

class PageController extends Controller
{
    // Páginas Frontoffice

    public function sobre(){
      $services=Service::all();
        return view('sobre', compact('services'));
    }

}
