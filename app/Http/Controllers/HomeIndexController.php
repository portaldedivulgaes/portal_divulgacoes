<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Service;

class HomeIndexController extends Controller
{
    public function index(){

      $posts = Post::where('deleted_at', '=', NULL)
      ->where('status', '=', 'Published')
      ->orderby('important', 'DESC')
      ->orderby('created_at', 'ASC')
      ->take(3)->get();

      $services = Service::all();

      return view('index', compact('posts', 'services'));
    }
}
