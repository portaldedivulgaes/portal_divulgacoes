<?php

namespace App\Http\Controllers;

use App\Contact;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\URL;
use Illuminate\Http\Request;
use Mail;
use App\Mail\SendContact;
use App\Service;
use App\Notifications\alertMessage;


class ContactController extends Controller
{
  /**
  * Display a listing of the resource.
  *
  * @return \Illuminate\Http\Response
  */
  public function contact()
  {
    $services = DB::table('services')->get();
    return view('contactos', ['services' => $services]);

  }

  /**
  * Show the application dashboard.
  *
  * @return \Illuminate\Http\Response
  */
  public function contactPost(Request $request)
  {
    $this->validate($request, [
      'name' => 'required',
      'email' => 'required|email',
      'message' => 'required'
    ]);

    $service=Service::findOrFail($request->get('service'));

    Mail::to($service->email)->send(new SendContact($request->get('email'), $request->get('name'), $request->get('subject'), $request->get('message')));
    $url = URL::route('contact') . '#contact_div';
    $service->notify(new alertMessage($request->get('name'),$request->get('subject')));
    return redirect($url)->with('success', 'Mensagem enviada com sucesso!');
  }
}
