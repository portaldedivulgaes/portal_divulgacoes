<?php

namespace App\Http\Controllers;

use App\View;
use App\User;
use App\Post;
use Illuminate\Support\Facades\Auth;
use Facade\Ignition\Middleware\AddLogs;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
class StatisticsController extends Controller


{

    public function index()
    {

        $services = DB::table('services')->get();
        $posts =  DB::table('posts')->get();
        $newsletters =  DB::table('newsletters')->get();
        $months = array("Janeiro", "Fevereiro", "Março", "Abril","Maio", "Junho", "Julho", "Agosto","Setembro", "Outubro", "Novembro", "Dezembro");



        // SE FOR ADMINSTRADOR
        if(Auth::User()->role=='Admin'){


            //Admin: Total de publicaçoes
            //SELECT COUNT(*) as quant FROM posts
            $totalposts = DB::table("posts")
            ->select(DB::raw("COUNT(*) as quant"))
            ->get();

            if ($totalposts->isEmpty()) {
                $totalposts=null;
            }


            //Admin: total de divulgaçoes ou eventos ??????????????????????????????????????????????
            //SELECT TYPE, COUNT(*) FROM posts GROUP BY type
            $postspublished = DB::table("posts")
            ->select(DB::raw("type, COUNT(*) as quant"))
            ->groupBy(DB::raw("type"))
            ->get();

            if ($postspublished->isEmpty()) {
                $postspublished=null;
            }


            //Admin: Numero total de visitas nos posts
            $visits = VIEW::distinct('id')->count('id');


            // Admin: Quantos eventos? Quantas publicações?
            // SELECT NAME, COUNT(*) AS quant FROM eventTypes JOIN posts ON eventTypes.id=posts.eventtype_id GROUP by eventTypes.name ORDER BY eventTypes.name asc
            $postevents = DB::table('eventTypes')
            ->selectRaw("eventTypes.name, COUNT(*) as quant")
            ->join('posts', 'posts.eventtype_id', '=', 'eventTypes.id')
            ->groupBy('eventTypes.name')
            ->orderBy('eventTypes.name','asc')
            ->get();

            if ($postevents->isEmpty()) {
                $postevents=null;
            }



            //ADMIN: Total de publicações por gabinete
            //SELECT posts.service_id, services.initial, COUNT(*) AS quant FROM posts LEFT JOIN services ON services.id=posts.service_id GROUP BY posts.service_id
            $gabposts = DB::table('posts')
            ->selectRaw("posts.service_id, services.initial, COUNT(*) AS quant")
            ->leftJoin('services', 'posts.service_id', '=', 'services.id')
            ->groupBy('posts.service_id')
            ->orderBy('posts.service_id','asc')
            ->get();

            if ($gabposts->isEmpty()) {
                $gabposts=null;
            }

            //ADMIN: Total de newslettes por gabinete
            //SELECT services.initial, COUNT(*) AS quant FROM newsletters LEFT JOIN services ON services.id=newsletters.service_id GROUP BY services.initial ORDER BY services.initial asc
            $gabnews = DB::table('newsletters')
            ->selectRaw("services.initial, COUNT(*) AS quant")
            ->leftJoin('services', 'newsletters.service_id', '=', 'services.id')
            ->groupBy('services.initial')
            ->orderBy('services.initial','asc')
            ->get();

            if ($gabnews->isEmpty()) {
                $gabnews=null;
            }



            // ADMIN: Numero de Publicações por mes
            // SELECT EXTRACT(month FROM created_at) "Month", COUNT(*) AS quant FROM posts WHERE EXTRACT(YEAR FROM created_at) =2020
            //GROUP BY EXTRACT(month FROM created_at) ORDER BY EXTRACT(month FROM created_at)
            $pubsmonths= DB::table('posts')
            ->selectRaw("EXTRACT(MONTH FROM created_at) as month, COUNT(*) AS quant ")
            ->whereYear('created_at','=',date("Y"))
            ->groupBy('month')
            ->get();

            $test = sizeof($pubsmonths);

            if ($pubsmonths->isEmpty()) {

                $pubsmonths=null;
                $pubsyear=null;

            }else{

               $pubsyear = array(1=>0,2=>0,3=>0,4=>0,5=>0,6=>0,7=>0,8=>0,9=>0,10=>0,11=>0,12=>0);

               for ($i = 1; $i <= 12 ; $i++){

                    for ($a=0 ; $a < sizeof($pubsmonths) ;$a++){

                        if ($pubsmonths[$a]->month == $i){
                            $pubsyear[$i] = $pubsmonths[$a]->quant;
                        }
                    }
                }

            }



            //ADMIN: Numero de Newsletters por mês
            //SELECT EXTRACT(month FROM created_at) "Month", count(*) AS quant FROM newsletters WHERE EXTRACT(YEAR FROM created_at) = YEAR(CURDATE()) GROUP BY EXTRACT(month FROM created_at) ORDER BY EXTRACT(month FROM created_at)



            //ADMIN: Numero total de newsletters
            $newsletters = DB::table('newsletters')->count();


            //ADMIN: Numero total de funcionários
            $totalusers = DB::table('users')->count();


            //Admin: Funcionário com maior numero de posts
            //SELECT user, COUNT(user) as quant FROM posts GROUP BY USER ORDER BY quant desc;
            $hardworker = DB::table("posts")
            ->select(DB::raw("user, COUNT(user) as quant"))
            ->groupBy(DB::raw("user"))
            ->orderBy('quant', 'desc')
            ->get();

            if ($hardworker->isEmpty()) {
                $hardworker=null;
            }



            return view('backoffice.statistics.statistics' , compact('test','months','pubsyear','services','postspublished','visits','totalposts','postevents','gabposts','gabnews','newsletters','totalusers','hardworker'));










            // SE FOR FUNCIONÁRIO
        } else {
            //Funcionário: numero total de Visitas do seu gabinete
            // SELECT COUNT(*) AS quant FROM views JOIN posts ON views.post_id =posts.id WHERE posts.service_id= {{Auth::User()->service_id}}
            $visits= DB::table("views")
            ->select(DB::raw("COUNT(*) AS quant "))
            ->join('posts', 'views.post_id', '=', 'posts.id')
            ->where('posts.service_id', '=', Auth::User()->service_id)
            ->get();



            //Funcionário: Total de publicaçoes do seu gabinete
            //SELECT COUNT(*) as quant FROM posts
            $totalposts = DB::table("posts")
            ->select(DB::raw("COUNT(*) as quant"))
            ->where('posts.service_id', '=', Auth::User()->service_id)
            ->get();

            if ($totalposts->isEmpty()) {
                $totalposts=null;
            }




            //Funcionário: publicações e eventos do gabinete
            $postspublished = DB::table("posts")
            ->select(DB::raw("type, COUNT(*) as quant"))
            ->where('service_id', '=', Auth::User()->service_id)
            ->groupBy(DB::raw("type"))
            ->get();

            if ($postspublished->isEmpty()) {
                $postspublished=null;
            }





            //Funcionário: Total de publicações do gabinete
            $gabposts = DB::table("posts")
            ->select(DB::raw("service_id"))
            ->where('service_id', '=', Auth::User()->service_id)
            ->get();

            if ($gabposts->isEmpty()) {
                $gabposts=null;
            }



            // Funcionário: Quantos eventos? Quantas publicações? do gabinete
            // SELECT NAME, COUNT(*) AS quant FROM eventTypes JOIN posts ON eventTypes.id=posts.eventtype_id where posts.service_id=XXX GROUP by eventTypes.name ORDER BY eventTypes.name asc
            $postevents = DB::table('eventTypes')
            ->selectRaw("eventTypes.name, COUNT(*) as quant")
            ->join('posts', 'posts.eventtype_id', '=', 'eventTypes.id')
            ->where('posts.service_id', '=', Auth::User()->service_id)
            ->groupBy('eventTypes.name')
            ->orderBy('eventTypes.name','asc')
            ->get();

            if ($postevents->isEmpty()) {
                $postevents=null;
            }




             // Funcionário: Numero de Publicações por mes do gabinete
            // SELECT EXTRACT(month FROM created_at) "Month", COUNT(*) AS quant FROM posts WHERE EXTRACT(YEAR FROM created_at) =2020
            //GROUP BY EXTRACT(month FROM created_at) ORDER BY EXTRACT(month FROM created_at)
            $pubsmonths= DB::table('posts')
            ->selectRaw("EXTRACT(MONTH FROM created_at) as month, COUNT(*) AS quant ")
            ->where('service_id', '=', Auth::User()->service_id)
            ->whereYear('created_at','=',date("Y"))
            ->groupBy('month')
            ->get();

            $test = sizeof($pubsmonths);

            if ($pubsmonths->isEmpty()) {

                $pubsmonths=null;
                $pubsyear=null;

            }else{

               $pubsyear = array(1=>0,2=>0,3=>0,4=>0,5=>0,6=>0,7=>0,8=>0,9=>0,10=>0,11=>0,12=>0);

               for ($i = 1; $i <= 12 ; $i++){

                    for ($a=0 ; $a < sizeof($pubsmonths) ;$a++){

                        if ($pubsmonths[$a]->month == $i){
                            $pubsyear[$i] = $pubsmonths[$a]->quant;
                        }
                    }
                }

            }


            //Funcionário: numero total de visistas por post
            //Funcionário: post com mais visistas

            // Total de newsletters do gabinete
            //SELECT COUNT(*) AS quant FROM newsletters WHERE service_id = {{service_id}}
            $newsletters = DB::table('newsletters')
            ->select(DB::raw("COUNT(*) AS quant"))
            ->where('service_id', '=', Auth::User()->service_id)
            ->count();


            return view('backoffice.statistics.statistics' , compact('visits','months','postspublished','gabposts','totalposts','pubsyear','postevents','newsletters'));
        }

















    }


}
