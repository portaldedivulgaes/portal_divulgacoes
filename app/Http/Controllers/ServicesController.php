<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UpdateServiceRequest;


class ServicesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

/*         if(Auth::User()->service_id!=$service){
            abort (401);
        } */


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service)
    {
        $services = DB::table('services')->get();

        //O funcionário apenas pode alterar a informação do seu gabinete. E adminstrador tambem
        if (Auth::user()->role!="Admin"){
            if(Auth::User()->service_id != $service->id ){
                abort (401);
            }else{
                return view('backoffice.options.serviceedit', compact('services','service'));
            }
        }else{
            return view('backoffice.options.serviceedit', compact('services','service'));
        }
    }









    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateServiceRequest $request, Service $service)
    {
       $fields = $request->validated();
        $service->fill($fields);

        $service->updated_at == date("Y-m-d",time()); // data em que foi modificado

        $service->initial=strtoupper($service->initial); //Iniciais para maiusculas

        $service->save();

        return redirect()->route('options',Auth::user()->user)->with('success', 'Dados do gabinete actualizados com sucesso');


    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service)
    {
        //
    }
}
