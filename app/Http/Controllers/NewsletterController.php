<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Storage;
use App\Newsletter;
use App\Post;
use App\TargetAudience;
use App\Service;
use Mail;
use App\Mail\SendNewsletter;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Http\Requests\StoreNewsletterRequest;
use Session;


class NewsletterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('dataNewsletter')) {
            session()->forget('dataNewsletter');
        }
        if (Auth::user()->isAdmin()) {
            $services = Service::all();
            $newsletters = Newsletter::all();
            return view('backoffice.newsletters.list', compact('newsletters', 'services'));
        } else {
            $service = Service::findOrFail(Auth::user()->service_id);
            $newsletters = $service->newsletters;
            return view('backoffice.newsletters.list', compact('newsletters'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $oldData=null;
        if(session()->has('dataNewsletter')) {
            $oldData = session()->get('dataNewsletter');
        }

        if (Auth::user()->isAdmin()) {
            $services = Service::all();
            $posts = Post::where('status', 'Published')->get();
            $targetaudiences = TargetAudience::all();

            return view('backoffice.newsletters.add', compact('posts', 'targetaudiences', 'services', 'oldData'));
        } else {
            $service = Service::findOrFail(Auth::user()->service_id);
            $posts = $service->posts->where ('status', 'Published');
            $targetaudiences = TargetAudience::all();

            return view('backoffice.newsletters.add', compact('posts', 'targetaudiences', 'service', 'oldData'));
        }

    }

    public function preview(StoreNewsletterRequest $request)
    {
        $subject = $request->subject;
        $targetaudiences = $request->targetaudience;
        $selectedPosts = explode(',', $request->selectedPosts);
        $posts = Post::find($selectedPosts);
        $service_id = Service::find($request->service_id);
//        dd($posts,$targetaudiences,$subject);
        $request->validated();
        session()->put('dataNewsletter', ['targetAudience'=>$targetaudiences, 'selectedPosts'=>$selectedPosts, 'subject'=>$subject, 'service_id'=>$request->service_id]);

        return view('backoffice.newsletters.show', ['subject' => $subject, 'targetaudiences' => $targetaudiences, 'posts' => $posts, 'selectedPosts' => $selectedPosts, 'service_id' => $service_id]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminat\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNewsletterRequest $request)
    {
        if(session()->has('dataNewsletter')) {
            session()->forget('dataNewsletter');
        }
        $fields = $request->validated();
        $newsletter = new Newsletter();

        $newsletter->user = auth()->user()->user;
        $newsletter->service_id = $request->service_id;
        $newsletter->subject = $fields["subject"];

        // Data em que é criada
        $t = time();
        $newsletter->create_at == date("Y-m-d", $t);
        $newsletter->save();


        foreach ($fields['selectedPosts'] as $p) {
            $post = Post::find($p);
            $newsletter->posts()->attach($post);
        }

        $posts = $newsletter->posts()->get();
        $from = $newsletter->service->email;
        $subject = $newsletter->subject;
        $path = Storage::disk('public')->url('posts_images') . "/";


        foreach ($fields['targetaudience'] as $t) {
            // Guarda na Base de dados
            $targetAudience = Targetaudience::where('email', $t)->first();
            $newsletter->targetaudiences()->attach($targetAudience);

            // Envia o email
            Mail::to($t)->send(new SendNewsLetter($posts, $from, $subject, $path));

        }
        $newsletter->save();


        return redirect()->route('newsletters.index')->with('success', 'Newsletter criada com sucesso!');
    }


    function destroy(Newsletter $newsletter)
    {
        $newsletter->delete();
        return redirect()->route('newsletters.index')->with('success', 'Newsletter eliminada com sucesso!');
    }
}
