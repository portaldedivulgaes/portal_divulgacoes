<?php

namespace App\Http\Controllers;

use App\EventType;
use App\Service;
use Auth;
use Illuminate\Http\Request;
use App\Http\Requests\StoreEventTypesRequest;
use App\Http\Requests\UpdateEventTypesRequest;

class EventTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      if(Auth::user()->isAdmin()){
        $services=Service::all();
        $eventTypes=EventType::all();
        return view('backoffice.eventTypes.list', compact('eventTypes', 'services'));
      }else{
        $service=Service::findOrFail(Auth::user()->service_id);
        $eventTypes=$service->eventtypes;
        return view('backoffice.eventTypes.list', compact('eventTypes'));
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $EventType=new EventType;
        return view('backoffice.eventTypes.add', compact("EventType"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreEventTypesRequest $request)
    {
      $fields=$request->validated();
      $eventType=new EventType();
      $eventType->fill($fields);
      $eventType->save();
      return $eventType->load('service');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\EventType  $eventType
     * @return \Illuminate\Http\Response
     */
    public function show(EventType $eventType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\EventType  $eventType
     * @return \Illuminate\Http\Response
     */
    public function edit(EventType $eventType)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\EventType  $eventType
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateEventTypesRequest $request, EventType $eventType)
    {
      $fields=$request->validated();
      $eventType=EventType::findOrFail($request->id);
      $eventType->fill($fields);
      $eventType->save();
      return $eventType->load('service');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\EventType  $eventType
     * @return \Illuminate\Http\Response
     */
    public function destroy(EventType $eventType, $id)
    {
      $eventType=EventType::findOrFail($id);
      $eventType->delete();
      return 'deleted';
    }
}
