<?php

namespace App\Http\Controllers;

use App\User;
use Artisan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;



class OptionsController extends Controller
{

    public function index($loggeduser)
    {

        $services = DB::table('services')->get();

        // POSTS do utilizaror
        $posts = DB::table('posts')
        ->select('*')
        ->where('user', $loggeduser)
        ->get();


        // NEWSLETTERS do utilizador
        $newsletters = DB::table('newsletters')
        ->select('*')
        ->where('user', $loggeduser)
        ->get();

        return view('backoffice.options.options', compact('services','posts','newsletters'));

    }



    public function show_reset()
    {
        if(Auth::User()->role!='Admin'){
            abort (401);
        }

        return view('backoffice.options.reset');

    }


    public function show_backup()
    {

        if(Auth::User()->role!='Admin'){
            abort (401);
        }
        return view('backoffice.options.backup');

    }




    public function resetdatabase()
    {
        //Schema::disableForeignKeyConstraints();


        //Artisan::call('migrate', array('--path' => 'app/migrations', '--force' => true));


        return redirect()->route('options',Auth::user())->with('success', 'Valores de origem repostos com sucesso');

    }






    public function backupdatabase()
    {

        return redirect()->route('options')->with('success', 'Backup criado com sucesso ');

    }




}
