<?php

namespace App\Http\Controllers;

use App\TargetAudience;
use Illuminate\Http\Request;
use App\Http\Requests\StoreTargetAudienceRequest;
use App\Http\Requests\UpdateTargetAudienceRequest;

class TargetAudienceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $targetAudiences=TargetAudience::all();
      return view('backoffice.targetAudience.list', compact('targetAudiences'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTargetAudienceRequest $request)
    {
      $fields=$request->validated();
      $targetAudience=new TargetAudience();
      $targetAudience->fill($fields);
      $targetAudience->save();
      return $targetAudience;
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\TargetAudience  $targetAudience
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTargetAudienceRequest $request, TargetAudience $targetAudience)
    {
      $fields=$request->validated();
      $targetAudience=TargetAudience::findOrFail($request->id);
      $targetAudience->fill($fields);
      $targetAudience->save();
      return $targetAudience;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\TargetAudience  $targetAudience
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $targetAudience=TargetAudience::findOrFail($id);
      $targetAudience->delete();
      return 'deleted';
    }

    public function getEmails(){
      return TargetAudience::all();
    }
}
