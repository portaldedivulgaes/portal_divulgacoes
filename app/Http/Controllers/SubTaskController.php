<?php

namespace App\Http\Controllers;

use App\SubTask;
use Illuminate\Http\Request;
use App\Http\Requests\StoreSubTaskRequest;
use App\Http\Requests\UpdateSubTaskRequest;

class SubTaskController extends Controller
{
  public function index(){
      $subtasks=subTask::all();
      return view('backoffice.tasks.list', compact('subtasks'));
  }

  public function store(StoreSubTaskRequest $request){
      $fields=$request->validated();
      $subtask=new SubTask();
      $subtask->fill($fields);
      $subtask->save();
      return redirect()->route('tasks.index');
  }

  public function create(){
      $subtask = new SubTask;
  }

  public function update(UpdateSubTaskRequest $request, SubTask $subtask){
      $fields=$request->validated();
      $subtask->fill($fields);
      $subtask->save();
      return redirect()->route('tasks.index');
  }

  public function updateStatusDone($subtask){
      $subtask=SubTask::findOrFail($subtask);
      $subtask->status='Done';
      $subtask->save();
      return redirect()->route('tasks.index');
  }

  public function updateStatusTodo($subtask){
      $subtask=SubTask::findOrFail($subtask);
      $subtask->status='To Do';
      $subtask->save();
      return redirect()->route('tasks.index');
  }

  public function edit(Task $task){
      return view('backoffice.tasks.index', compact('subtask'));
  }

  public function destroy(SubTask $subtask){
      $subtask->delete();
      return redirect()->route('tasks.index');
  }
}
