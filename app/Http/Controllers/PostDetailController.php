<?php

namespace App\Http\Controllers;

use App\PostDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Post;
use App\Service;

class PostDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($service, $post)
    {
        $post = Post::findOrFail($post);
        if($post->service->initial==$service){
        $services = Service::all();
          if($post->status == 'Draft' || $post->status == 'Archived'){
              abort (404);
          }else{
              // Adiciona na base de dados a visita ao post
              DB::table('views')->insert( ['created_at' => date("Y-m-d",time()),'post_id' => $post->id ] );

              return view('divulgacao', compact('post', 'services'));
          }
        }else{
            abort(404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PostDetail  $postDetail
     * @return \Illuminate\Http\Response
     */
    public function show(PostDetail $postDetail)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PostDetail  $postDetail
     * @return \Illuminate\Http\Response
     */
    public function edit(PostDetail $postDetail)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PostDetail  $postDetail
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PostDetail $postDetail)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PostDetail  $postDetail
     * @return \Illuminate\Http\Response
     */
    public function destroy(PostDetail $postDetail)
    {
        //
    }
}
