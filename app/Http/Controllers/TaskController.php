<?php

namespace App\Http\Controllers;

use Auth;
use App\Task;
use App\SubTask;
use Illuminate\Http\Request;
use App\Http\Requests\StoreTaskRequest;
use App\Http\Requests\UpdateTaskRequest;

class TaskController extends Controller
{
    public function index(){
        $tasks=Auth()->user()->tasks;
        return view('backoffice.tasks.list', compact('tasks'));
    }

    public function store(StoreTaskRequest $request){
        $fields=$request->validated();
        $task=new Task();
        $task->user=auth()->user()->user;
        $task->fill($fields);
        $task->save();
        return redirect()->route('tasks.index')->with('success', 'Tarefa adicionada com sucesso!');
    }

    public function create(){
        $task = new Task;
        return view('backoffice.tasks.add', compact('tasks', 'user'));
    }

    public function update(UpdateTaskRequest $request, Task $task){
        $fields=$request->validated();
        $task->fill($fields);
        $task->save();
        return redirect()->route('tasks.index')->with('success', 'Tarefa atualizada com sucesso!');
    }

    public function edit(Task $task){
        return view('backoffice.tasks.edit', compact('task'));
    }

    public function destroy(Task $task){
        $task->delete();
        return redirect()->route('tasks.index')->with('success', 'Tarefa eliminada com sucesso.');
    }
}
