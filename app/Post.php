<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    protected $fillable = [
        'title', 'resume', 'description', 'type', 'image', 'poster', 'startDate', 'endDate', 'location',
        'link', 'validity', 'important', 'status', 'tags', 'eventtype_id', 'service_id', '$user'
    ];

    public function eventType()
    {
        return $this->belongsTo("App\EventType", "eventtype_id");
    }

    public function service()
    {
        return $this->belongsTo("App\Service", "service_id");
    }

    public function user()
    {
        return $this->belongsTo("App\User", "user")->withTrashed();
    }

    use SoftDeletes;
}
