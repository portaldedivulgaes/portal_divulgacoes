<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Newsletter extends Model
{
    use SoftDeletes;
    protected $table = 'newsletters';
    protected $fillable = ['service_id','$user','subject'];


    public function user()
    {
        return $this->belongsTo("App\User", "user")->withTrashed();
    }

    public function service()
    {
        return $this->belongsTo("App\Service", "service_id");
    }

    public function posts()
    {
        return $this->belongsToMany("App\Post", "posts_newsletters", 'newsletter_id', 'post_id');
    }

    public function targetaudiences()
    {
        return $this->belongsToMany("App\TargetAudience", 'targetaudiences_newsletters', 'newsletter_id', 'targetaudience_id');

    }








//    protected $dates = [
//        'created_at',
//        'updated_at',
//        'deleted_at'
//    ];

//    protected $dateFormat = 'U';
}
