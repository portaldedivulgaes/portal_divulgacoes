<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class sendCOntact extends Mailable
{
    use Queueable, SerializesModels;
    public  $user_message;
    public  $user_subject;
    public $name;
    public  $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($from, $name, $subject, $message)
    {
      $this->user_message = $message;
      $this->user_subject = $subject;
      $this->name = $name;
      $this->email = $from;
      $this->from($from);
      $this->subject("Noma mensagem de ".$name." - Portal de Divulgações");
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
      return $this->view('email-contact');
    }
}
