<?php

namespace App\Mail;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Collection;


class SendNewsletter extends Mailable
{
    use Queueable, SerializesModels;
    public  $posts;
    public $path;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($posts,$from,$subject,$path)
    {
        $this->posts = $posts;
        $this->path=$path;
        $this->from($from);
        $this->subject($subject);
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
     return $this->view('backoffice.mailtemplate');
    }
}
