<?php

namespace App;

use App\Service;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    protected $primaryKey = "user";
    public $incrementing = false;


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    //user name email photo role service_id
    protected $fillable = ['user', 'photo', 'role', 'service_id'];
    protected $table = 'users';

    public function posts(){
        return $this->hasMany("App\Post","user");
    }

    public function tasks(){
        return $this->hasMany("App\Task","user");
    }

    public function notes(){
        return $this->hasMany("App\Note","user");
    }

    public function service(){
        return $this->belongsTo("App\Service","service_id");
    }

    public function roleToStr()
    {
        switch ($this->role) {
            case 'Employer':
                return 'Funcionário';
            case 'Admin':
                return 'Administrador';
        }
    }


    public function serviceToStr()
    {
        if ($this->service_id == null){
            return 'Não atribuido';
        }else{
            $initial = SERVICE::where('id', $this->service_id)->first()->initial;
            return $initial;
        }
    }




    public function isAdmin()
    {
        return $this->role == 'Admin';
    }


}
