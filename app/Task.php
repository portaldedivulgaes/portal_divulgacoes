<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
    protected $fillable = ['title'];

    public function subtasks(){
      return $this->hasMany("App\SubTask", "tasks_id");
    }

    public function user(){
      return $this->belongsTo("App\User", "user_id");
    }
}
