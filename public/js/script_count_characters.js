//Carateres do campo titulo
var text_max1 = 150;
$('#count_message1').html(text_max1 + ' caracteres disponíveis.');

$('#inputTitle').keyup(function() {
    var text_length1 = $('#inputTitle').val().length;
    var text_remaining1 = text_max1 - text_length1;
    $('#count_message1').html(text_remaining1 + ' caracteres disponíveis.');
});

//Carateres do campo link
var text_max2 = 250;
$('#count_message2').html(text_max2 + ' caracteres disponíveis.');

$('#inputLink').keyup(function() {
    var text_length2 = $('#inputLink').val().length;
    var text_remaining2 = text_max2 - text_length2;
    $('#count_message2').html(text_remaining2 + ' caracteres disponíveis.');
});

//Carateres do campo textoCurto
var text_max3 = 150;
$('#count_message3').html(text_max3 + ' caracteres disponíveis.');

$('#inputResume').keyup(function() {
    var text_length3 = $('#inputResume').val().length;
    var text_remaining3 = text_max3 - text_length3;
    $('#count_message3').html(text_remaining3 + ' caracteres disponíveis.');
});


//Carateres do campo localizacao
var text_max4 = 150;
$('#count_message4').html(text_max4 + ' caracteres disponíveis.');

$('#inputLocation').keyup(function() {
    var text_length4 = $('#inputLocation').val().length;
    var text_remaining4 = text_max4 - text_length4;
    $('#count_message4').html(text_remaining4 + ' caracteres disponíveis.');
});


function info_modal(mensagem) {

    // modal com informação variavel
    $("#titulo_modal").html("Sucesso!");
    $("#modal_mensagem").html(mensagem);

    $("#modal_bt_2").removeClass();
    $("#modal_bt_2").addClass("btn btn-success text-white");
    $("#modal_bt_2").html("<i class='fas fa-check mr-1'></i> OK");
    $("#modal_confirm_delete").hide();

    $("#myModal").modal();

    $('#myModal').on('hidden.bs.modal', function(e) {
        window.location.href = "/backoffice/divulgacoes";
    })
}