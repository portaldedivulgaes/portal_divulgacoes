$(document).ready(function() {
  $(".remove_notification_form").submit(function(){
    data=$(this).serialize();

    $.ajax({
      url: "/backoffice/notifications/markAsRead/"+$(this).find('button').data('id'),
      method: 'POST',
      data: data,
      context: $(this),
      success: function(data){
        var count=$(".badge-counter").html();
        count--;
        $(".badge-counter").html(count);
        if(count==0){
          $(this).parent().parent().parent().append('<p class="text-center small text-gray-500" style="margin:17px;">Não tem Notificações</p>')
          $(".badge-counter").hide();
        }
        $(this).parent().parent().remove();
      },
    });
    return false;
  });
});
