$(document).ready(function () {

    function focus_gabinete(){
        $("#gabinete").removeClass("gabinete_focus");
    }

    $(".gabinfo").on('click', function () {
        $("#gabinete").val($(this).attr("service_id"));
        $("#gabinete").addClass( "gabinete_focus");
        $("#assunto").focus();
        setTimeout(focus_gabinete, 1600);
    });

});
