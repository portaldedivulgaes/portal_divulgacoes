
    $(document).ready(function() {




              $('#dataTable').DataTable( {


                "columnDefs": [
                    { "orderable": false, "targets": 0 },
                    { "width": "60px", "targets": 0 },
                    { "width": "auto", "targets": 1 },
                    { "width": "auto", "targets": 2 },
                    { "width": "auto", "targets": 5 },
                    { "width": "110px", "targets": 6 },
                    { "orderable": false, "targets": 6 }
                  ],


                "language": {
                    "lengthMenu":  "Mostrar _MENU_ registos por página",
                    "search":      "Procurar:",
                    "zeroRecords": "Sem registos",
                    "paginate": {
                      "first":      "Primeiro",
                      "last":       "Ultimo",
                      "next":       "Proximo",
                      "previous":   "Anterior"
                  },

                    "info": "A mostrar página _PAGE_ de _PAGES_",
                    "infoEmpty": "Sem registos disponiveis",
                    "infoFiltered": "(filtrado de um total de _MAX_ registos)"
                },

                "order": [1, 'desc']


            } );


            var formToSubmit //Varial para indicar o forumulário a submeter

            $(".form_user_id").submit(function(e){
                formToSubmit = this;
                $("#spanUserId").text(this.id);
                return false;
            });


            $(".btn_submit").click(function(e){
                formToSubmit.submit();
            });






 });




