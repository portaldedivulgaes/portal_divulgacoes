// Checkbox buttons
var checkboxBtn = document.getElementsByClassName('task-checkbox');
var checkboxBtnActive = document.getElementsByClassName('checkbox-active');

for (var i = 0; i < checkboxBtn.length; i++) {
    checkboxBtn[i].onclick = function() {
        this.parentElement.parentElement.parentElement.onclick = '';
    }
}

for (var i = 0; i < checkboxBtnActive.length; i++) {
    checkboxBtnActive[i].onclick = function() {
        this.parentElement.parentElement.parentElement.onclick = '';
    }
}

// Add buttons to create a subtask
var addSubTaskBtn = document.getElementsByClassName('add-subtask-buttons');
var closeSubTaskBtn = document.getElementsByClassName('subtask-close-button');

for (var i = 0; i < addSubTaskBtn.length; i++) {
    addSubTaskBtn[i].onclick = function() {
        // Delete the parent div
        var parentDiv = this.parentElement;
        parentDiv.style.display = "none";

        // Create a input for the form
        var subTaskInput = document.createElement("input");
        subTaskInput.type = "text";
        subTaskInput.name = "title";
        subTaskInput.placeholder = "Título do item";
        subTaskInput.classList.add("subtask-input-form");

        // Create an input button
        var newItem = document.createElement("input");
        newItem.type = "submit";
        newItem.value = "Adicionar";
        newItem.classList.add("d-none", "d-sm-inline-block", "btn", "btn-sm", "btn-success", "shadow-sm", "subtask-add-button");

        // Create the close button
        var closeBtn = document.createElement("button");
        closeBtn.classList.add("d-none", "d-sm-inline-block", "btn", "btn-sm", "btn-success", "shadow-sm", "subtask-close-button");
        closeBtn.type = "button";

        var closeSpan = document.createElement("span");
        closeSpan.style.width = "13px";
        closeSpan.style.display = "inline-block";

        var closeIcon = document.createElement("i");
        closeIcon.classList.add("fas", "fa-times");

        closeSpan.appendChild(closeIcon);
        closeBtn.appendChild(closeSpan);

        // Create a div to add the previous buttons
        var div = document.createElement("div");
        div.classList.add("subtask-form-buttons");
        div.innerHTML = subTaskInput.outerHTML + "<br>" + newItem.outerHTML + "&nbsp;" + closeBtn.outerHTML;

        var subtaskDiv = this.parentElement.parentElement.getElementsByClassName("subtask-create-form");

        for (var k = 0; k < subtaskDiv.length; k++) {
            subtaskBtn = subtaskDiv[k];
            subtaskBtn.appendChild(div);
        }

        // Delete subtaks-buttons div and Add the parentDiv
        for (var j = 0; j < closeSubTaskBtn.length; j++) {
            closeSubTaskBtn[j].onclick = function() {
                parentDiv.style.display = "block";
                div.style.display = "none";
            }
        }
    }
}

// Add trash icon to delete a subtask item
var taskItem = document.getElementsByClassName('task-item');

for (var i = 0; i < taskItem.length; i++) {
    taskItem[i].onmouseover = function() {
        var subtaskDeleteOption = this.getElementsByClassName('subtask-delete-option');
        subtaskDeleteOption[0].style.opacity = "1";
    }
}

for (var i = 0; i < taskItem.length; i++) {
    taskItem[i].onmouseout = function() {
        var subtaskDeleteOption = this.getElementsByClassName('subtask-delete-option');
        subtaskDeleteOption[0].style.opacity = "0";
    }
}

// Dont trigger the function on taskItem → onclick
var deleteButton = document.getElementsByClassName('subtask-delete-option');

for (var i = 0; i < deleteButton.length; i++) {
    deleteButton[i].onclick = function() {
        this.parentElement.parentElement.onclick = '';
    }
}

// Add a input form to update a subtask
for (var i = 0; i < taskItem.length; i++) {
    taskItem[i].onclick = function test() {
        // Disable the function click previous created
        this.onclick = '';

        // Mouse Cursor Default
        this.style.cursor = "default";

        // Delete the subtask title
        var subtaskTitle = this.getElementsByClassName('task-item-title');
        subtaskTitle[0].style.display = "none";

        // Add trash trash icon
        var subtaskDeleteOption = this.getElementsByClassName('subtask-delete-option');
        subtaskDeleteOption[0].style.opacity = "1";

        // Create a input form to update the subtask title
        var updateInput = document.createElement('input');
        updateInput.type = "text";
        updateInput.name = "title";
        updateInput.placeholder = subtaskTitle[0].textContent;
        updateInput.autofocus = true;
        updateInput.classList.add("subtask-input-form");

        // Create an input button
        var updateBtn = document.createElement("input");
        updateBtn.type = "submit";
        updateBtn.value = "Alterar";
        updateBtn.classList.add("d-none", "d-sm-inline-block", "btn", "btn-sm", "btn-success", "shadow-sm", "subtask-submit-update-button");

        // Create the close button
        var closeUpdateBtn = document.createElement("button");
        closeUpdateBtn.classList.add("d-none", "d-sm-inline-block", "btn", "btn-sm", "btn-success", "shadow-sm", "subtask-close-button");
        closeUpdateBtn.type = "button";

        var closeSpan = document.createElement("span");
        closeSpan.style.width = "13px";
        closeSpan.style.display = "inline-block";

        var closeIcon = document.createElement("i");
        closeIcon.classList.add("fas", "fa-times");

        closeSpan.appendChild(closeIcon);
        closeUpdateBtn.appendChild(closeSpan);

        // Create a div to append the previous buttons
        var buttonDiv = document.createElement("div");
        buttonDiv.classList.add("subtask-update-buttons", "text-right");
        buttonDiv.style.position = "relative";
        buttonDiv.style.bottom = "12px";
        buttonDiv.innerHTML = updateBtn.outerHTML + "&nbsp;" + closeUpdateBtn.outerHTML;

        // Create a div to append the above input + div with the buttons
        var inputDiv = document.createElement("div");
        inputDiv.classList.add("subtask-update-div");
        inputDiv.innerHTML = updateInput.outerHTML + buttonDiv.outerHTML;

        // Append the previous div to a existent form
        var subtaskForm = this.getElementsByClassName("subtask-update-form");

        for (var k = 0; k < subtaskForm.length; k++) {
            subtaskUpdateInput = subtaskForm[k];
            subtaskUpdateInput.appendChild(inputDiv);
        }

        var buttonCloseSubTaskDiv = document.getElementsByClassName('subtask-close-button');

        // On click don't display Edit Div
        for (var j = 0; j < buttonCloseSubTaskDiv.length; j++) {
            buttonCloseSubTaskDiv[j].onclick = function() {
                subtaskTitle[0].style.display = "inline";
                subtaskForm[0].style.display = "none";
                taskItem[0].style.cursor = "pointer";
                taskItem[0].onclick = test();
            }
        }
    }
}
