var note = document.getElementsByClassName('note');

for (var i = 0; i < note.length; i++) {
    note[i].onmouseover = function() {
        var deleteButton = this.getElementsByClassName('note-delete-button');
        deleteButton[0].style.opacity = "1";
    }
}

for (var i = 0; i < note.length; i++) {
    note[i].onmouseleave = function() {
        var deleteButton = this.getElementsByClassName('note-delete-button');
        deleteButton[0].style.opacity = "0";
    }
}

for (var i = 0; i < note.length; i++) {
    note[i].onclick = function addUpdateForm() {

        this.onclick = '';

        var noteTitle = this.getElementsByClassName('note-title');
        noteTitle[0].style.display = "none";

        var inputUpdate = document.createElement('input');
        inputUpdate.type = "text";
        inputUpdate.name = "title";
        inputUpdate.placeholder = noteTitle[0].textContent;
        inputUpdate.classList.add("note-update-input");

        var buttonUpdate = document.createElement('input');
        buttonUpdate.type = "submit";
        buttonUpdate.value = "Alterar";
        buttonUpdate.classList.add("d-none", "d-sm-inline-block", "btn", "btn-sm", "btn-success", "shadow-sm", "note-update-button");

        // Create the close button
        var closeUpdateBtn = document.createElement("button");
        closeUpdateBtn.classList.add("d-none", "d-sm-inline-block", "btn", "btn-sm", "btn-success", "shadow-sm", "note-close-update-button");
        closeUpdateBtn.type = "button";

        var closeSpan = document.createElement("span");
        closeSpan.style.width = "13px";
        closeSpan.style.display = "inline-block";

        var closeIcon = document.createElement("i");
        closeIcon.classList.add("fas", "fa-times");

        closeSpan.appendChild(closeIcon);
        closeUpdateBtn.appendChild(closeSpan);

        // Create a div to append the previous buttons
        var buttonDiv = document.createElement("div");
        buttonDiv.style.position = "relative";
        buttonDiv.style.marginTop = "5px";
        buttonDiv.innerHTML = buttonUpdate.outerHTML + "&nbsp;" + closeUpdateBtn.outerHTML;

        var div = document.createElement("div");
        div.innerHTML = inputUpdate.outerHTML + buttonDiv.outerHTML;

        var updateForm = this.getElementsByClassName("note-update-form");

        for (var k = 0; k < updateForm.length; k++) {
            test = updateForm[k];
            test.appendChild(div);
        }

        var buttonCloseUpdateDiv = document.getElementsByClassName('note-close-update-button');

        // On click don't display Edit Div
        for (var j = 0; j < buttonCloseUpdateDiv.length; j++) {
            buttonCloseUpdateDiv[j].onclick = function() {
                noteTitle[0].style.display = "inline list-item";
                updateForm[0].style.display = "none";
                note[0].style.cursor = "pointer";
            }
        }
    }
}


var deleteButton = document.getElementsByClassName('note-delete-button');

for (var i = 0; i < deleteButton.length; i++) {
    deleteButton[i].onclick = function() {
        this.parentElement.parentElement.parentElement.parentElement.onclick = '';
    }
}
