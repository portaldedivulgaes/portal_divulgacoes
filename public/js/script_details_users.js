
    $(document).ready(function() {

      $('#dataTable').DataTable( {

        "columnDefs": [
          { "width": "100px", "targets": 0 },
          { "width": "150px", "targets": 1 },
          { "width": "auto", "targets": 2 },
          { "width": "150px", "targets": 3 },

          { "orderable": true,"targets": 3 },


          { "width": "150px", "targets": 4 },
          { "width": "110px", "targets": 5 },
          { "orderable": false,"targets": 5 },
        ],

        "language": {
            "lengthMenu":  "Mostrar _MENU_ registos por página",
            "search":      "Procurar:",
            "zeroRecords": "Sem registos",
            "paginate": {
              "first":      "Primeiro",
              "last":       "Ultimo",
              "next":       "Proximo",
              "previous":   "Anterior"
          },

            "info": "A mostrar página _PAGE_ de _PAGES_",
            "infoEmpty": "Sem registos disponiveis",
            "infoFiltered": "(filtrado de um total de _MAX_ registos)"
        },


        "order": [[ 3, "desc" ]]

    } );

 });




