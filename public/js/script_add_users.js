
    $(document).ready(function() {

        var role=false;
        var service=false;
        var userid=false;



        // Inicializa a utilização das tooltips
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
          })



          // Verifica as condições iniciais

        if ($('#user').val()!=""){
            userid=true;
        }

        if ($('#role').val()!=""){
            role=true;
        }


        if ( $('#role').val()=="Admin"){
            $('#service_id').prop('disabled', true);
            $("#service_id").val("0");
            service=true;
        }

        if ( $('#role').val()=="0"){
            $('#service_id').prop('disabled', true);
            $("#service_id").val("-2");
        }




        //  Valida o INUPT user
        $('#user').on('keypress', function (event) {
            var regex = new RegExp("^[a-zA-Z0-9._-]+$");
            var key = String.fromCharCode(!event.charCode ? event.which : event.charCode);

            if (!regex.test(key)) {
            event.preventDefault();
            return false;
            }
        });




     // Mudança de perfil
      $('#role').on('change', function() {
          if ( $('#role').val()=="Admin"){
              $('#service_id').prop('disabled', true);
              $("#service_id").val("0");

          }else{
              $("#service_id").val("-1");
              $('#service_id').prop('disabled', false);
          }

        });




        // Funcionalidade de escolher a imagem
      $(document).on("click", ".browse", function() {

        var file = $(this).parents().find(".file");
        file.trigger("click");
      });
      $('input[type="file"]').change(function(e) {
        var fileName = e.target.files[0].name;


        // Verifica se o tamanho da imagem não passa os 2MB
        if (e.target.files[0].size > 2097152 ){ //Bytes
            $("#fileHelp").addClass("warning_img_size");
            $("#fileHelp").focus();
            fileName=null;

            // Se a imagem for maior que 2MB, repoem a img default
            document.getElementById("preview").src ="/img/default_user.png";
            $("#inputPhoto").val(null)

            return ;
        }else{
            $("#fileHelp").removeClass("warning_img_size");
        }

        $("#file").val(fileName);

        var reader = new FileReader();
        reader.onload = function(e) {
          // get loaded data and render thumbnail.
          document.getElementById("preview").src = e.target.result;
        };
        // read the image file as a data URL.
        reader.readAsDataURL(this.files[0]);
      });


      $(".img-thumbnail").click(function(){
        var file = $(this).parents().find(".file");
        file.trigger("click");
      });


      //reset para imagem de origem
      $("#btn_remove_photo").click(function(){
        document.getElementById("preview").src ="/img/default_user.png";
        $("#inputPhoto").val(null)
        $("#file").val(null);
      });





      // Valida form antes de submeter


      $("#buttonSubmit").click(function(){


         //campo da identificação
         if ($('#user').val().length<4){
            $('#user').attr("placeholder", "Insira uma identificação válida");
            $("#user").addClass("field_focus");
            setTimeout(field_focus, 1400);
            return
        } else {
            userid=true;
        }


        //campo do perfil
        if ($('#role').val()=="0"){
            $("#role").addClass("field_focus");
            setTimeout(field_focus, 1400);
            return
        } else {
            role=true;
        }

        //campo do gabinete
        if ($('#service_id').val()=="-1"){

            $("#service_id").addClass("field_focus");
            setTimeout(field_focus, 1400);
            return
        } else {
            service=true;
        }

        if (role==true && service==true && userid==true){
            $("#form_user").submit();
        }

      });


      // Remove a class da animação CSS
      function field_focus(){
        $("#user").removeClass("field_focus");
        $("#role").removeClass("field_focus");
        $("#service_id").removeClass("field_focus");
    }



 });




