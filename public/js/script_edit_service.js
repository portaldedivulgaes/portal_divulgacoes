
$(document).ready(function() {


    // Letras maiusculas para as iniciais do gabinete
    $('#initial').keyup(function(){
        $(this).val($(this).val().toUpperCase());
    });



    // Apenas Letras e espaços no input do NOME
    $("#name").keypress(function (e) {
        var keyCode = e.keyCode || e.which;

        //Regex for Valid Characters i.e. Alphabets.
        var regex = /^[A-Za-zÀ-ÖØ-öø-ÿ\s]*$/;

        //Validate TextBox value against the Regex.
        var isValid = regex.test(String.fromCharCode(keyCode));
        return isValid;
    });


        // Apenas Letras e espaços no input das iniciais
        $("#initial").keypress(function (e) {
            var keyCode = e.keyCode || e.which;

            //Regex for Valid Characters i.e. Alphabets.
            var regex = /^[A-Za-zÀ-ÖØ-öø-ÿ]*$/;

            //Validate TextBox value against the Regex.
            var isValid = regex.test(String.fromCharCode(keyCode));
            return isValid;
        });



    // Auto Initials
    $("#name").focusout(function() {
        let str = $('#name').val();
        let acronym = str.split(/\s/).reduce((response,word)=> response+=word.slice(0,1),'')

        $('#initial').val( acronym );

    });


    // Só permite numeros
    $("#phone").bind("keypress", function (e) {
        var keyCode = e.which ? e.which : e.keyCode
        if (!(keyCode >= 48 && keyCode <= 57)) {
          return false;
        }
    });




    function focus_preview(){
        $("#preview").removeClass("preview_focus");
    }


    // Preview
    $("#buttonPreview").click(function(){
        if ( $('#initial').val()=="" || $('#name').val()=="" || $('#description').val()=="" || $('#email').val()=="" ){
            return;
        }

        $('#mockupName').text( $('#name').val() );
        $('#mockupInitial').text( $('#initial').val() );
        $('#mockupDescription').text( $('#description').val() );
        $('#mockupEmail').text( $('#email').val() );
        if ($('#phone').val()!=""){
            $('#mockupPhone').text('Telefone: '+ $('#phone').val() );
        }else{
            $('#mockupPhone').text(" "+$('#phone').val() );
        }

        $("#preview").addClass( "preview_focus");
        setTimeout(focus_preview, 1500);

      });

});
