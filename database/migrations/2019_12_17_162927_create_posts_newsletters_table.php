<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsNewslettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts_newsletters', function (Blueprint $table) {

            $table->unsignedBigInteger('newsletter_id');
            $table->unsignedBigInteger('post_id');
            $table->primary(['newsletter_id', 'post_id']);
            $table->foreign('newsletter_id')->references('id')->on('newsletters');
            $table->foreign('post_id')->references('id')->on('posts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts_newsletters');
    }
}
