<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->string('user', 40);
            $table->primary('user');
            $table->string('name', 150)->nullable();
            $table->string('email', 150)->unique()->nullable();
            $table->string('photo')->nullable()->default('default_user.png');
            $table->enum('role', ['Admin', 'Employer']);
            $table->timestamps();
            $table->softDeletes();
            $table->rememberToken();
            $table->unsignedBigInteger('service_id')->nullable();
              $table->foreign('service_id')->references('id')->on('services');
        });



        $data = array( // Utilizadores incluidos APENAS para efeitos de testes e apresentação.
            //array('user'=>'', 'name'=> '','email'=>'','role'=>'','created_at'=>'2019-12-20 00:00:00','updated_at'=>'2019-12-20 00:00:00')

            //profs
            array('user'=>'eugenia.bernardino', 'name'=> 'Eugenia Bernardino','email'=>'eugenia.bernardino@my.ipleiria.pt','role'=>'Admin','created_at'=>'2019-1-30 00:00:00','updated_at'=>'2019-1-30 00:00:00'),
            array('user'=>'eudardo.silva', 'name'=> 'Eudardo Silva','email'=>'eudardo.silva@my.ipleiria.pt','role'=>'Admin','created_at'=>'2019-1-30 00:00:00','updated_at'=>'2019-1-30 00:00:00'),
            array('user'=>'micaela.dinis', 'name'=> 'Micaela Dinis','email'=>'micaela.dinis@my.ipleiria.pt','role'=>'Admin','created_at'=>'2019-1-30 00:00:00','updated_at'=>'2019-1-30 00:00:00'),
            array('user'=>'angela.pereira', 'name'=> 'Angela pereira','email'=>'angela.pereira@my.ipleiria.pt','role'=>'Admin','created_at'=>'2019-1-30 00:00:00','updated_at'=>'2019-1-30 00:00:00'),

            // developers
            array('user'=>'2180498', 'name'=> 'Daniel Nunes','email'=>'2180498@my.ipleiria.pt','role'=>'Admin','created_at'=>'2019-12-20 00:00:00','updated_at'=>'2019-12-20 00:00:00'),
            array('user'=>'2180460', 'name'=> 'João Adão','email'=>'2180460@my.ipleiria.pt','role'=>'Admin','created_at'=>'2019-12-20 00:00:00','updated_at'=>'2019-12-20 00:00:00'),
            array('user'=>'2180487', 'name'=> 'José Areia','email'=>'2180487@my.ipleiria.pt','role'=>'Admin','created_at'=>'2019-12-20 00:00:00','updated_at'=>'2019-12-20 00:00:00'),
            array('user'=>'2180500', 'name'=> 'Mariana Pereira','email'=>'2180500@my.ipleiria.pt','role'=>'Admin','created_at'=>'2019-12-20 00:00:00','updated_at'=>'2019-12-20 00:00:00'),
            array('user'=>'2180482', 'name'=> 'Micaela Santos','email'=>'2180482@my.ipleiria.pt','role'=>'Admin','created_at'=>'2019-12-20 00:00:00','updated_at'=>'2019-12-20 00:00:00'),

        );

        DB::table('users')->insert($data); // Query Builder approach
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
