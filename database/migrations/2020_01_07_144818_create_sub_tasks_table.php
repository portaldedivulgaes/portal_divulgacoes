<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubTasksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subTasks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('title');
            $table->enum('status', ['To Do', 'Done'])->default('To Do');;
            $table->timestamps();
            $table->unsignedBigInteger('tasks_id')->nullable();
              $table->foreign('tasks_id')->references('id')->on('tasks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subTasks');
    }
}
