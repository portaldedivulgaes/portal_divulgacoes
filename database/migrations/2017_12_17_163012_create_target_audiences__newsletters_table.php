<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTargetAudiencesNewslettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('targetAudiences_Newsletters', function (Blueprint $table) {

          $table->unsignedBigInteger('newsletter_id');
          $table->unsignedBigInteger('targetaudience_id');
          $table->primary(['newsletter_id','targetaudience_id'], 'targetAudiences_newsletters');
          $table->foreign('newsletter_id')->references('id')->on('newsletters');
          $table->foreign('targetaudience_id')->references('id')->on('targetAudiences');
        });

        $data = array(
            array('name'=>'Estudantes ESTG', 'email'=>'estg@my.ipleiria.pt', 'created_at'=>date("Y-m-d H:i:s")),
            array('name'=>'Funcionários Docentes e Não Docentes ESTG', 'email'=>'funcionarios.estg@ipleiria.pt', 'created_at'=>date("Y-m-d H:i:s")),
            array('name'=>'Funcionários Docentes', 'email'=>'docentes.estg@ipleiria.pt', 'created_at'=>date("Y-m-d H:i:s")),
            array('name'=>'Funcionários Não Docentes', 'email'=>'naodocentes.estg@ipleiria.pt', 'created_at'=>date("Y-m-d H:i:s")),
        );

        DB::table('targetAudiences')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('targetAudiences_Newsletters');
    }
}
