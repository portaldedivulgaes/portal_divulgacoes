<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('name', 120);
            $table->string('initial', 20);
            $table->text('description')->nullable();
            $table->string('email', 250);
            $table->string('phone', 150)->nullable();
            $table->timestamps();



        });

        // ADD dos Gabinetes à base de Dados
        // id, name, initial, description, email, phone, created_at, updated_at

        $data = array(
            array('id'=>'1', 'name'=> 'Gabinete de Apoio à Investigação & Desenvolvimento','initial'=>'GAI&D','description'=>'Divulga novas oportunidades relacionadas com a Investigação e Desenvolvimento','email'=>'gaid.estg@ipleiria.pt','phone'=>'244820797','created_at'=>'2019-12-20 00:00:00','updated_at'=>'2019-12-20 00:00:00'),
            array('id'=>'2', 'name'=> 'Gabinete de Imagem e Relações com o Exterior','initial'=>'GIRE','description'=>'Dedica-se à partilha de informações gerais de interesse para toda a ESTG','email'=>'gire.estg@ipleiria.pt','phone'=>'244820797','created_at'=>'2019-12-20 00:00:00','updated_at'=>'2019-12-20 00:00:00'),
            array('id'=>'3', 'name'=> 'Gabinete Internacional ESTG','initial'=>'INTERNACIONAL','description'=>'Fornece informações de interesse aos alunos internacionais','email'=>'internacional.estg@ipleiria.pt','phone'=>'244820797','created_at'=>'2019-12-20 00:00:00','updated_at'=>'2019-12-20 00:00:00')
        );

        DB::table('services')->insert($data); // Query Builder approach



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('services');
    }
}
