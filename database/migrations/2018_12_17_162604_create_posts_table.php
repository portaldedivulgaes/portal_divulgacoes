<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('title', 150);
            $table->string('resume', 150);
            $table->text('description');
            $table->enum('type', ['Post', 'Event']);
            $table->string('image', 150)->nullable();
            $table->string('poster', 150)->nullable();
            $table->dateTime('startDate')->nullable();
            $table->dateTime('endDate')->nullable();
            $table->string('location', 150)->nullable();
            $table->string('link', 250)->nullable();
            $table->dateTime('validity')->nullable();
            $table->boolean('important');
            $table->enum('status', ['Published', 'Draft', 'Archived']);
            $table->string('tags', 250)->nullable();
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('eventtype_id');
              $table->foreign('eventtype_id')->references('id')->on('eventTypes');
            $table->unsignedBigInteger('service_id')->nullable();
              $table->foreign('service_id')->references('id')->on('services');
            $table->string('user', 40);
              $table->foreign('user')->references('user')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
