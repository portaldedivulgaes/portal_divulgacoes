<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('eventTypes', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->string('name', 150);
            $table->string('icon', 50);
            $table->timestamps();
            $table->softDeletes();
            $table->unsignedBigInteger('service_id');
              $table->foreign('service_id')->references('id')->on('services');
        });

        $data = array(
            array('name'=>'Congresso', 'icon'=>'fas fa-graduation-cap', 'service_id'=>1, 'created_at'=>date("Y-m-d H:i:s")),
            array('name'=>'Palestra', 'icon'=>'fas fa-graduation-cap', 'service_id'=>1, 'created_at'=>date("Y-m-d H:i:s")),
            array('name'=>'Seminário', 'icon'=>'fas fa-graduation-cap', 'service_id'=>2, 'created_at'=>date("Y-m-d H:i:s")),
            array('name'=>'Aula Aberta', 'icon'=>'fas fa-graduation-cap', 'service_id'=>2, 'created_at'=>date("Y-m-d H:i:s")),
            array('name'=>'Workshop', 'icon'=>'fas fa-graduation-cap', 'service_id'=>3, 'created_at'=>date("Y-m-d H:i:s")),
            array('name'=>'Jornada', 'icon'=>'fas fa-graduation-cap', 'service_id'=>3, 'created_at'=>date("Y-m-d H:i:s")),
        );

        DB::table('eventTypes')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('eventTypes');
    }
}
