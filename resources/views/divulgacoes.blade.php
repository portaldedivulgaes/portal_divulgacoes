@extends('layout.master')
@section('title', $service_name)

@section('titleHeader', 'Divulgações '.$service_name)


<!-- CSS Link -->
<link href="{{asset('css/style_divulgacoes.css')}}" rel="stylesheet">

@section('content')

    @include('layout.partials.frontoffice.header')

    <div class="container">
        <div class="row mb-3 btnSearch">
            <form class="input-group col-md-6 col-md-12" action="/divulgacoes/{{$service_name}}" method="get">
                <input name="search" class="form-control" id="keywords" onkeyup="timeOut()" type="text"
                       value="{{ $search }}"
                       placeholder="Pesquise aqui...">
                <button type="submit" id="btnFilter" class="btn btn-light justify-content-end ml-1"
                        data-toggle="collapse">Pesquisar
                </button>
                <a type="reset" class="btn btn-light justify-content-end ml-1"
                   href="{{route('divulgacoes', $service_name)}}">Voltar</a>
            </form>
        </div>

        <br>
        <!-- Secção com o Conteúdo -->

        <div class="row">
                @if (count($posts))
                    @foreach($posts as $post)
                        @if (empty($post->deleted_at))
                            <div class="col-lg-4 col-md-6">
                                <div class="card cartao">
                                    <div class="inner">
                                        <div class="card-img-to card_img animatic_img" style="background-image: url('{{ $post->image!='' ? asset('/storage/posts_images/'.$post->image) : route('divulgacoes.svgThumb', $post->id) }}')"></div>
                                    </div>
                                    <div class="card-body">
                                        <h5 class="card-title text-truncate">{{$post->title}}</h5>
                                        <p class="card-text text-truncate">{{$post->resume}}</p>
                                    </div>
                                    <div class="card-footer footercard d-flex justify-content-end align-items-center">
                                        <a href="{{route('divulgacao', [$post->service->initial, $post->id])}}"
                                           class="btn btn-success button botao_gradiente botao_gradiente_fundo">Ver
                                            Mais </a>
                                    </div>
                                </div>
                            </div>
                        @endif
                    @endforeach
                @else
                    <h6 class="msgDivulgacoes">Não Existe Publicações Disponíveis!</h6>
                @endif

            <!--Paginação-->

            <div class="col-lg-12">
                <ul class="pagination justify-content-center text-black">
                    {{ $posts->links() }}
                </ul>
            </div>
        </div>
    </div>

    <script>
        sal();
    </script>
@endsection
