@extends('layout.master')

@section('title', $post->title." | ".$post->service->initial )

@section('titleHeader')
    Divulgação {{strtoupper($post->service->initial)}}
@endsection

@section('tags', $post->tags)

<link href="{{ asset('css/style_divulgacao.css') }}" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Poppins:400,600,700&display=swap" rel="stylesheet">

@section('content')

    @include('layout.partials.frontoffice.header')

    <div class="container">
        <h1> {{$post->title}}</h1>
        <div class="row">
            <div class="col-lg-6 order-last order-lg-first">
                <p>{{$post->resume}}</p>
            </div>
            <div class="col-lg-2">
                <br>
            </div>
            <div class="col-lg-4 order-first order-lg-last">
                @isset($post->startDate)<p>
                    <b>Data: </b><?=date('d/m/Y', strtotime($post->startDate))?><?php if (isset($post->endDate)) {
                        echo " - " . date('d/m/Y', strtotime($post->endDate));}?></p>@endisset
                @isset($post->location)<p><b>Localização: </b>{{$post->location}}</p>@endisset
                @isset($post->link)<p><b>Link: </b> <a href="{{$post->link}}" target="_blank"
                                                       class="divulgacao_link">{{$post->link}}</a>@endisset
                </p>
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-12">
                @if ($post->image == null)
                    <img src="{{route('divulgacoes.svg', $post->id)}}" class="card-img-to card_img animatic_img">
                @else
                    <img src="{{asset('/storage/posts_images/'.$post->image)}}" class="img-fluid" alt="Imagem de capa">
                @endif
            </div>
        </div>
        <br><br>
        <div class="row">
            <div class="col-12">
                <p>
                    <br><br> {!! $post->description !!}
                </p>
            </div>
        </div>
        <br>
        <br>
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    @if ($post->poster == null)

                    @else
                        <img src="{{asset('/storage/posts_posters/'.$post->poster)}}" class="img-fluid" id="cartaz"
                             alt="Imagem do Cartaz">
                    @endif
                </div>
            </div>
        </div>
        <div class="row">
            <div class="imagem-programa" data-target="#exampleModal" data-toggle="modal">
                @if ($post->poster == null)

                @else
                    <img style="cursor: zoom-in; width: 40%; margin-left: 11px" src="{{asset('/storage/posts_posters/'.$post->poster)}}" class="img-fluid" id="cartaz"
                         alt="Imagem do Cartaz">
                @endif
            </div>
            <div class="col-lg-1"><br> <br></div>
            <div>
            </div>
        </div>
    </div>
@endsection
