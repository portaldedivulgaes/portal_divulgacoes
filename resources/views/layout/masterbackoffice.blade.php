<!DOCTYPE html>
<html lang="pt-PT">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    @yield('meta')

    <title> @yield('title') | Backoffice | Portal de Divulgações</title>

    <!-- Favicon -->
    <link rel="shortcut icon" href="/img/icon_logo.png" type="image/x-icon">

    <!-- Fonts -->
    <link href="{{asset('/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

    <!-- CSS Links -->
    <link href="{{asset('/css/sb-admin-2.css')}}" rel="stylesheet">
    <link href="{{asset('/css/style_notifications.css')}}" rel="stylesheet">

    <!-- Sal JS -->
    <link rel="stylesheet" href="{{asset('vendor/sal.js/dist/sal.css')}}">

    @yield('styleLinks')

</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">


        @include('layout.partials.backoffice.sidebar')

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content">

                @include('layout.partials.backoffice.topbar')



                <!-- Begin Page Content -->
                <div class="container-fluid">
                    @if ($errors->any())
                    @include ('backoffice.partials.errors')
                    @endif
                    @if (!empty(session('success')))
                    @include ('backoffice.partials.success')
                    @endif
                    @yield('content')

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Scroll to Top Button-->
            <a class="scroll-to-top rounded" href="#page-top">
                <i class="fas fa-angle-up"></i>
            </a>

            <!-- Logout Modal-->
            <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Deseja mesmo sair?</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">Clique em "Terminar sessão" para confirmar.</div>
                        <div class="modal-footer">
                            <button class="btn btn-sm btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                            <a class="btn  btn-sm btn-primary" href="{{ route('logout')}}">Terminar sessão</a>
                        </div>
                    </div>
                </div>

            </div>

            @include('layout.partials.backoffice.footer')

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->




    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('/js/popper.min.js')}}"></script>
    <script src="{{asset('/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{asset('/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{asset('/js/sb-admin-2.min.js')}}"></script>

    <!-- Sal JS -->
    <script src="{{asset('vendor/sal.js/dist/sal.js')}}"></script>

    <!-- Data Tables-->
    <script src="{{asset('/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    {{-- Script das Notifications --}}
    <script src="{{asset('js/script_notifications.js')}}"></script>

    <!-- Individual Scripts -->
    @yield('scripts')

</body>

</html>
