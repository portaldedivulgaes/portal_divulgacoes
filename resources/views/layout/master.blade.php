<!DOCTYPE html>
<html lang="pt" dir="ltr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title> @yield('title') | Portal de Divulgações</title>

    <meta name="keywords" content="Portal de Divulgações, ESTG, IPLeiria, Leiria, Politécncio de Leiria, GIRE, GAI&D, Internacional, @yield('tags')">

    <!-- Favicon -->
    <link rel="shortcut icon" href="/img/icon_logo.png" type="image/x-icon">

    <!-- Sal JS -->
    <link rel="stylesheet" href="{{asset('vendor/sal.js/dist/sal.css')}}">

    <!-- CSS Link -->
    <link href="{{asset('css/style_master.css')}}" rel="stylesheet">
    <!-- Bootstrap core CSS -->
    <link href="{{asset('vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- Fontawesome core CSS -->
    <link href="{{asset('vendor/fontawesome-free/css/all.min.css')}}" rel=" stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans:300,500&display=swap" rel="stylesheet">
</head>

<body onload="loading()">
    <script src="{{asset('vendor/sal.js/dist/sal.js')}}"></script>
    <script src="{{asset('vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Loader -->
    <div id="loaderBg">
        <div id="loader"></div>
    </div>

    @include('layout.partials.frontoffice.nav')

    @yield('content')

    @include('layout.partials.frontoffice.footer')

    <script>
        var loader = document.getElementById('loader');
        var loaderBg = document.getElementById('loaderBg');

        function loading() {
            loader.style.opacity = "0";
            loaderBg.style.opacity = "0";
            loaderBg.style.zIndex = "-1";
        }
    </script>
</body>

</html>
