<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="/backoffice">
        <div class="sidebar-brand-icon">
            <img src="/img/icon_logo2.png" alt="iconLogo" style="width:35px;">
        </div>
        <div class="sidebar-brand-text">
            <img src="/img/logotipo-portal-de-divulgacoes_para-fundo-branco2.png" alt="logotipo" style="width:120px;">
        </div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Dashboard -->
    <li class="nav-item {{Route::is('home') ? 'active' : ''}}">
        <a class="nav-link" href="{{route('home')}}">
            <i class="fas fa-tachometer-alt"></i>
            <span> Dashboard</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Gerir Publicações -->
    <div class="sidebar-heading">
        Gerir Publicações
    </div>

    <!-- Publicações -->
    <li class="nav-item @isset ($menuPostType) @isset ($menuPostStatus) @if ($menuPostStatus!='Draft' && Route::is('posts.*') || Route::is('events.*')) active @endif @endisset @endisset"  >
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true"
           aria-controls="collapseTwo">
            <i class="fas fa-calendar fa-fw"></i>
            <span> Publicações</span>
        </a>
        <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
            <div id="menu-posts" class="bg-white py-2 collapse-inner rounded">
                <h6 class="collapse-header">Tipos de Publicação:</h6>
                <a href="{{route('posts.index')}}" class="collapse-item @isset ($menuPostType) @isset ($menuPostStatus) @if ($menuPostType == 'Post' && $menuPostStatus != 'Draft') active @endif @endisset @endisset">
                    <span> Divulgações</span>
                </a>
                <a href="{{route('events.index')}}" class="collapse-item @isset ($menuPostType) @isset ($menuPostStatus) @if ($menuPostType == 'Event' && $menuPostStatus != 'Draft') active @endif @endisset @endisset">
                    <span> Eventos</span>
                </a>
            </div>
        </div>
    </li>

    <!-- Rascunhos -->
    <li class="nav-item  @if(Route::is('drafts.*')) active @endif @isset ($menuPostStatus) @if ($menuPostStatus == 'Draft') active @endif @endisset">
        <a href="{{route('drafts.index')}}" class="nav-link ">
            <i class="fas fa-archive fa-fw"></i><span> Rascunhos</span></a>
    </li>

    <!-- Newsletters -->
    <li class="nav-item {{Route::is('newsletters.*') ? 'active' : ''}}">
        <a href="{{route('newsletters.index')}}" class="nav-link">
            <i class="fas fa-envelope fa-fw"></i>
            <span> Newsletters</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Ferramentas -->
    <div class="sidebar-heading"> Ferramentas</div>

    <!-- Tipo de Divulgação -->
    <li class="nav-item {{Route::is('eventtypes.*') ? 'active' : ''}}">
        <a href="{{route('eventtypes.index')}}" class="nav-link">
            <i class="fas fa-clone fa-fw"></i>
            <span> Tipo de Publicação</span>
        </a>
    </li>

    <!-- Público Alvo -->
    <li class="nav-item {{Route::is('targetaudiences.*') ? 'active' : ''}}">
        <a href="{{route('targetaudiences.index')}}" class="nav-link">
            <i class="fas fa-users-cog fa-fw"></i>
            <span> Público Alvo</span>
        </a>
    </li>

    <!-- Estatisticas -->
    <li class="nav-item {{Route::is('statistics.*') ? 'active' : ''}}">
        <a href="{{route('statistics.index')}}" class="nav-link">
            <i class="fas fa-chart-area fa-fw"></i>
            <span> Estatísticas</span>
        </a>
    </li>


    <!-- Users -->


    @if (Auth::user()->role=="Admin" )
        <li class="nav-item {{Route::is('users.*') ? 'active' : ''}}">
            <a href="{{route('users.index')}}" class="nav-link">
                <i class="fas fa-user-friends fa-fw"></i>
                <span> Utilizadores</span>
            </a>
        </li>
    @endif


    <li class="nav-item {{Route::is('tasks.*') ? 'active' : ''}}">
        <a href="{{route("tasks.index")}}" class="nav-link">
            <i class="fas fa-tasks fa-fw"></i>
            <span> Tarefas</span>
        </a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->
