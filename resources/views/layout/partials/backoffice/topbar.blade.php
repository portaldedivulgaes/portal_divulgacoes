<!-- Topbar -->
<nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

  <!-- Sidebar Toggle (Topbar) -->
  <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
    <i class="fa fa-bars"></i>
  </button>

  <!-- Sidebar Toggle (Topbar) -->
  <span class="ml-2">
    @if (Auth::user()->role!="Admin" )
      Gabinete {{Auth::user()->serviceToStr()}}
    @else
      Administrador
    @endif
  </span>
  <!-- Topbar Navbar -->
  <ul class="navbar-nav ml-auto">

    <!-- Nav Item - Search Dropdown (Visible Only XS) -->
    <li class="nav-item dropdown no-arrow d-sm-none">
      <a class="nav-link dropdown-toggle" href="#" id="searchDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-search fa-fw"></i>
      </a>
      <!-- Dropdown - Messages -->
      <div class="dropdown-menu dropdown-menu-right p-3 shadow animated--grow-in" aria-labelledby="searchDropdown">
        <form class="form-inline mr-auto w-100 navbar-search">
          <div class="input-group">
            <input type="text" class="form-control bg-light border-0 small" placeholder="Procurar por..." aria-label="Search" aria-describedby="basic-addon2">
            <div class="input-group-append">
              <button class="btn btn-primary" type="button">
                <i class="fas fa-search fa-sm"></i>
              </button>
            </div>
          </div>
        </form>
      </div>
    </li>
    <li class="nav-item dropdown no-arrow mx-1">
        <a class="nav-link dropdown-toggle" href="{{route('index')}}" target="_blank">
          <i class="fas fa-desktop"></i>
        </a>
    </li>
    @if (Auth::user()->role!="Admin" )
          <?php $notifications = Auth::user()->service->unreadNotifications ?>
    <!-- Nav Item - Alerts -->
    <li class="nav-item dropdown no-arrow mx-1">
      <a class="nav-link dropdown-toggle" href="#" id="alertsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <i class="fas fa-bell fa-fw"></i>
        <!-- Counter - Alerts -->
        @if(count($notifications)>0)
          <span class="badge badge-danger badge-counter">{{count($notifications)>10?'10+':count($notifications)}}</span>
        @endif
      </a>

      <!-- Dropdown - Alerts -->
      <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="alertsDropdown">
        <h6 class="dropdown-header">
          Notificações
        </h6>
        @if (count($notifications)>0)
          @foreach ($notifications as $notification)
            <div class="dropdown-item d-flex align-items-center">
              <div class="mr-3">
                <form class="remove_notification_form" action="" method="POST">
                  @method('PUT')
                  @csrf
                  <button type="submit" data-id='{{$notification->id}}' class="icon-circle">
                    <div class="icons">
                      <i class="fas fa-envelope text-white icon-default"></i>
                      <i class="fas fa-trash-alt text-white icon-hover"></i>
                    </div>
                  </button>
                </form>
              </div>
              <div style="width:100%;">
                <div class="small text-gray-500">{{date('H\hi  - d/m/Y', strtotime($notification->created_at))}}</div>
                Nova mensagem de <b>{{$notification->data['name']}}</b><br>
                Assunto: <b>{{$notification->data['subject']}}</b><br>
                <div class="text-right small text-gray-500">Consulte o Email do Gabinete {{Auth::user()->service->initial}}</div>
              </div>
            </div>
          @endforeach
        @else
          <p class='text-center small text-gray-500' style="margin:17px;">Não tem Notificações</p>
        @endif
      </div>

    </li>

  @endif

    <li class="nav-item">

    </li>


    <div class="topbar-divider d-none d-sm-block"></div>

    <!-- Nav Item - User Information -->
    <li class="nav-item dropdown no-arrow">
      <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ Auth::user()->name }}</span>
        <img class="img-profile rounded-circle" src="{{Storage::disk('public')->url('users_photos/').Auth::user()->photo}}">
        {{--                <img class="img-profile rounded-circle" src="{{asset('/storage/users_photos').'/'.Auth::user()->photo}}">--}}
      </a>
      <!-- Dropdown - User Information -->
      <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
        <a class="dropdown-item" href="{{route('options',Auth::user()->user)}}">
          <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
          Definições
        </a>
        <a class="dropdown-item" href="{{route('faqs')}}">
          <i class="fas fa-question-circle mr-2 text-gray-400"></i>
          Ajuda
        </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" href="" data-toggle="modal" data-target="#logoutModal">
          <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
          Terminar sessão
        </a>
      </div>
    </li>

  </ul>

</nav>
<!-- End of Topbar -->
