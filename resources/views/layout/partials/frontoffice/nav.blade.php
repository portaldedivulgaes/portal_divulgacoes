<!-- CSS Link -->
<link rel="stylesheet" href="{{asset('css/style_navbar.css')}}">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,600,600i,700,700i,800&display=swap" rel="stylesheet">

<!-- Button/Icon Back to Top -->
<i class="fas fa-arrow-alt-circle-up" id="backToTop" title="Ir para o Topo"></i>


<div class="container" id="nav_container">
    <nav class="navbar navbar-expand-lg">
        <a class="navbar-brand" href="/">
            <img src="{{asset('img/logotipo-portal-de-divulgacoes_para-fundo-branco.png')}}" alt="Lógotipo - Portal de Divulgações" class="logo">
        </a>
        {{--<span class="buttonSideNav" style="font-size:30px; cursor:pointer; color: white;" onclick="openNav()">&#9776;</span>--}}
        <div class="collapse navbar-collapse justify-content-end" id="navbarNavDropdown">
            <ul class="navbar-nav containerNav">
                <li class="nav-item">
                    <a class="nav-link line_nav" href="/">Início</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link line_nav" href="/sobre">Sobre</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link line_nav" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        Divulgações <i class="fas fa-sort-down"></i>
                    </a>
                    <div class="dropdown-menu dropdown-nav" aria-labelledby="navbarDropdownMenuLink">
                        @foreach ($services as $service)
                        <a class="dropdown-item itemdown" href="{{route('divulgacoes', $service->initial)}}">{{$service->initial}}</a>
                        @endforeach
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link line_nav" href="/contactos">Contactos</a>
                </li>
            </ul>
        </div>
    </nav>
</div>
<div id="mySidenav" class="sidenav">
    <nav class="main-nav float-right d-none d-lg-block">
        <ul class="navbar-nav">
            <li class="nav-item line_sidebar"><a class="nav-link" href="/">Início</a></li>
            <li class="nav-item line_sidebar"><a class="nav-link" href="/sobre">Sobre</a></li>
            <li class="drop-down line_sidebar"><a class="nav-link" href="">Divulgações <i class="fas fa-sort-down"></i></a>
                <ul>
                    @foreach ($services as $service)
                    <li class="line_sidebar"> <a class="dropdown-item" href="{{route('divulgacoes', $service->initial)}}">{{$service->initial}}</a></li>
                    @endforeach
                </ul>
            </li>
            <li class="nav-item line_sidebar"><a class="nav-link" href="/contactos">Contactos</a></li>
        </ul>
        <img src="/img/logotipo-portal-de-divulgacoes_para-fundo-branco.png" alt="Logótipo - Portal de Divulgações" class="logo_sidenav">

    </nav>
</div>


<script src="/js/script_nav.js"></script>
