<link rel="stylesheet" href={{asset('/css/style_header.css')}}>

<section class="background">
    <svg version="1.1" id="curve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 88" style="enable-background:new 0 0 1920 117;" xml:space="preserve">
        <path class="st0" d="M0,45c0,0,138-35,473-35s601,35,909,35s538-45,538-45v89H0V45z"/>
    </svg>
    <h1 class="backgroundTitle">@yield('titleHeader')</h1>
</section>
