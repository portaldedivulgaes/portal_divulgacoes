<link href="{{ asset('css/style_footer.css') }}" rel="stylesheet">

<footer class="footer page-footer font-small pt-4 py-2 bg-dark mt-5">
    <div class="container">

        {{-- Div logotipo + menu --}}
        <div class="row">
            <div class="col-12 col-md-3 col-lg-2" id="logofooter">
                <a href="{{route('index')}}">
                    <img src="{{asset('img/logotipo-portal-de-divulgacoes_para-fundo-branco.png')}}" alt="Logótipo do Portal de Divulgações">
                </a>
            </div>

            <div class="col-12 col-md-9 col-lg-10 miniMenu mb-5">
                <ul class="list-inline small">
                    <li class="list-inline-item"><a href="/">Início</a></li>
                    <li class="list-inline-item"><a href="/sobre">Sobre</a></li>
                    <ul class="dropdown">
                    <li class="list-inline-item dropbtn" style="color: #81898f;">Divulgações</li>
                    <li class="dropdown-content" style="list-style-type:none;">
                        @foreach ($services as $service)
                        <a href="{{route('divulgacoes', $service->initial)}}">{{$service->initial}}</a>
                        @endforeach
                    </li>
                  </ul>
                    <li class="list-inline-item"><a href="/contactos">Contactos</a></li>
                </ul>
            </div>
        </div>

        <hr>

        {{-- Div Copyright + Social Media --}}
        <div class="row">
            {{-- Copyright text --}}
            <div class="col-md-7 col-lg-8 small text-muted">
                <p class="text-center text-md-left">©
                    <script>
                        document.write(new Date().getFullYear());
                    </script>
                    Copyright. Portal de Divulgações</p>
            </div>

            {{-- Social Media Icons --}}
            <div class="col-md-5 col-lg-4 ml-lg-0">

                <div class="text-center text-md-right">
                    <ul class="list-unstyled list-inline">
                        {{-- Instagram --}}
                        <li class="list-inline-item">
                            <a href="https://www.instagram.com/ipleiriashop/" class="btn-floating mx-2" target="_blank">
                                <i class="fab fa-instagram" title="Portal de Divulgações no Instagram"></i>
                            </a>
                        </li>
                        {{-- Facebook --}}
                        <li class="list-inline-item">
                            <a href="https://www.facebook.com/IPLeiriaShop-109581130505780/" class="btn-floating btn-md mx-2" target="_blank">
                                <i class="fab fa-facebook-f" title="Portal de Divulgações no Facebook"></i>
                            </a>
                        </li>
                        {{-- Youtube --}}
                        <li class="list-inline-item">
                            <a href="https://www.youtube.com/" class="btn-floating mx-2" target="_blank">
                                <i class="fab fa-youtube" title="Portal de Divulgações no Youtube"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
</footer>
