<?xml version="1.0" encoding="utf-8"?>
<!-- Generator: Adobe Illustrator 24.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0)  -->
<svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
viewBox="0 0 1180 398" style="enable-background:new 0 0 1180 398;" xml:space="preserve">

<style type="text/css">
.st0{fill:<?php if($post->service_id==1)echo "#ACE087"; else if($post->service_id==2) echo "#F4CF5D"; else echo "#8BDFE2";?>;}
.st1{fill:none;stroke:#000000;stroke-width:3;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
.st2{fill:url(#SVGID_1_);}
.st3{fill:#FFFFFF;}
.st4{font-family:'Calibri'; font-weight:lighter;}
.st5{font-size:21px;}
.st6{font-family:'Calibri'; font-weight: bold;}
.st7{font-size:45px;}
.st8{font-size:25px;}
.st9{opacity:6.000000e-02;stroke:#000000;stroke-miterlimit:10;}
.st10{opacity:6.000000e-02;stroke:#000000;stroke-miterlimit:10;}
.st11{opacity:0.14;fill:#FFFFFF;}
</style>
<g>
	<rect y="0" class="st0" width="1180" height="398"/>
	<?php if($post->service_id==1){?>
	<g id="Circle">
		<path class="st9" d="M609,203.9C609,110.8,667.4,31.3,749.5,0H640.2c-55.1,50.6-89.7,123.2-89.7,203.9
			c0,75.6,30.3,144.1,79.5,194.1h97.4C657.1,361.8,609,288.5,609,203.9z"/>
		<path class="st9" d="M1180,364.9c-26.2,13.9-56.1,21.8-87.8,21.8c-17.7,0-34.9-2.5-51.1-7c39.3-47.8,62.9-109,62.9-175.7
			c0-70.4-26.3-134.7-69.6-183.5c18.2-5.9,37.6-9.1,57.8-9.1c31.7,0,61.6,7.9,87.8,21.8V0H905c12,4.6,23.4,10.1,34.3,16.7
			c-52,43.7-85.1,109.1-85.1,182.3c0,76.4,36,144.4,92,187.9c-6.1,4-12.5,7.7-19.1,11.1H1180V364.9z M904.5,199
			c0-61.7,29.8-116.4,75.7-150.6c40.2,39.6,65.2,94.7,65.2,155.6c0,58-22.6,110.7-59.5,149.7C936.8,319.8,904.5,263.2,904.5,199z"/>
	</g>
	<?php } else if($post->service_id==2){ ?>
	<g>
		<rect x="530.2" y="104" class="st10" width="477.7" height="132"/>
		<rect x="578.2" y="132" class="st10" width="477.7" height="132"/>
		<rect x="626.3" y="160" class="st10" width="477.7" height="132"/>
	</g>
	<?php } else { ?>
		<g>
			<rect x="870.8" class="st11" width="199.5" height="398"/>
			<rect x="904.8" y="0" class="st11" width="131.6" height="398"/>
			<rect x="937" y="0" class="st11" width="67.2" height="398"/>
		</g>
	<?php	} ?>
	<polyline class="st1" points="33,348 33,32 1148,32 1148,149.7 	"/>
	<g>
		<g>
			<path d="M1017.6,328.6h-1.1v-10.1h2.4c0.9,0,1.4,0.1,1.9,0.6c0.4,0.4,0.6,0.9,0.6,1.6v2.6c0,0.7-0.2,1.2-0.6,1.6
			c-0.4,0.4-1,0.6-1.9,0.6h-1.2V328.6L1017.6,328.6z M1020.3,323.4v-2.6c0-0.4-0.1-0.7-0.4-0.9c-0.2-0.2-0.6-0.4-1-0.4h-1.2v5.1
			h1.2C1019.8,324.5,1020.3,324.2,1020.3,323.4z"/>
			<path d="M1028.8,320.6v6.2c0,0.7-0.2,1.2-0.6,1.6c-0.4,0.4-1.1,0.6-2,0.6c-0.9,0-1.5-0.1-2-0.6c-0.5-0.4-0.6-0.9-0.6-1.6v-6.2
			c0-0.6,0.2-1.2,0.6-1.6c0.4-0.4,1.1-0.6,2-0.6c0.9,0,1.5,0.1,2,0.6C1028.6,319.3,1028.8,319.8,1028.8,320.6z M1027.7,326.6v-6.2
			c0-0.9-0.5-1.2-1.5-1.2s-1.5,0.4-1.5,1.2v6.2c0,0.9,0.5,1.2,1.5,1.2S1027.7,327.5,1027.7,326.6z"/>
			<path d="M1032.8,328.6h-1.1v-10.1h2.4c0.9,0,1.4,0.1,1.9,0.6c0.4,0.4,0.6,0.9,0.6,1.6v2.1c0,1.1-0.5,1.7-1.4,2l1.9,3.7h-1.2
			l-1.7-3.6h-1.2V328.6L1032.8,328.6z M1035.4,322.9v-2.2c0-0.4-0.1-0.7-0.4-0.9c-0.2-0.2-0.6-0.4-1-0.4h-1.2v4.6h1.2
			C1034.9,324.2,1035.4,323.8,1035.4,322.9z"/>
			<path d="M1043.2,318.6v1h-2v9.1h-1.1v-9.2h-2v-1L1043.2,318.6L1043.2,318.6z"/>
			<path d="M1046,318.6h1.6l2.2,10.1h-1.1l-0.5-2.4h-3l-0.5,2.4h-1.1L1046,318.6z M1046.7,319.8l-1.2,5.6h2.5L1046.7,319.8z"/>
			<path d="M1051.9,328.6v-10.1h1.1v9.1h2.9v1L1051.9,328.6L1051.9,328.6z"/>
		</g>
		<g>
			<path d="M1018,338.6h-1.5v-5.8h1.5c0.5,0,0.9,0.1,1.1,0.4c0.2,0.2,0.4,0.5,0.4,0.9v3.4c0,0.4-0.1,0.7-0.4,1
			C1018.8,338.4,1018.5,338.6,1018,338.6z M1018.8,337.2v-3.4c0-0.1-0.1-0.4-0.2-0.5c-0.1-0.1-0.4-0.2-0.6-0.2h-0.9v4.7h0.9
			c0.2,0,0.5-0.1,0.6-0.2C1018.8,337.6,1018.8,337.5,1018.8,337.2z"/>
			<path d="M1023.7,338.6h-2.6v-5.8h2.6v0.5h-2v2.1h1.6v0.5h-1.6v2.1h2V338.6z"/>
		</g>
		<g>
			<path d="M1030.2,342.7h-2.5v-10.1h2.5c0.9,0,1.4,0.1,1.9,0.6c0.4,0.4,0.6,0.9,0.6,1.6v5.7c0,0.7-0.2,1.2-0.6,1.6
			C1031.6,342.5,1030.9,342.7,1030.2,342.7z M1031.6,340.6v-5.7c0-0.4-0.1-0.6-0.4-0.9c-0.2-0.2-0.6-0.4-1-0.4h-1.4v8.2h1.4
			c0.4,0,0.7-0.1,1-0.4C1031.4,341.2,1031.6,340.9,1031.6,340.6z"/>
			<path d="M1037.8,341.8v1h-2.6v-1h0.7v-8.2h-0.7v-1h2.6v1h-0.7v8.2H1037.8z"/>
			<path d="M1043.4,342.7h-1.6l-2.2-10.1h1.1l2,8.8l2-8.8h1.1L1043.4,342.7z"/>
			<path d="M1051.8,340.8v-8.1h1.1v8.1c0,0.7-0.2,1.2-0.6,1.6c-0.4,0.4-1.1,0.6-2,0.6c-0.9,0-1.5-0.1-2-0.6
			c-0.5-0.4-0.6-0.9-0.6-1.6v-8.1h1.1v8.1c0,0.9,0.5,1.2,1.5,1.2S1051.8,341.7,1051.8,340.8z"/>
			<path d="M1055.9,342.7v-10.1h1.1v9.1h2.9v1L1055.9,342.7L1055.9,342.7z"/>
			<path d="M1063.7,338.6v-1h2.9v5.1h-0.5l-0.4-1c-0.2,0.4-0.5,0.6-0.9,0.9c-0.4,0.2-0.7,0.4-1.1,0.4c-1.5,0-2.2-0.7-2.2-2.1v-6.2
			c0-0.6,0.2-1.2,0.6-1.6c0.4-0.4,1-0.6,1.9-0.6s1.5,0.1,1.9,0.6c0.4,0.4,0.6,0.9,0.6,1.6v0.9h-1.1v-0.9c0-0.9-0.5-1.2-1.5-1.2
			c-1,0-1.5,0.4-1.5,1.2v6.2c0,0.4,0.1,0.7,0.2,0.9c0.1,0.1,0.5,0.4,0.9,0.4c0.4,0,0.9-0.1,1.2-0.5c0.4-0.4,0.5-0.6,0.5-1v-1.9
			L1063.7,338.6L1063.7,338.6z"/>
			<path d="M1070.7,332.7h1.6l2.2,10.1h-1.1l-0.5-2.4h-3l-0.5,2.4h-1.1L1070.7,332.7z M1071.4,333.8l-1.2,5.6h2.5L1071.4,333.8z"/>
			<path d="M1080.5,340.8v-0.9h1.1v0.9c0,1.4-0.7,2.1-2.4,2.1l-0.1,0.7h0.1c0.4,0,0.7,0.1,1,0.4c0.2,0.2,0.4,0.5,0.4,0.9v0.4
			c0,0.7-0.5,1.1-1.4,1.1c-0.9,0-1.4-0.4-1.4-1.1v-0.1h0.7v0.1c0,0.4,0.2,0.5,0.6,0.5s0.6-0.1,0.6-0.5v-0.4c0-0.1,0-0.2-0.1-0.4
			c-0.1-0.1-0.2-0.1-0.4-0.1h-1l0.2-1.4c-0.7-0.1-1.2-0.2-1.5-0.6c-0.4-0.4-0.5-0.9-0.5-1.5v-6.2c0-0.6,0.2-1.2,0.6-1.6
			c0.4-0.4,1-0.6,1.9-0.6c0.9,0,1.5,0.1,1.9,0.6c0.4,0.4,0.6,0.9,0.6,1.6v1.5h-1.1v-1.5c0-0.9-0.5-1.2-1.5-1.2s-1.5,0.4-1.5,1.2
			v6.2c0,0.9,0.5,1.2,1.5,1.2C1080,342,1080.5,341.7,1080.5,340.8z"/>
			<path d="M1089.3,334.6v6.2c0,0.7-0.2,1.2-0.6,1.6c-0.4,0.4-1.1,0.6-2,0.6s-1.5-0.1-2-0.6c-0.5-0.4-0.6-0.9-0.6-1.6v-6.2
			c0-0.6,0.2-1.2,0.6-1.6c0.4-0.4,1.1-0.6,2-0.6s1.5,0.1,2,0.6C1089.1,333.4,1089.3,334,1089.3,334.6z M1087.4,331
			c-0.1,0-0.4,0.1-0.6,0.2c-0.2,0.1-0.5,0.2-0.9,0.2c-0.2,0-0.5-0.1-0.6-0.2c-0.1-0.1-0.2-0.5-0.4-0.9l0.6-0.2
			c0.1,0.2,0.2,0.5,0.5,0.5c0.1,0,0.4-0.1,0.6-0.2c0.2-0.1,0.5-0.2,0.9-0.2c0.2,0,0.5,0.1,0.6,0.2c0.1,0.1,0.2,0.5,0.4,0.9
			l-0.6,0.2C1087.8,331.1,1087.6,331,1087.4,331z M1088.2,340.8v-6.2c0-0.9-0.5-1.2-1.5-1.2c-1,0-1.5,0.4-1.5,1.2v6.2
			c0,0.9,0.5,1.2,1.5,1.2S1088.2,341.7,1088.2,340.8z"/>
			<path d="M1096.8,342.7h-4.6v-10.1h4.3v1h-3.4v3.6h2.7v1h-2.7v3.7h3.5L1096.8,342.7L1096.8,342.7z"/>
			<path d="M1104,340.8c0,1.5-0.9,2.1-2.6,2.1s-2.6-0.7-2.6-2.1v-1.4h1.1v1.4c0,0.4,0.1,0.6,0.4,0.9c0.2,0.2,0.6,0.2,1.1,0.2
			s0.9-0.1,1.1-0.2c0.2-0.2,0.4-0.5,0.4-0.9c0-0.4,0-0.6-0.1-0.9c-0.1-0.2-0.2-0.5-0.5-0.6l-2.7-2.6c-0.4-0.4-0.5-0.6-0.6-1
			c-0.1-0.4-0.1-0.6-0.1-1.1c0-0.6,0.2-1.2,0.6-1.6c0.4-0.4,1.1-0.6,2-0.6c1.6,0,2.5,0.7,2.5,2.1v1.4h-1.1v-1.4
			c0-0.4-0.1-0.6-0.4-0.9c-0.2-0.2-0.6-0.2-1.1-0.2c-0.5,0-0.9,0.1-1.1,0.2c-0.2,0.2-0.4,0.5-0.4,0.9c0,0.4,0,0.6,0.1,0.9
			c0.1,0.2,0.2,0.4,0.5,0.6l2.7,2.6c0.2,0.4,0.5,0.6,0.6,1C1103.9,339.9,1104,340.3,1104,340.8z"/>
		</g>
		<g>
			<rect x="1016.5" y="341.8" width="7.2" height="1"/>
		</g>
		<polygon points="1017.5,312.4 1011.3,312.4 1010.3,312.4 1010.3,313.3 1010.3,319.6 1011.3,319.6 1011.3,313.3 1017.5,313.3 		"/>
		<g>

			<linearGradient id="SVGID_1_" gradientUnits="userSpaceOnUse" x1="1005.0992" y1="1659.2648" x2="974.0935" y2="1659.2648" gradientTransform="matrix(1 0 0 1 0 -1329.8898)">
				<stop  offset="0" style="stop-color:#808080"/>
				<stop  offset="1" style="stop-color:#000000"/>
			</linearGradient>
			<path class="st2" d="M997.8,312.9c-2.7-2.7-7.2-2.7-9.9,0l-11.6,11.6c-2.7,2.7-2.7,7.2,0,9.9l11.6,11.6c2.7,2.7,7.2,2.7,9.9,0
			l7.3-7.3v-18.4L997.8,312.9z"/>
			<path d="M1005.1,320.2l-4.2,4.2c-2.7,2.7-2.7,7.2,0,9.9l4.2,4.2V320.2z"/>
			<path class="st3" d="M992.6,334.8c-0.6,0-1.1,0.1-1.6,0.4l-4.1-4.1c0.2-0.5,0.4-1,0.4-1.6c0-0.6-0.1-1.1-0.4-1.6l4.3-4.3
			c0.5,0.2,1,0.4,1.6,0.4c1.9,0,3.2-1.5,3.2-3.2c0-1.9-1.5-3.2-3.2-3.2c-1.7,0-3.2,1.5-3.2,3.2c0,0.6,0.1,1.1,0.4,1.6l-4.3,4.3
			c-0.5-0.2-1-0.4-1.6-0.4c-1.9,0-3.2,1.5-3.2,3.2c0,1.7,1.5,3.2,3.2,3.2c0.6,0,1.1-0.1,1.6-0.4l4.1,4.1c-0.2,0.5-0.4,1-0.4,1.6
			c0,1.9,1.5,3.2,3.2,3.2c1.7,0,3.2-1.5,3.2-3.2C995.9,336.2,994.4,334.8,992.6,334.8z"/>
		</g>
	</g>
</g>
<text id="data" transform="matrix(1 0 0 1 98.9507 349.3467)" class="st4 st5">@isset($post->startDate)<?=date('d/m/Y', strtotime($post->startDate))?><?php if(isset($post->endDate)){echo " - ".date('d/m/Y', strtotime($post->endDate));}?>@endisset</text>
<text id="localizacao" transform="matrix(1 0 0 1 98.9506 314.5626)" class="st4 st5"><?=$post->location?></text>
<?php
//TÍTULO
$titulo=$post->title;
$tituloWorked=textConvert(45, $titulo);
?>
<g>
	<text transform="matrix(1 0 0 1 98.9506 105.7942)" class="st6 st7"><?php echo $tituloWorked[0];?></text>
</g>
<?php if(sizeOf($tituloWorked)==2){?>
	<g>
		<text transform="matrix(1 0 0 1 98.9504 156.0148)" class="st6 st7"><?php echo $tituloWorked[1];?></text>
	</g>
	<?php } ?>


	<?php
	//TÍTULO
	$descricao=$post->resume;
	$descricaoWorked=textConvert(70, $descricao);
	?>
	<g>
		<text transform="matrix(1 0 0 1 98.9506 215.881)" class="st4 st8"><?php echo $descricaoWorked[0];?></text>
	</g>
	<?php if(sizeOf($descricaoWorked)==2){?>
		<g>
			<text transform="matrix(1 0 0 1 98.9506 250.34)" class="st4 st8"><?php echo $descricaoWorked[1];?></text>
		</g>
		<?php } ?>

	</svg>

	<?php
	function textConvert($maxDim, $text){
		if(strlen($text)>$maxDim){
			$frase=explode(' ', $text);
			$frase1="";
			$frase2="";
			$count=0;
			foreach ($frase as $palavra) {
				$count+=strlen($palavra);
				if($count>$maxDim){
					if($count>($maxDim*1.8)){
						$frase2=$frase2." (...)";
						break;
					}
					$frase2=$frase2." ".$palavra;
				}else{
					$frase1=$frase1." ".$palavra;
				}
			}
			return array($frase1, $frase2);
		}
		else{
			return array($text);
		}
	}

	?>
