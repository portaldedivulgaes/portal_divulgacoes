<!DOCTYPE html>
<html lang="pt" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title>Contact</title>
  </head>
  <body bgcolor="#f2f2f2" style="font-family: Helvetica">
    <h3>Nova Mensagem de {{ $name }}</h3>
    <span style="font-size:14px">
      <p> <b>Email:</b> {{ $email }} </p>
      <p> <b>Assunto:</b> {{ $user_subject }} </p>
      <p> <b>Mensagem:</b> </p>
      <div style="background-color: white; padding: 15px; white-space: pre-wrap">{{ $user_message }}</div>
      <br>
      <hr>
      <p style="text-align:center;">Portal de Divulgações {{ "© ".date("Y") }}</p>
    </span>
  </body>
</html>
