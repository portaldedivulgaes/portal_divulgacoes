@extends('layout.master')

@section('title', 'Início')

<!-- CSS Link -->
<link href="{{asset('css/style_index.css')}}" rel="stylesheet">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,500,500i,600,600i,700,700i,800&display=swap" rel="stylesheet">

@section('content')

<!-- Curve Background -->
<section class="background">
    <svg version="1.1" id="curve" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1920 188" style="enable-background:new 0 0 1920 190;" xml:space="preserve">
        <path class="st0" d="M0.5,102.5c0,0,438-158,922.5-55.5c424.22,89.75,998-47,998-47l-0.5,190.5H0.5V102.5z" />
    </svg>

    <div class="container" id="container-titulo">
        <div class="row">
            <div class="col-md-6" id="titulo-pagina">
                <h2>Bem vindo ao</h2>
                <h2>Portal de Divulgações</h2>
                <br>
                <br>
                <p>Bem vindo ao Portal de Divulgações! Tudo o que precisa de saber acerca das mais recentes noticias e publicações da ESTG.</p>
                <br>
                <a href="#destaques">Ver destaques</a>
            </div>
            <div class="col-md-6">
                <img class="img_index" src="{{asset('img/chat_index.png')}}" alt="chat_index">
            </div>
        </div>
    </div>
</section>

<!-- Destaques -->
<div class="container">
    <section class="destaques" id="destaques">
        <h2 style="font-family: Raleway; font-weight:700; font-size:36px; text-align:center;" data-sal-duration="1000" data-sal="fade" data-sal-delay="100" data-sal-easing="ease-out-bounce">
            Divulgações em Destaque</h2>
        <br>
        <p style="font-family: Raleway; font-weight:400; text-align:center;" data-sal-duration="1000" data-sal="fade" data-sal-delay="100" data-sal-easing="ease-out-bounce">Aqui pode encontrar as divulgações
            que se consideram mais relevantes! <br>
            Podem encontrar muitas mais no separador divulgações.</p>
        <br>
        <br>
        @if (count($posts) == 0)
        <br><br><br>
        <h4 style="font-family: Raleway; font-weight:400; text-align:center; position:relative; left:1%;" data-sal-duration="1000" data-sal="fade" data-sal-delay="100" data-sal-easing="ease-out-bounce">De
            momento não se encontram publicações
            disponíveis.</h4>
        <br><br><br>
        @endif
        <div class="card-deck justify-content-center" data-sal-duration="1000" data-sal="slide-up" data-sal-delay="100" data-sal-easing="ease-out-bounce">
            @if (count($posts) > 0)
            @foreach ($posts as $post)
            <div class="card" style="max-width:22rem; padding-bottom:50px;">
                <a class="link-post" href="{{route('divulgacao', [$post->service->initial, $post->id])}}">
                    <div>
                        @if (!isset($post->image))
                        <div class="card-img" style="height:180px; background-image:url('{{route('divulgacoes.svgThumb', $post->id)}}');"></div>
                        @else
                        <div class="card-img" style="height:180px; background-image:url('{{ asset('/storage/posts_images/'.$post->image) }}');"></div>
                        @endif
                    </div>
                    <div class="card-body">
                        <h5 class="card-title">{{$post->title}}</h5>
                        <p class="card-text">{{$post->resume}}</p>
                    </div>
                </a>
                <div class="card-footer" style="position:absolute; bottom:0; width:100%;">
                    <small class="text-muted">Data de Publicação: {{date('d/m/Y', strtotime($post->created_at))}}</small>
                </div>
            </div>
            @endforeach
            @endif
        </div>
    </section>
</div>

<!-- Informação Gabinetes -->
<section class="servicos">
    <h2 style="font-family: Raleway; font-weight:700; font-size:36px; text-align:center;" data-sal-duration="1000" data-sal="fade" data-sal-delay="100" data-sal-easing="ease-out-bounce">Serviços no Portal de Divulgações</h2>
    <br>
    <p style="font-family: Raleway; font-weight:400; text-align:center;" data-sal-duration="1000" data-sal="fade" data-sal-delay="100" data-sal-easing="ease-out-bounce">
        Em baixo pode ficar a conhecer mais acerca dos serviços que trabalham com o Portal de Divulgações ESTG. <br> Esses serviços são conhecidos como GAID, GIRE e Internacional ESTG.
    </p>
    <br>
    <br>
    <div class="bg_servicos_gaid" style="background-color: #f2f4f7; width: 100%; height: 615px;">
        <div class="container container_servicos" style="line-height:1.8; font-family:Raleway; font-weight:400; color:#545454;" data-sal-duration="1000" data-sal="slide-right" data-sal-delay="100" data-sal-easing="ease-out-bounce">
            <div class="row">
                <!--  GAID  -->
                <div class="col-md-4">
                    <h2 style="font-family:Raleway; font-weight:400; line-height:1.4; color:#545454">{{$services[0]->name}}</h2>
                    <br>
                    <div class="row">
                        <div class="col-md-12 mb-2">
                            <i class="fas fa-map-marker-alt iconesRef mr-3"></i><span>Gabinete B2D9 | Piso 1 | Edifício B</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mb-2">
                            <i class="fas fa-clock iconesRef mr-3"></i><span>10:00 - 12:00 | 14:30 - 16:30</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mb-4">
                            <i class="fas fa-at iconesRef mr-3"></i><span>{{$services[0]->email}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <p>O <b><i>{{$services[0]->initial}} – {{$services[0]->name}}</i></b> do Politécnico de Leiria é um Gabinete que foi criado em 2006 para apoiar a organização interna da Escola na área da
                        investigação, desenvolvimento e inovação, enquanto importante área de atuação da Escola.</p>
                    <p>Tem como objetivos prioritários:</p>
                    <ul>
                        <li style="margin-left:20px;">apoiar e promover a participação dos investigadores em projetos e programas de I&D financiados por instituições nacionais ou internacionais;</li>
                        <li style="margin-left:20px;">apoiar e promover a realização de prestações de serviços de I&D e de transferência de conhecimento e tecnologia para as entidades exteriores;</li>
                        <li style="margin-left:20px;">apoiar nos processos de atribuição de bolsas de investigação no âmbito de projetos ou prestações de serviços de ID;</li>
                        <li style="margin-left:20px;">promover e apoiar a criação de parcerias que constituam mais-valias para o desenvolvimento das atividades de ID.</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="bg_servicos_gire" style="height:560px; width:100%;">
        <div class="container container_servicos" style="line-height:1.8; font-family:Raleway; font-weight:400; color:#545454;" data-sal-duration="1000" data-sal="slide-left" data-sal-delay="100" data-sal-easing="ease-out-bounce">
            <div class="row">
                <!--  GIRE  -->
                <div class="col-md-4">
                    <h2 style="font-family:Raleway; font-weight:400; line-height:1.4; color:#545454">{{$services[1]->name}}</h2>
                    <br>
                    <div class="row">
                        <div class="col-md-12 mb-2">
                            <i class="fas fa-map-marker-alt iconesRef mr-3"></i><span>Gabinete B1D11 | Piso 0 | Edifício B</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mb-2">
                            <i class="fas fa-clock iconesRef mr-3"></i><span>10:00 - 12:00 | 14:30 - 16:30</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mb-4">
                            <i class="fas fa-at iconesRef mr-3"></i><span>{{$services[1]->email}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <p>Inserido numa instituição que visa a excelência, o <b><i>{{$services[1]->name}}</i></b> <b><i>({{$services[1]->initial}})</i></b> tem como objetivo dar a conhecer, promover e dignificar a
                        imagem da Escola, junto dos seus públicos quer interno: antigos, atuais e potenciais estudantes, toda a comunidade académica; quer externo: empresas, entidades públicas locais e nacionais e todos os cidadãos em geral.</p>
                    <p>O <b><i>{{$services[1]->initial}}</i></b> desenvolve, assim, um vasto leque de atividades, que pautadas pela exigência e pelo rigor, visam sempre apoiar e promover as atividades da instituição e a reputação
                        desta, interna e externamente.</p>
                    <p>Ao <b><i>{{$services[1]->initial}}</i></b>, incumbe ainda, a gestão da reserva de espaços disponibilizados pela ESTG.</p>
                </div>
            </div>
        </div>
    </div>

    <div class="bg_servicos_inter" style="background-color: #f2f4f7; height:510px; width:100%;">
        <div class="container container_servicos" style="line-height:1.8; font-family:Raleway; font-weight:400; color:#545454;" data-sal-duration="1000" data-sal="slide-right" data-sal-delay="100" data-sal-easing="ease-out-bounce">
            <div class="row">
                <!--  INTER  -->
                <div class="col-md-4">
                    <h2 style="font-family:Raleway; font-weight:400; line-height:1.4; color:#545454">{{$services[2]->name}}</h2>
                    <br>
                    <div class="row">
                        <div class="col-md-12 mb-2">
                            <i class="fas fa-map-marker-alt iconesRef mr-3"></i><span>Gabinete B1D4 | Piso 0 | Edifício B</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mb-2">
                            <i class="fas fa-clock iconesRef mr-3"></i><span>10:00 - 12:00 | 14:30 - 16:30</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 mb-4">
                            <i class="fas fa-at iconesRef mr-3"></i><span>{{$services[2]->email}}</span>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <p>O <b><i>{{$services[2]->name}}</i></b> como um dos mais recentes serviços do Politécnico de Leiria, tem como objetivo dar a conhecer ao mundo académico do Politécnico de Leiria as mais recentes novidades e
                        oportunidades do mundo exterior.</p>
                    <p>É ainda importante referir que este serviço presta auxílio aos estudantes vindos do estrangeiro de forma a que estes possam estar da melhor forma integrados no espaço académico e social do Politécnico de Leiria.</p>
                    <p>Perante todos estes serviços que este gabinete presta pode-se claramente afirmar que este gabinete tem uma importância bastante relevante neste mundo da ESTG do Politécnico de Leiria.</p>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Vídeo -->
<section class="video">
    <div class="container" id="container_video">

        <h2 style="font-family: Raleway; font-weight:700; font-size:36px; text-align: center;" data-sal-duration="1000" data-sal="fade" data-sal-delay="100" data-sal-easing="ease-out-bounce">Portal
            de Divulgações ESTG</h2>
        <br>
        <p data-sal-duration="1000" data-sal="fade" data-sal-delay="100" data-sal-easing="ease-out-bounce">O <b><i>Portal de Divulgações ESTG</i></b> foi desenvolvido pelo um grupo de alunos do TeSP de Desenvolvimento
            Web e Multimédia, sendo eles,
            <i>Daniel Nunes, João Adão, José Areia, Mariana Pereira </i> e <i>Micaela Santos.</i> Este portal
            tem como objetivo colocar todas as publicações feitas pelos três serviços anteriormente referidos numa
            única plataforma.
            É ainda imporante sublinhar que estes alunos desenvolveram uma área de trabalho para estes serviços de
            forma a facilitar o seu trabalho. Criaram ainda uma newsletter dinâmica referente a cada serviço com o
            intuito de não haver envios
            contínuos por parte destes serviços. Para conhecerem um pouco melhor o trabalho podem visualizar o vídeo
            abaixo disponível.</p>
        <br>
        <iframe style="margin:0 auto; display:block; border:none;" width="739" height="415" src="https://www.youtube-nocookie.com/embed/vSNcV_Ds97E" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
          allowfullscreen></iframe>

    </div>
</section>

<!-- Script JS -->
<script>
    sal();
</script>

@endsection
