@extends('layout.masterbackoffice')

@section('title', 'Newsletters')

@section('styleLinks')
    <link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endsection

@section('content')

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Newsletters</h1>
        <a href="{{route('newsletters.create')}}" class="btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-plus text-white-50 mr-1"></i> Criar Newsletter</a>
    </div>

    <!-- Content Row -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Lista de Newsletters Enviadas</h6>
        </div>

        <div class="card-body">
            @if (count($newsletters))
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Assunto</th>
                            <th>Data de Criação</th>
                            @if(Auth::user()->isAdmin())
                                <th>Gabinete</th>@endif
                            <th style="width: 140px;">Opções</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($newsletters as $newsletter)
                            <tr>
                                <td class="align-middle">{{$newsletter->subject}}</td>
                                <td class="align-middle">{{$newsletter->created_at->format("m/d/Y")}}</td>
                                @if(Auth::user()->isAdmin())
                                    <td>{{$newsletter->service->initial}}</td>@endif
                                <td class="align-middle text-center">
                                    <form method="POST" role="form"
                                          action="{{route('newsletters.destroy',$newsletter)}}"
                                          class="d-inline-block form_newsletter_id">
                                        @csrf
                                        @method('DELETE')
                                        <button type='submit' class='btn btn-outline-danger btn-sm' data-toggle="modal"
                                                data-target="#confirm-delete">
                                            <i class='fas fa-trash-alt mr-1'></i> Remover
                                        </button>
                                    </form>

                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <h6 style="text-align: center">Sem Newsletters Registadas</h6>
            @endif
        </div>

    </div>

    <!-- Modal remove newsletter -->
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="confirm-delete" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Eliminar Newsletter</h5>


                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <span>Tem a certeza que pretende eliminar esta newsletter?</span>
                </div>
                <div class="modal-footer">
                    <button type="submit" id="eliminarNewsletter" class="btn btn-danger btn-sm"><i class="far fa-trash-alt mr-2"></i>Eliminar Newsletter
                    </button>
                    <button type="button" class="btn btn-secondary btn-sm mr-2" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section("scripts")
    <script>
        $(document).ready(function () {
            $('#dataTable').DataTable({
                'order': [
                    [1, 'desc']
                ],
                'language': {
                    'url': '//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese.json'
                },
                "bFilter": false,
                "searching": false,
                "bLengthChange": false,
                "info": false
            });

            var formToSubmit //Variavel qeu indica o row a submeter
            $(".form_newsletter_id").submit(function (e) {
                formToSubmit = this;
                return false;
            });

            $("#eliminarNewsletter").click(function (e) {
                formToSubmit.submit();
            });
        });
    </script>
@endsection
