@extends('layout.masterbackoffice')

@section('title', 'Criação da Newsletter | Newsletters')

@section('styleLinks')
    <link href="{{asset('css/style_newsletters.css')}}" rel="stylesheet">
    <link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
    <link href="{{asset('vendor/select2/select2-bootstrap4.min.css')}}" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet">
@endsection

@section('content')

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Criação da Newsletter</h1>
        <a href="{{route('newsletters.index')}}" class="btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-chevron-left text-white-50 mr-2"></i>Voltar à listagem</a>
    </div>

    <div class="card shadow mb-4">
        <div class="card-header py-3 font-weight-bold text-primary">
            Preencha os campos abaixo para criar a newsletter
        </div>
        <div class="card-body">
            <form method="POST" action="{{route('newsletters.preview')}}" class="form-group" id="newsletter">
                @if(Auth::user()->isAdmin())
                    <div class="form-group">
                        <label for="newsletter_service">Gabinete</label>
                        <select class="form-control" name="service_id" style="width: 100%!important;">
                            @foreach ($services as $service)
                                <option @if($oldData!=null && $oldData['service_id']==$service->id) selected @endif value="{{$service->id}}">{{$service->name}}</option>
                            @endforeach
                        </select>
                    </div>
                @else
                    <input type="hidden" name="service_id" value="{{ Auth::user()->service_id }}">
                @endif


                @csrf
                <div class="form-group">
                    <label for="subject">Assunto</label>
                    <input type="text" class="form-control" name="subject" id="subject" value="@if($oldData!=null){{$oldData['subject']}}@endif"
                           placeholder="Escreva aqui o assunto da Newsletter">
                </div>

                <div class="form-group">
                    <label for="target"> Endereço de Emails </label>
                    <select name="targetaudience[]" id="target" class="form-control" multiple="multiple"
                            placeholder="Clique e selecione os emails" style="width: 100%!important;">
                        @foreach($targetaudiences as $targetaudience)
                            <option @if($oldData!=null && in_array($targetaudience->email, $oldData['targetAudience'])) selected @endif value="{{$targetaudience->email}}">{{$targetaudience->name}}</option>
                        @endforeach
                    </select>
                </div>

                <input type="text" style="display:none;" id="selectedPosts" name="selectedPosts"/>

                <!-- Row select posts -->
                <div class="card mt-4">
                    <div class="card-header py-3">
                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal"
                                data-target="#ModalPosts">
                            <i class="fas fa-plus-square mr-1"></i>
                            Selecionar Publicações
                        </button>
                    </div>

                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered" id="tablePostsSelects" width="100%" cellspacing="0">
                                <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Título</th>
                                    <th>Texto Curto</th>
                                    @if(Auth::user()->isAdmin())
                                        <th style="width: 140px;">Gabinete</th>@endif
                                    <th style="width: 80px" class="align-middle text-center">Opções</th>
                                </tr>
                                </thead>
                                <tbody>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <br>

                <div class="form-group text-right">
                    <button type="submit" class="btn btn-info btn-sm mr-1"><i class="fas fa-search mr-2"></i>Pré-visualizar
                    </button>
                    <a class="btn btn-secondary btn-sm" href="{{route('newsletters.index')}}" role="button">Cancelar</a>
                </div>

                <!-- Modal from Select Posts-->
                <div class="modal fade" id="ModalPosts" role="dialog">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Escolha as Publicações</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                @if (count($posts))
                                    <table class="table table-bordered display" id="modalChoseTable" width="100%"
                                           cellspacing="0">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>Título</th>
                                            <th>Texto Curto</th>
                                            @if(Auth::user()->isAdmin())
                                                <th style="width: 100px" >Gabinete</th>@endif
                                            <th></th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($posts as $post)
                                            <tr class="post_{{$post->id}}">
                                                <td>{{$post->id}}</td>
                                                <td class="align-middle">{{$post->title}}</td>
                                                <td class="align-middle">{!!$post->resume!!}</td>
                                                @if(Auth::user()->isAdmin())
                                                    <td>{{$post->service->initial}}</td>@endif
                                                <td>
                                                    <button type='button'
                                                            class='btn btn-outline-danger btn-sm btnremove'>
                                                        <i class='fas fa-trash-alt mr-1'></i> Remover
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                            </div>
                            @else
                                <h6 style="text-align: center">Sem Publicações Disponíveis</h6>
                            @endif


                            <div class="modal-footer">
                                <button type="button" id="selectRows" class="btn btn-sm btn-primary">  <i class="fas fa-plus-square text-white-50 mr-2"></i>Selecionar</button>
                                <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Fechar
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- End Modal -->

            </form>


        </div>
    </div>

    <!-- Modal remove -->
    <div class="modal fade" tabindex="-1" role="dialog" id="modalEliminar">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Remover Divulgação</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Tem a certeza que pretende remover esta publicação?</p>
                </div>
                <div class="modal-footer">
                    <button id="removePub" type="button" class="btn btn-danger btn-sm btnremove">Remover</button>
                    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cancelar</button>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script src="{{asset('/js/script_newsletter.js')}}"></script>
    <script src="{{asset('vendor/select2/select2.js')}}"></script>
    <script src="{{asset('vendor/select2/script.js')}}"></script>
    <script>


        $(document).ready(function () {

            $('#modalChoseTable tbody').on('click', 'tr', function () {
                $(this).toggleClass('selected');
            });

            //*******  lista no modal com as publicações   *******//
            var modalTable = $('#modalChoseTable').DataTable({
                'order': [
                  <?=Auth::user()->isAdmin()?'[3, \'asc\'],':''?>
                    [1, 'asc']
                ],
                'language': {
                    'url': '//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese.json'
                }, 'columnDefs': [
                    {
                        'targets': [0],
                        'visible': false
                    }, {
                        'targets': [-1],
                        'visible': false
                    }
                ]

            });

            //*******  tabela que recebe as publicações selecionadas da modal   *******//
            var tableSelects = $('#tablePostsSelects').DataTable({
                'order': [
                    [1, 'desc']
                ],
                'language': {
                    'url': '//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese.json'
                },
                "bFilter": false,
                "searching": false,
                "bLengthChange": false,
                "paging": false,
                "info": false,
                'columnDefs': [
                    {
                        'targets': [0],
                        'visible': false
                    }
                ]
            });

                <?php if($oldData!=null){ ?>
            var oldData=<?php echo json_encode($oldData['selectedPosts']);?>

                $.each(oldData, function (key, value) {
                    tableSelects.row.add(modalTable.row($(".post_"+value)).data()).draw();
                    modalTable.row($(".post_"+value)).remove().draw();
                });

            $(".btnremove").on('click', function () {
                var $row = tableSelects.row($(this).parent().parent());
                // console.log($row);
                $('#modalEliminar').modal();
                $("#removePub").on('click', function () {
                    modalTable.row.add($row.data()).draw();
                    tableSelects.row($row).remove().draw();
                    $("#removePub").off();
                    $('#modalEliminar').modal('hide');
                });
            });
            <?php } ?>


            //botão de passagem dos itens selecionados para a tabela '#tablePostsSelects'
            $('#selectRows').click(function () {

                $('.selected').each(function () {
                    // console.log(modalTable.row(this));
                    tableSelects.row.add(modalTable.row(this).data()).draw();
                    modalTable.row(this).remove().draw();
                });
                $('.btnremove').off('click');
                //revome da tabela e volta a colocar na tabela inicial(modalTable)
                $(".btnremove").on('click', function () {
                    var $row = tableSelects.row($(this).parent().parent());
                    // console.log($row);
                    $('#modalEliminar').modal();
                    $("#removePub").on('click', function () {
                        modalTable.row.add($row.data()).draw();
                        tableSelects.row($row).remove().draw();
                        $("#removePub").off();
                        $('#modalEliminar').modal('hide');
                    });
                });

            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#newsletter').on('submit', function () {
                var dataposts = tableSelects.rows().data();

                var divulgacoes = [];
                dataposts.each(function (value, index) {
                    divulgacoes.push(value[0]);
                });

                $("#selectedPosts").val(divulgacoes);

                return true;

            });






        });
    </script>
@endsection
