@extends('layout.masterbackoffice')
@section('title', 'Criação da Newsletter | Newsletters')
@section('content')
    <div class="container-fluid">

        <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Pré-visualização da Newsletters</h1>

            <form method="post" action="{{route('newsletters.store')}}">
                @csrf
                @foreach ($targetaudiences as $t)
                    <input type="hidden" name="targetaudience[]" value="{{$t}}">
                @endforeach
                <input type="hidden" name="subject" value="{{$subject}}">
                <input type="hidden" name="service_id" value="{{$service_id->id}}">
                @foreach ($selectedPosts as $p)
                    <input type="hidden" name="selectedPosts[]" value="{{$p}}">
                @endforeach
                <a href="{{route('newsletters.create')}}" class="btn btn-sm btn-primary shadow-sm mr-1">
                    <i class="fas fa-chevron-left text-white-50 mr-2"></i>Voltar à listagem</a>

                <button type="submit" class="btn btn-sm btn-success shadow-sm">
                    <i class="fas fa-paper-plane text-white-50 mr-2"></i>Enviar Newsletter
                </button>


            </form>
        </div>

        <div class="card shadow mb-4">
            <div class="card-header py-3">
                Visualização
            </div>
            <div class="card-body">
                <div>
                    <p><b>Assunto da Newsletter: </b> {{$subject}}</p>
                    <p><b>Para: </b>{{implode(', ',$targetaudiences)}}</p>
                </div>

                {{--Começo do template--}}
                <!doctype html>
                <html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml"
                      xmlns:o="urn:schemas-microsoft-com:office:office">
                <head>
                    <meta charset="UTF-8">
                    <meta http-equiv="X-UA-Compatible" content="IE=edge">
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <title>Newsletter</title>

                </head>
                <body
                    style="height: 100%;margin: 0;padding: 0;width: 100%;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;">
                <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%"
                       id="bodyTable"
                       style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;background-color: #FAFAFA;">
                    <tr>
                        <td align="center" valign="top" id="bodyCell"
                            style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 10px;width: 100%;border-top: 0;">
                            <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer"
                                   style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;border: 0;max-width: 600px !important;">
                                <tr>
                                    <td valign="top" id="templatePreheader"
                                        style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #fafafa;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 9px;"></td>
                                </tr>

                                <tr>
                                    <td class="mcnImageCardBlockInner" valign="top"
                                        style="padding-top: 9px;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">

                                        <table align="left" border="0" cellpadding="0"
                                               class="mcnImageCardBottomContent" width="100%"
                                               style="background-color: #404040;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                            <tbody>
                                            @foreach($posts  as $post)
                                                <tr>
                                                    <td class="mcnImageCardBottomImageContent"
                                                        align="left"
                                                        valign="top"
                                                        style="padding-top: 0px;padding-right: 0px;padding-bottom: 0;padding-left: 0px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">

                                                        @if ($post->image == null)
                                                            <img src="{{route('divulgacoes.svg', $post->id)}}"
                                                                 width="564"
                                                                 style="max-width: 1180px;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;vertical-align: bottom;"
                                                                 class="mcnImage">
                                                        @else
                                                            <img src="{{asset('/storage/posts_images/'.$post->image)}}"
                                                                 class="img-fluid" alt="Imagem de capa">
                                                        @endif
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="mcnTextContent" valign="top"
                                                        style="padding: 9px 18px;color: #F2F2F2;font-family: Helvetica;font-size: 14px;font-weight: normal;text-align: center;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;line-height: 150%;"
                                                        width="546">
                                                        <a href="{{route('divulgacao', [$post->service->initial, $post->id])}}" target="_blank" style="color: white;">{{$post->title}}</a>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td valign="top" width="600"
                                                        style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #FAFAFA;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 9px;padding-bottom: 9px;">
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td valign="top" id="templateFooter">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%"
                                               class="mcnDividerBlock"
                                               style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;table-layout: fixed !important;">
                                            <tbody class="mcnDividerBlockOuter">
                                            <tr>
                                                <td class="mcnDividerBlockInner"
                                                    style="min-width: 100%;padding: 10px 18px 25px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                    <table class="mcnDividerContent" border="0" cellpadding="0"
                                                           cellspacing="0"
                                                           width="100%"
                                                           style="min-width: 100%;border-top: 2px solid #EEEEEE;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                        <tbody>
                                                        <tr>
                                                            <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                <span></span>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>

                                        <table align="left" border="0" cellpadding="0" cellspacing="0"
                                               width="100%" class="mcnTextContentContainer">
                                            <tbody>
                                            <tr>

                                                <td valign="top" class="mcnTextContent"
                                                    style="padding: 0px 18px 9px;line-height: 125%;text-align: left;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #656565;font-family: Helvetica;font-size: 12px;">

                                                    Apartado 4133 | 2411-901 Leiria – PORTUGAL<br>
                                                    Tel._ (+351) 244 830 010<br>
                                                    <a data-auth="NotApplicable"
                                                       href="http://www.ipleiria.pt/"
                                                       rel="noopener noreferrer" target="_blank"
                                                       title="www.ipleiria.pt"
                                                       style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #656565;font-weight: normal;text-decoration: underline;">www.ipleiria.pt</a>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                </center>
                </body>
                </html>
            </div>
        </div>
    </div>
@endsection
