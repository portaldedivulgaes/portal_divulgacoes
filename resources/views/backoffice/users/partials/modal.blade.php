<!-- Modal -->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModal" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Eliminar Utilizador</h5>


                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <span>Tem a certeza que deseja eliminar este utilizador?</span><br><br>
                <span><h6><i class="fas fa-user mr-2"></i>Utilizador: <span class="font-weight-bold" id="spanUserId"></span><span></h6>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-danger btn-sm btn_submit"><i class="far fa-trash-alt mr-2"></i>Sim,
                    eliminar utilizador
                </button>
                <button type="button" class="btn btn-secondary btn-sm mr-2" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>
