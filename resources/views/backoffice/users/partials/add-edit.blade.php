
<div class="row">

    <div class="col col-4 mb-3 div-user-photo">

        <div class="card mx-auto ">
            <p class="card-text text-center mt-3">Imagem de apresentação</p>


            <!-- Verifica se a imagem já existe-->
            @if ($user->photo!=null)
                <img src="{{Storage::disk('public')->url('users_photos/').$user->photo}}" id="preview" class="img-thumbnail shadow mx-auto" alt="Imagem de apresentação" title="Clique para mudar a imagem de apresentação" />
            @else
                <img src="{{asset('img/default_user.png')}}" id="preview" class="img-thumbnail shadow mx-auto" alt="Imagem de apresentação" title="Clique para mudar a imagem de apresentação" />
            @endif


            <div class="text-center"><div class="reset_photo mx-auto" id="btn_remove_photo" title="Remover imagem de apresentação"><i class="fas fa-times text-white"></i></div></div>


            <div class="card-body overflow-hidden">
                <p class="text-center">Selecione a sua imagem de apresentação</p>

                <div class="input-group mx-auto mt-4">

                    <input type="file" name="photo" id="inputPhoto" class="file" accept="image/*" aria-describedby="fileHelp">
                    <input type="text" class="form-control" disabled placeholder="Selecione o ficheiro" id="file">
                    <div class="input-group-append">
                        <button type="button" class="browse btn btn-primary">Procurar</button>
                    </div>

                </div>

                <span id="fileHelp" class="form-text text-muted text-center mt-3">O tamanho do ficheiro não deve ultrapassar os 2MB </span>
            </div>

        </div>


    </div>



    <div class="col userfields">

        @if($user->created_at==null)
            <h4 class="mt-2"><i class="fas fa-user mr-2"></i>Novo utilizador</h4><br>
        @else
            @if($user->name!=null)
                <h4 class="mt-2"><i class="fas fa-user mr-2"></i>{{$user->name}}</h4><br>
            @else
                <h4 class="mt-2"><i class="fas fa-user mr-2"></i>Aguarda primeiro acesso</h4><br>
            @endif
        @endif


        {{-- Identificação do utilizador --}}
        @if (Auth::user()->role=="Admin" )
        <label for="user"><span class="campo_obrigatorio text-danger"><small>&#10033;</small></span> Identificação do utilizador</label>
        <i class="fas fa-info-circle text-primary id_info ml-1" data-toggle="tooltip" data-html="true" data-placement="bottom" title="<p class='text-left p-2'>- Caracteres alfanuméricos<br><br>- Não contém espaços<br><br>- Máximo de 40 caracteres<br><br>- Depois de criado, não pode ser alterado</p>"></i>

        <input type="text" class="form-control" name="user" id="user" value="{{old('user',$user->user)}}" @if ($user->created_at!=null) disabled @endif pattern="^[a-zA-Z0-9_-]+$" maxlength="40" title="Insira uma identificação válida" placeholder="Insira uma identificação válida">
        <small class="text-muted">Exemplo: 2100000, nome_sobrenome</small>

        @else
        <input type="hidden" class="form-control" name="user" id="user" value="{{old('user',$user->user)}}">
            Identificação do utilizador: <strong>{{ $user->user}} </strong>
        @endif

        <br><br>

        {{-- Perfil de utilizador --}}
        @can("updateRole",$user)
            <label for="role"><span class="campo_obrigatorio text-danger"><small>&#10033;</small></span> Perfil de utilizador</label>
            <select class="custom-select" name="role" id="role" @if (Auth::user()->user == $user->user ) disabled @endif>
                <option value="0" hidden selected>Escolha o perfil de utilizador</option>
                <option {{old('role',$user->role)=='Admin'?"selected":""}} value="Admin">Administrador</option>
                <option {{old('role',$user->role)=='Employer'?"selected":""}} value="Employer">Funcionário</option>
            </select>

        @else
            <input type="hidden" class="form-control" name="role" id="role" value="{{old('user',$user->role)}}">
            Perfil de utilizador: <strong>{{$user->roleToStr()}}</strong>
        @endcan


        <br><br>

        {{-- Gabinete --}}
        @can("updateService_id",$user)
        <label for="service_id"><span class="campo_obrigatorio text-danger"><small>&#10033;</small></span> Gabinete</label>
        <select class="custom-select" name="service_id" id="service_id" @if (Auth::user()->user == $user->user || $user->user==null ) disabled @endif>
            <option value="-2" hidden selected></option>
            <option value="-1" hidden >Escolha o gabinete</option>
            <option value="0" hidden>Não se aplica</option>

            @foreach($services as $service)
            <option value={{ $service->id }} {{old('service_id',$user->service_id)==$service->id?"selected":""}}>{{ $service->initial }}</option>
            @endforeach

        </select>
        @else
            <input type="hidden" class="form-control" name="service_id" id="service_id" value="{{old('user',$user->service_id)}}">
            Gabinete: <strong>{{$user->serviceToStr()}}</strong>
        @endcan

        <br><br>

        @if (Auth::user()->role=="Admin" )
            <small><span class="campo_obrigatorio text-danger">&#10033;</span> Campo de preenchimento campo obrigatório</small><br>
            <small>Nota: A password é fornecida pelos serviços do IPLeiria</small>
        @else
            <small>Nota: apenas um administrador pode alerar o seu gabinete / perfil de utilizador </small>
        @endif

    </div>

</div>

