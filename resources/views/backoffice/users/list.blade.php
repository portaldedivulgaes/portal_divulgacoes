@extends('layout.masterbackoffice')


@section('title', 'Utilizadores')



@section('styleLinks')
<link href="{{asset('css/style_users.css')}}" rel="stylesheet">
<link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endsection


@section('content')
@include('backoffice.users.partials.modal') <!-- MODAL DE INFORMAÇÔES -->


<div class="d-sm-flex align-items-right justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800 text-right">Utilizadores</h1>

    @can("create",App\User::class)
    <div>
        <a href="{{route('users.trashed')}}" class="btn btn-sm btn-primary shadow-sm mr-2"><i class="fas fa-trash-restore text-white-50 mr-2"></i> Recuperar utilizador</a>
        <a href="{{route('users.create')}}" class="btn btn-sm btn-primary shadow-sm "><i class="fas fa-plus text-white-50 mr-2"></i> Adicionar utilizador</a>

    </div>

    @endcan


</div>

<!-- Content Row -->
<div class="card shadow mb-4">

    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Lista de utilizadores</h6>
    </div>




    <div class="card-body">


        <div class="table-responsive">

            <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <thead>
                    <tr>
                        <th class="text-center">Imagem</th>
                        <th>Utilizador</th>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Perfil</th>
                        <th>Gabinete</th>
                        <th class="text-center">Opções</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($users as $user)
                    <tr>

                        <!-- USER PHOTO -->
                        <td class="align-middle text-center ">
                            @if ($user->photo)
                            <img src="{{Storage::disk('public')->url('users_photos/').$user->photo}}" class="img_user rounded-circle" alt="Imagem de apresentação">
                            @else
                            <img src="{{asset('img/default_user.png')}}" class="img_user rounded-circle" alt="Imagem de apresentação">
                            @endif
                        </td>

                        <!-- USER ID -->
                        <td class="align-middle">{{ $user->user }}</td>

                        <!-- NOME -->

                        <td class="align-middle">
                            @if ($user->name == null)
                            <span class="text-primary">Aguarda primeiro acesso </span>
                            @else
                            {{ $user->name }}
                            @endif
                        </td>


                        <!-- E-MAIL -->
                        <td class="align-middle">
                            @if ($user->email == null)
                            <span >Sem dados</span>
                            @else
                            {{ $user->email }}
                            @endif
                        </td>

                        <!-- ROLE -->
                        <td class="align-middle">
                            {{$user->roleToStr()}}
                        </td>


                        <!-- Service Initial -->
                        <td class="align-middle">
                            {{$user->serviceToStr()}}
                        </td>



                        <td nowarp class="text-center align-middle">


                            <!-- Função View -->
                            <a class="btn btn-sm btn-outline-primary mb-1" href="{{ route('users.show',$user->user) }}" title="Ver perfil"><i class="fas fa-eye"></i></a>


                            <!-- Função Edit -->
                            @can("update",$user)
                            <a class="btn btn-sm btn-outline-warning  mb-1" href="{{ route('users.edit',$user->user) }}" title="Editar informações"><i class="fas fa-pen"></i></a>
                            @else
                            <span class="btn btn-sm btn-outline-secondary disabled mb-1"><i class="fas fa-pen"></i></span>
                            @endcan



                            <!-- Função delete -->


                            @if ($user->user== Auth::user()->user )<!-- Utilizador não pode eliminar-se  -->

                            <span class="btn btn-sm btn-outline-secondary disabled  mb-1">
                                <i class="fas fa-trash-alt"></i></span>
                                @else

                                @can("delete",$user)

                                <form method="POST" role="form" id="{{ $user->user }}" action="{{route('users.destroy',$user)}}" class="d-inline-block form_user_id" >
                                    @csrf
                                    @method('DELETE')

                                    <button type="submit" class="btn btn-sm btn-outline-danger btn-delete mb-1" title="Eliminar utilizador" data-toggle="modal" data-target="#deleteModal" ><i class="fas fa-trash-alt"></i></button>

                                </form>

                                @else

                                <span class="btn btn-sm btn-outline-secondary disabled mb-1">
                                    <i class="fas fa-trash-alt"></i></span>

                                    @endcan

                                    @endif

                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>



        </div>


        @endsection




        @section('scripts')

        <script src="{{asset('/js/script_list_users.js')}}"></script>

        @endsection
