

@extends('layout.masterbackoffice')


@section('title', 'Detalhes do utilizador | Utilizadores')

@section('styleLinks')
<link href="{{asset('css/style_add_user.css')}}" rel="stylesheet">
@endsection

@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Detalhes do utilizador</h1>
    <div>
        <a href="javascript:history.go(-1)" class="btn btn-sm btn-primary shadow-sm mr-2"><i class="fas fa-chevron-left text-white-50 mr-2"></i>Voltar</a>
        @can("update",$user)
        <a class="btn btn-sm btn-warning shadow-sm" href="{{ route('users.edit',$user->user) }}" title="Editar informações"><i class="fas fa-pen text-white-50 mr-2"></i>Editar Informações</a>
        @else
        <span class="btn btn-sm btn-secondary disabled"><i class="fas fa-pen"></i></span>
        @endcan
    </div>
</div>

<!-- Content Row -->



<div class="card shadow ">

    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Dados do utilizador {{$user->name}}</h6>
    </div>

    <div class="card-body ">


        <div class="row mb-3">

            <!-- Coluna da imgage -->
            <div class="col col-2 col-photo">

                <!-- Verifica se a imagem já existe-->
                @if ($user->photo==null)
                    <img src="{{asset('img/default_user.png')}}" class="img-thumbnail shadow text-center ml-4" alt="Imagem de apresentação" title="Imagem de apresentação" />
                @else
                    <img class="img-thumbnail shadow text-center ml-4" src="{{Storage::disk('public')->url('users_photos/').$user->photo}}">
                @endif

            </div>

            <!-- Coluna dos dados -->
            <div class="col">

                <div class="m-3"><strong>Nome:</strong>
                    @if ($user->name == null)
                    <span class="text-primary">Aguarda primeiro acesso</span>
                    @else
                    {{ $user->name }}
                    @endif
                </div>


                <div class="m-3" ><strong>Email:</strong>
                    @if ($user->email == null)
                    <span>Aguarda primeiro acesso</span>
                    @else
                    {{ $user->email }}
                    @endif
                </div>


                <div class="m-3"><strong>Perfil:</strong>
                    {{$user->roleToStr()}}
                </div>


                <div class="m-3"><strong>Gabinete:</strong>
                    {{$user->serviceToStr()}}
                </div>


                <div class="m-3" ><strong>Utilizador criado em:</strong>
                    {{$user->created_at->format("d/m/Y")}}
                </div>

            </div>

        </div>



        <div class="row mt-3">

            <div class="col mt-3">

                <ul class="nav nav-tabs m-2" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="resumo-tab" data-toggle="tab" href="#resumo" role="tab" aria-controls="resumo"
                        aria-selected="true">Resumo</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="history-tab" data-toggle="tab" href="#history" role="tab" aria-controls="history"
                        aria-selected="false">Histórico de atividade</a>
                    </li>

                </ul>

                <div class="tab-content m-2 px-3" id="myTabContent">


                    <div class="tab-pane fade show active p-2" id="resumo" role="tabpanel" aria-labelledby="resumo-tab">
                        <p>Este utilizador é {{$user->roleToStr()}}, e tem permissão para:</p>

                        @if ($user->service_id == null)
                            <ul>
                                <li>Criar novos eventos / divulgaçoes para <strong>qualquer gabinete</strong></li>
                                <li>Editar / modificar quais quer eventos e divulgaçoes já existentes</li>
                                <li>Criar / modificar / eliminar contas de utilizadores</li>
                                <li>Gerir as listas dos públicos alvos</li>
                                <li>Gerir os tipos de divulgações</li>
                                <li>Consultar as estatísticas referentes a todos os gabinetes</li>
                                <li>Criar um backup da base de dados</li>
                                <li>Repor os dados de origem da base de dados</li>

                            </ul>
                        @else
                            <ul>
                                <li>Criar novos eventos / divulgaçoes para o gabinete <strong>{{$user->serviceToStr()}}</strong></li>
                                <li>Editar / modificar quais quer eventos e divulgaçoes já existentes</li>
                                <li>Criar e enviar newsletters em nome do gabinete <strong>{{$user->serviceToStr()}}</strong></li>
                                <li>Consultar as estatísticas referentes ao gabinete <strong>{{$user->serviceToStr()}}</strong></li>
                                <li>Personalizar a sua imagem de apresentação</li>

                            </ul>
                        @endif

                    </div>

                    <div class="tab-pane fade p-2" id="history" role="tabpanel" aria-labelledby="history-tab">


                        <div class="table-responsive " style="overflow:hidden;">
                            <table class="table table-bordered historydetails" nowarp id="dataTable" style="width:100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th class="text-center"># ID</th>
                                        <th>Tipo de publicação</th>
                                        <th>Titulo</th>
                                        <th>Gabinete</th>
                                        <th>Data de criação</th>
                                        <th class="text-center">Opções</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($posts as $post)
                                    <tr>
                                        {{-- ID do post --}}
                                        <td class="align-middle text-center">
                                            {{ $post->id}}
                                        </td>

                                        {{-- Tipo de publicação --}}
                                        <td class="align-middle">
                                            {{ $post->type}} @if ($post->deleted_at!=null)<span class="text-danger">(eliminado)</span>@endif
                                        </td class="align-middle">


                                        {{-- Titulo do post --}}
                                        <td class="align-middle">
                                            {{ $post->title}}
                                        </td>

                                        {{-- Gabinete --}}
                                        <td class="align-middle">
                                            @if ($post->service_id==null)
                                            Não atribuido
                                            @elseif ($post->service_id==1)
                                            GAI&D
                                            @elseif ($post->service_id==2)
                                            GIRE
                                            @else
                                            Internacional
                                            @endif
                                        </td>

                                        {{-- Data de criação --}}
                                        <td class="align-middle">
                                            {{ $post->created_at}}
                                        </td>

                                        {{-- Opções --}}
                                        <td class="text-center align-middle">


                                        @if ($post->deleted_at!=null)
                                            <span class="text-danger">(eliminado)</span>
                                        @else
                                            <a class="btn btn-sm btn-outline-primary mb-1" href="{{ route('posts.show',$post->id) }}" title="Ver publicação"><i class="fas fa-eye"></i></a>
                                            <a class="btn btn-sm btn-outline-warning  mb-1" href="{{ route('posts.edit',$post->id) }}" title="Editar informações"><i class="fas fa-pen"></i></a>
                                        @endif


                                        </td>

                                    </tr>

                                    @endforeach





                                    @foreach($newsletters as $newsletter)
                                    <tr>
                                        {{-- ID do post --}}
                                        <td class="align-middle text-center">
                                            {{$newsletter->id}}
                                        </td>

                                        {{-- Tipo de publicação --}}
                                        <td class="align-middle">
                                            Newsletter @if ($newsletter->deleted_at!=null)<span class="text-danger">(eliminado)</span>@endif
                                        </td>

                                        {{-- Titulo --}}
                                        <td class="align-middle">
                                            {{$newsletter->subject}}
                                        </td>

                                        {{-- Gabinete --}}
                                        <td class="align-middle">
                                            @if ($newsletter->service_id==null)
                                            Não atribuido
                                            @elseif ($newsletter->service_id==1)
                                            GAI&D
                                            @elseif ($newsletter->service_id==2)
                                            GIRE
                                            @else
                                            Internacional
                                            @endif
                                        </td>


                                        {{-- Data de criação --}}
                                        <td class="align-middle">
                                            {{ $newsletter->created_at}}
                                        </td>


                                        {{-- Opções --}}


                                        <td class="align-middle text-center">

                                        @if ($newsletter->deleted_at!=null)
                                            <span class="text-danger">(eliminado)</span>
                                        @else
                                            <a class="btn btn-sm btn-outline-primary mb-1" href="{{ route('newsletters.index') }}" title="Página das Newsletters"><i class="fas fa-external-link-alt"></i></a>
                                        @endif

                                        </td>

                                    </tr>

                                    @endforeach


                                </tbody>
                            </table>
                        </div>



                    </div>


                </div>



            </div>

        </div>

    </div>

</div>






@endsection




@section('scripts')

<script src="{{asset('/js/script_details_users.js')}}"></script>

@endsection
