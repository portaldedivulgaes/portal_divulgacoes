@extends('layout.masterbackoffice')

@section('title', 'Novo utilizador | Utilizadores')

@section('styleLinks')
<link href="{{asset('css/style_add_user.css')}}" rel="stylesheet">
@endsection


@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Criar novo utilizador</h1>
    <a href="javascript:history.go(-1)" class="btn btn-sm btn-primary shadow-sm "><i class="fas fa-chevron-left text-white-50 mr-2"></i>Voltar</a>
</div>

<!-- Basic Card Example -->
<div class="card shadow ">


    <div class="card-body ">

        <form method="POST" action="{{route('users.store')}}" class="form-group pt-3" id="form_user" enctype="multipart/form-data">
            @csrf
            @include('backoffice.users.partials.add-edit')
            <div class="form-group text-right">
                <br><br>
                <button type="button" class="btn btn-sm btn-primary" name="ok" id="buttonSubmit"><i class="fas fa-plus text-white-50 mr-2"></i>Adicionar utilizador</button>
                <a href="{{route('users.index')}}" class="btn btn-sm btn-secondary text-white mr-3">Cancelar</a>
            </div> </form>

        </div>

    </div>

        @endsection

        @section('scripts')

        <script src="{{asset('/js/script_add_users.js')}}"></script>


        @endsection
