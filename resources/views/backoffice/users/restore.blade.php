@extends('layout.masterbackoffice')


@section('title', 'Recuperar Utilizadores | Utilizadores')



@section('styleLinks')
<link href="{{asset('css/style_users.css')}}" rel="stylesheet">
<link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endsection


@section('content')



<div class="d-sm-flex align-items-right justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800 text-right">Recuperar utilizador</h1>

    <a href="{{route('users.index')}}" class="btn btn-sm btn-primary shadow-sm mr-2"><i class="fas fa-chevron-left text-white-50 mr-2"></i>Voltar</a>


</div>

<!-- Content Row -->
<div class="card shadow mb-4">

    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Lista de utilizadores eliminados</h6>
    </div>




    <div class="card-body">


        <div class="table-responsive">

            <table class="table table-bordered" id="dataTable" width="100%" >
                <thead>
                    <tr>
                        <th class="text-center">Imagem</th>
                        <th>Utilizador</th>
                        <th>Nome</th>
                        <th>Perfil</th>
                        <th>Gabinete</th>
                        <th>Eliminado em</th>
                        <th class="text-center">Recuperar</th>
                    </tr>
                </thead>
                <tbody>

                    @foreach($users as $user)
                    <tr>

                        <!-- USER PHOTO -->
                        <td class="align-middle text-center ">
                            @if ($user->photo)
                            <img src="{{Storage::disk('public')->url('users_photos/').$user->photo}}" class="img_user rounded-circle" alt="Imagem de apresentação">
                            @else
                            <img src="{{asset('img/default_user.png')}}" class="img_user rounded-circle" alt="Imagem de apresentação">
                            @endif
                        </td>

                        <!-- USER ID -->
                        <td class="align-middle">{{ $user->user }}</td>

                        <!-- NOME -->

                        <td class="align-middle">
                            @if ($user->name == null)
                            <span class="text-primary">Aguarda primeiro acesso </span>
                            @else
                            {{ $user->name }}
                            @endif
                        </td>


                        <!-- ROLE -->
                        <td class="align-middle">
                            {{$user->roleToStr()}}
                        </td>


                        <!-- Service Initial -->
                        <td class="align-middle">
                            {{$user->serviceToStr()}}
                        </td>


                        <!-- Eliminado em -->
                        <td class="align-middle">
                            {{ $user->deleted_at->format("d/m/Y") }}
                        </td>



                        <td nowarp class="text-center align-middle">

                                @can("restore",$user)

                                    <form method="get" role="form" action="{{ route('users.restore',$user) }}" class="d-inline-block form_user_id" >
                                        @csrf
                                        @method("PUT")
                                        <button type="submit" class="btn btn-sm btn-outline-success btn-delete mb-1" title="Recuperar utilizador"><i class="fas fa-undo"></i></button>
                                    </form>

                                @else

                                    <span class="btn btn-sm btn-outline-success disabled mb-1"><i class="fas fa-undo"></i></span>

                                 @endcan


                                </td>
                            </tr>

                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>



        </div>


        @endsection




        @section('scripts')

        <script src="{{asset('/js/script_restore_users.js')}}"></script>

        @endsection
