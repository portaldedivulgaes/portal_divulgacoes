@extends('layout.masterbackoffice')

@section('title', 'Editar utilizador | Utilizadores')

@section('styleLinks')
<link href="{{asset('css/style_add_user.css')}}" rel="stylesheet">
@endsection


@section('content')

<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Editar utilizador</h1>
    <a href="javascript:history.go(-1)" class="btn btn-sm btn-primary shadow-sm "><i class="fas fa-chevron-left text-white-50 mr-2"></i>Voltar</a>


</div>


<!-- Basic Card Example -->
<div class="card shadow mb-4">

    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Dados do utilizador</h6>
    </div>


    <div class="card-body">

        <form method="POST" action="{{route('users.update',$user)}}" class="form-group" id="form_user" enctype="multipart/form-data">
            @csrf
            @method("PUT")
            @include('backoffice.users.partials.add-edit')
            <div class="form-group text-right">
                <br><br>
             <button type="button" class="btn btn-sm btn-success mr-1" name="ok" id="buttonSubmit"><i class="far fa-save mr-2"></i>Guardar Dados</button>

             @if (Auth::user()->role=="Admin")
                <a href="{{route('users.index')}}" class="btn btn-sm btn-secondary text-white mr-3">Cancelar</a>
             @else
                <a href="javascript:history.go(-1)" class="btn btn-sm btn-secondary text-white mr-3">Cancelar</a>
             @endif

            </div> </form>

</div>



@endsection


@section('scripts')

<script src="{{asset('/js/script_add_users.js')}}"></script>

@endsection
