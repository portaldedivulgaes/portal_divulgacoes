@extends('layout.masterbackoffice')


@section('title', 'Estatísticas')


@section('styleLinks')
<link href="{{asset('css/style_statistics.css')}}" rel="stylesheet">
@endsection


@section('content')


<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">
        @if (Auth::user()->role=="Admin" )
        Estatísticas
        @else
        Estatísticas do gabinete {{Auth::user()->serviceToStr()}}
        @endif

        
    </h1>

        {{-- <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-download text-white-50  mr-2"></i>Gerar Relatório</a> --}}

</div>

<!-- Content Row -->




    @if (Auth::user()->role=="Admin" )

        {{-- ESTATISTICAS ADMINISTRADOR --}}

        @include('backoffice.statistics.partials.adminstats')


    @else
        {{-- ESTATISTICAS FUNCIONARIO --}}
        @include('backoffice.statistics.partials.userstats')


    @endif




@section('scripts')

@endsection

@endsection
