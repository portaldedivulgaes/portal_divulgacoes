<div class="row">

     <div class="col col-3 monthpubs" style="min-width:310px">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Publicações</h6>
            </div>
            <div class="card-body">
                 <p>Total de visitas às publicações: <strong>{{ $visits }}</strong></p>

                <hr>

                <p>Total de publicações:
                    @if ($totalposts==null)
                    - Sem publicações</p>
                    @else
                    <strong>{{ $totalposts[0]->quant }}</strong></p>
                    @endif

                <hr>

                <p>Tipos de publicação:<br>

                    @if ($postspublished==null)
                        - Sem publicações
                    @else
                        @foreach ($postspublished as $postpublished)
                            @if ($postpublished->type == "Post")
                                - Divulgações:
                            @else
                                - Eventos:
                            @endif

                        <strong>{{$postpublished->quant}}</strong><br>

                        @endforeach
                    @endif

                    </p>

                <hr>
                <p>Eventos publicados: <br>
                    @if ($postevents==null)
                        - Sem eventos publicados
                    @else

                        @foreach ($postevents as $postevent)
                        - {{$postevent->name}}: <strong>{{$postevent->quant}}</strong><br>
                        @endforeach
                    @endif
                </p>
            </div>
          </div>
    </div>

    <div class="col pubsyear">

        <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Publicações por mês ( ano de @php echo date("Y"); @endphp )</h6>
            </div>


            <div class="card-body text-center align-middle" style="overflow:hidden; min-height:300px;">


                <div class="row justify-content-center">

                    @if ($pubsyear==null)
                    <p>- Sem publicações -</p>
                    @else
                        @foreach ($pubsyear as $i=>$pubyear)
                        <div class="card col col-2 m-2 p-3 bg-light " style="min-width:110px">

                            {{$months[$i-1]}}<hr style="width:50%; margin: auto; margin-bottom: 5px"><strong>{{$pubyear}}</strong>
                        </div>
                        @endforeach
                    @endif
                </div>


            </div>
        </div>

    </div>
</div>


    <div class="row">
    <div class="col">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Newsletters</h6>
            </div>
            <div class="card-body">
                <p>Newsletters criadas: <strong>{{$newsletters}}</strong></p>
            </div>
          </div>
    </div>


</div>




<div class="row">
    <div class="col">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Gabinetes</h6>
            </div>
            <div class="card-body">
                <div class="row">

                    <div class="col  mr-2">
                        <strong>Número de publicações por gabinete: </strong><br>
                            @if ($gabposts==null)
                            - Sem publicações
                            @else
                                @foreach ($gabposts as $gabpost)
                                    @if ($gabpost->initial==null)
                                        Administrador: <strong>{{$gabpost->quant}}</strong><br>
                                    @else
                                        {{$gabpost->initial}}: <strong>{{$gabpost->quant}}</strong><br>
                                    @endif
                                @endforeach

                            @endif
                    </div>


                    <div class="col">
                        <strong>Número de Newsletters por gabinete: </strong><br>
                        @if ($gabposts==null || $gabnews==null)
                            - Sem newsletters
                            @else
                                @foreach ($gabnews as $gabnew)
                                    @if ($gabnew->initial==null)
                                        Administrador: <strong>{{$gabnew->quant}}</strong><br>
                                    @else
                                        {{$gabnew->initial}}: <strong>{{$gabnew->quant}}</strong><br>
                                    @endif
                                @endforeach

                            @endif
                    </div>


                </div>

            </div>
          </div>
    </div>
</div>





<div class="row">
    <div class="col">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Funcionários</h6>
            </div>
            <div class="card-body">
              <p>Numero total de funcionários: <strong>{{$totalusers}}</strong></p>

            @if ($hardworker==null)
                <p>Funcionário com maior número de publicações:<strong> Não disponivel</strong></p>
            @else
                <p>Utilizador com maior número de publicações: <strong>{{$hardworker[0]->user}}</strong> - com {{$hardworker[0]->quant}} publicações. ( <a href="{{ route('users.show',$hardworker[0]->user) }}">ver perfil</a> )</p>
            @endif

            </div>
          </div>
    </div>
</div>

