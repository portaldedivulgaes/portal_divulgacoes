@extends('layout.masterbackoffice')

@section('styleLinks')
    <link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endsection

@section('title', 'Rascunhos')

@section('content')
    @include('backoffice.posts.partials.modal')

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Rascunhos</h1>
    </div>

    <!-- Content Row -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Lista dos Rascunhos</h6>
        </div>

        <div class="card-body">
            @if (count($posts))
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Título</th>
                            <th>Texto Curto</th>
                            <th>Tipo de Publicação</th>
                            @if(Auth::user()->isAdmin())<th>Gabinete</th>@endif
                            <th style="width: 100px">Opções</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($posts as $post)
                            <tr>
                                <td>{{$post->title}}</td>
                                <td>{{$post->resume}}</td>
                                <td style="width: 20%">
                                    @switch($post->type)
                                        @case('Post')
                                        Divulgação
                                        @break
                                        @case('Event')
                                        Evento
                                        @break
                                        @default
                                        Divulgação
                                    @endswitch
                                </td>
                                @if(Auth::user()->isAdmin())<td style="width: 20%">{{$post->service->initial}}</td>@endif
                                <td class="text-center align-middle">
                                    <a class="btn btn-sm btn-outline-primary mr-1"
                                       href="{{ route('posts.show',$post->id) }}" title="Ver publicação"><i
                                            class="fas fa-eye"></i></a>

                                    <a class="btn btn-sm btn-outline-warning  mr-1"
                                       href="{{ route('posts.edit',$post->id) }}" title="Editar"><i
                                            class="fas fa-pen"></i></a>
                                    <form method="POST" action="{{route('posts.destroy',$post->id)}}" role="form"
                                          class="d-inline-block form_post_id" title='Eliminar'>
                                        @csrf
                                        @method("DELETE")

                                        <button type="button" class="btn btn-sm btn-outline-danger" data-toggle="modal"
                                                data-target="#modalEliminar" data-title="{{$post->title}}"><i
                                                class="fas fa-trash-alt"></i></button>
                                    </form>
                                </td>

                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <h6 style="text-align: center">Sem Rascunhos Registados</h6>
            @endif
        </div>
    </div>
@endsection

@section("scripts")
    <script>
        $('#dataTable').dataTable({
            "order": [[1, 'desc']],
            'language': {'url': '//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese.json'}
        });

        $(document).ready(function () {
            $('#modalEliminar').on('show.bs.modal', function (e) {
                $("#eliminarDivulgacao").click(function () {
                    $(e.relatedTarget).parent().submit();
                });
                $("#titulo").text($(e.relatedTarget).data("title"));
            });
        });
    </script>
@endsection
