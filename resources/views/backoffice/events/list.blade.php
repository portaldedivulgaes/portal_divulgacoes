@extends('layout.masterbackoffice')

@section('styleLinks')
    <link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endsection

@section('title', 'Eventos')

@section('content')
    @include('backoffice.posts.partials.modal')

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Eventos</h1>
        <a href="{{route('posts.create','Event')}}" class="btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-plus text-white-50 mr-2"></i>Adicionar Evento</a>
    </div>

    <!-- Content Row -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Lista de Eventos</h6>
        </div>
        <div class="card-body">
            @if (count($posts))
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Título</th>
                            <th>Data de Publicação</th>
                            <th>Estado</th>
                            @if(Auth::user()->isAdmin())
                                <th>Gabinete</th>@endif
                            {{--<th>Destaque</th>--}}
                            <th style="width: 50px">Opções</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($posts as $post)
                            <tr>
                                <td>{{$post->title}}</td>
                                <td>{{date('d/m/Y', strtotime($post->created_at))}}</td>
                                <td>@switch($post->status)
                                        @case('Published')
                                        Publicado
                                        @break
                                        @case('Draft')
                                        Rascunho
                                        @break
                                        @case('Archived')
                                        Arquivado
                                        @break
                                        @default
                                        Publicado
                                    @endswitch
                                </td>
                                @if(Auth::user()->isAdmin())
                                    <td>{{$post->service->initial}}</td>@endif
                                {{--<td>{{$post->service->name}}</td>--}}
                                {{--                                <td>
                                                                    <form action="{{route('posts.destaque',$post)}}" method="post">
                                                                        @method('PATCH')
                                                                        @csrf
                                                                        <input type="checkbox" onclick="this.form.submit();">
                                                                        <!--@ if ($post->destaque) checked @ endif-->
                                                                    </form>
                                                                </td>--}}
                                <td nowrap>
                                    <a class="btn btn-sm btn-outline-primary" href="{{route('posts.show',$post->id)}}"
                                       title='Visualizar'><i class="fas fa-eye"></i></a>

                                    <a class="btn btn-sm btn-outline-warning" href="{{route('posts.edit',$post->id)}}"
                                       title='Editar'><i class="fas fa-pen"></i></a>

                                    <form method="POST" action="{{route('posts.destroy',$post->id)}}" role="form"
                                          class="d-inline-block form_post_id" title='Eliminar'>
                                        @csrf
                                        @method("DELETE")

                                        <button type="button" class="btn btn-sm btn-outline-danger" data-toggle="modal"
                                                data-target="#modalEliminar" data-title="{{$post->title}}"><i
                                                class="fas fa-trash-alt"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <h6 style="text-align: center">Sem Eventos Disponíveis</h6>
            @endif
        </div>
    </div>
@endsection

@section("scripts")
    <script>
        $('#dataTable').dataTable({
            "order": [[1, 'desc']],
            'language': {'url': '//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese.json'}
        });

        $(document).ready(function () {
            $('#modalEliminar').on('show.bs.modal', function (e) {
                $("#eliminarDivulgacao").click(function () {
                    $(e.relatedTarget).parent().submit();
                });
                $("#titulo").text($(e.relatedTarget).data("title"));
            });
        });
    </script>
@endsection
