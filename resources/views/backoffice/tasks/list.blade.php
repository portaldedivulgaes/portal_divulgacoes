@extends('layout.masterbackoffice')

@section('title', 'Tarefas')

@section('styleLinks')
<link href="{{asset('css/style_tasks.css')}}" rel="stylesheet">
@endsection

@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Tarefas</h1>
    <a href="#" id="btn_add_task" class="btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#addTaskModal"><i class="fas fa-plus text-white-50 mr-2"></i>Adicionar Tarefa</a>
    @include('backoffice.tasks.partials.add')
</div>

<!-- Content Row -->
<div class="card shadow mb-4">
    <div class="card-header py-3">
        <h6 class="m-0 font-weight-bold text-primary">Lista de tarefas</h6>
    </div>
    <div class="card-body container">
        @if(count($tasks))
        @foreach ($tasks as $task)
        <!-- Task -->
        <div id="taskDiv_{{$task->id}}">
            @php
            if (count($task->subtasks)) {
            $subtaskDone = 0;
            foreach ($task->subtasks as $subtask) {
              if ($subtask->status == "Done") {
                $subtaskDone = $subtaskDone + 1;
              }
            }

            $checkSubTasks = $subtaskDone;
            $totalSubTasks = count($task->subtasks);
            $progressBarTotal = floor(($checkSubTasks/$totalSubTasks)*100);
            }

            if (count($task->subtasks) == 0) {
              $progressBarTotal = 0;
            }
            @endphp
            <!-- Task Title -->
            <div>
                <h3 class="text-dark small font-weight-bold">{{$task->title}}
                    <span class="text-dark float-right">
                        @php if (isset($progressBarTotal)) {
                        echo $progressBarTotal;
                        }else{
                        echo 0;
                        }
                        @endphp%</span>
                </h3>
                <?php
                  if (isset($progressBarTotal)) { ?>
                <div id="progressbar_{{$task->id}}" class="progress mb-4">
                    <div class="progress-bar @if ($progressBarTotal == 100) bg-success @else bg-info @endif" role="progressbar" style="width:
                    @php echo $progressBarTotal;
                    @endphp%" aria-valuenow="
                    @php echo $progressBarTotal;
                    @endphp" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <?php  }else { ?>
            <div class="progress mb-4">
                <div class="progress-bar" role="progressbar" style="width:0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
            </div>
            <?php }?>

        </div>

        <!-- Sub Tasks -->
        <div class="container task">
            @if (count($task->subtasks))
            @foreach ($task->subtasks as $subtask)
            <div class="align-items-center mb-2 task-item">
                @if ($subtask->status == "Done")
                <form id="subtask-update-status-todo-form-{{$subtask->id}}" action="{{route('subtasks.updateStatusTodo', $subtask)}}" method="post" style="display:inline;">
                    @csrf
                    @method('PUT')
                    <a onclick="document.getElementById('subtask-update-status-todo-form-{{$subtask->id}}').submit();">
                        <div class="checkbox-active"><i class="fas fa-check check-item align-middle"></i></div>
                    </a>
                </form>
                <h4 class="small font-weight-bold task-item-title" id="task-item-title" style="text-decoration:line-through; font-style:italic; color:#bbbdc3;">{{$subtask->title}}</h4>
                @else
                <form id="subtask-update-status-done-form-{{$subtask->id}}" action="{{route('subtasks.updateStatusDone', $subtask)}}" method="post" style="display:inline;">
                    @csrf
                    @method('PUT')
                    <a onclick="document.getElementById('subtask-update-status-done-form-{{$subtask->id}}').submit();">
                        <div class="task-checkbox"></div>
                    </a>
                </form>
                <h4 class="small font-weight-bold task-item-title" id="task-item-title">{{$subtask->title}}</h4>
                @endif
                <form class="subtask-form-delete" action="{{route('subtasks.destroy', $subtask)}}" method="post" style="display:inline;">
                    @csrf
                    @method('DELETE')
                    <button class="d-none d-sm-inline-block btn btn-sm subtask-delete-option" type="submit">
                        <span class="text subtask-single-button">
                            <i class="fas fa-trash-alt"></i>
                        </span>
                    </button>
                </form>
                <form class="subtask-update-form" method="POST" action="{{route('subtasks.update', $subtask)}}" enctype="multipart/form-data" autocomplete="off">
                    @csrf
                    @method('PUT')
                    <input type="hidden" name="tasks_id" value="{{$task->id}}">
                </form>
            </div>
            @endforeach
            @else
            <div class="d-sm-flex align-items-center mb-2 task-item-mute">
                <div class="task-checkbox-mute"></div>
                <h4 class="small font-weight-bold task-item-title-mute">Adicione um item</h4>
            </div>
            @endif
        </div>

        <!-- Action Buttons Tasks -->
        <div class="text-right container subTask-buttons">
            <div class="subtask-buttons">
                <form class="subtask-create-form" method="POST" action="{{route('subtasks.store')}}" enctype="multipart/form-data" autocomplete="off">
                    @csrf
                    <input type="hidden" name="tasks_id" value="{{$task->id}}">
                </form>
            </div>
            <div>
                <button class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm add-subtask-buttons">
                    <span class="text">Adicionar Item</span>
                </button>
                <div class="dropdown dropdown-task-options no-arrow">
                    <button class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <span class="text subtask-single-button"><i class="fas fa-ellipsis-v"></i></span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenuButton">
                        <h6 class="dropdown-header text-center">Opções da tarefa:</h6>
                        <button class="dropdown-item" href="#" data-toggle="modal" data-target="#editTaskModal" data-name="{{$task->title}}" data-id="{{$task->id}}">Editar</button>
                        <button type="submit" class="dropdown-item" data-toggle="modal" data-target="#deleteTaskModal" data-name="{{$task->title}}" data-id="{{$task->id}}">Eliminar Tarefa</button>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <br>
    </div>
    @endforeach
    @else
    <h6 style="text-align: center">Sem Tarefas Registadas</h6>
    @endif
</div>
</div>

<!-- Modal Edit Task -->
<div class="modal fade" id="editTaskModal" tabindex="-1" role="dialog" aria-labelledby="editTaskModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header align-items-center">
                <h6 class="modal-title text-dark font-weight-bold">Editar Tarefa</h6>
                <button type="button" name="button" class="d-none d-sm-inline-block btn btn-sm close-modal-button" data-dismiss="modal">
                    <span>
                        <i class="fas fa-times"></i>
                    </span>
                </button>
            </div>
            <span class="line"></span>
            <div class="modal-body">
                <form method="POST" action="" class="form-group" enctype="multipart/form-data" autocomplete="off">
                    @csrf
                    @method('PUT')
                    @include('backoffice.tasks.partials.edit')
                    <input type="hidden" id="task_edit_id" name="id" value="">
            </div>
            <div class="modal-footer">
                <button href="#" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm" type="submit">
                    <span class="text">Atualizar</span>
                </button>
                <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm" data-dismiss="modal">
                    <span class="text">Fechar</span>
                </a>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Delete Task -->
<div class="modal fade" id="deleteTaskModal" tabindex="-1" role="dialog" aria-labelledby="deleteTaskModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header align-items-center">
                <h6 class="modal-title text-dark font-weight-bold">Eliminar Tarefa</h6>
                <button type="button" name="button" class="d-none d-sm-inline-block btn btn-sm close-modal-button" data-dismiss="modal">
                    <span>
                        <i class="fas fa-times"></i>
                    </span>
                </button>
            </div>
            <span class="line"></span>
            <div class="modal-body">
                <form method="POST" action="" class="form-group" enctype="multipart/form-data" autocomplete="off">
                    @csrf
                    @method('DELETE')
                    <h4 class="small font-weight-bold text-dark task-title">Pretende eliminar esta tarefa?</h4>
                    <input id="task_delete_title" class="task-title-input" type="text" name="title" placeholder="Adicione um título" maxlength="150" disabled>
                    <input type="hidden" id="task_delete_id" name="id" value="">
            </div>
            <div class="modal-footer">
                <button href="#" class="d-none d-sm-inline-block btn btn-sm btn-danger shadow-sm" type="submit">
                    <span class="text">Eliminar</span>
                </button>
                <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm" data-dismiss="modal">
                    <span class="text">Cancelar</span>
                </a>
            </div>
        </div>
        </form>
    </div>
</div>


@section('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        // Edit Task Modal jQuery
        $('#editTaskModal').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget)
            var modal = $(this)
            modal.find('#task_edit_title').val(button.data('name'));
            modal.find('#task_edit_id').val(button.data('id'));
            modal.find("form").attr('action', '/backoffice/tasks/' + button.data('id'));
        });

        // Delete Task Modal jQuery
        $('#deleteTaskModal').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget)
            var modal = $(this)
            modal.find('#task_delete_title').val(button.data('name'));
            modal.find('#task_delete_id').val(button.data('id'));
            modal.find("form").attr('action', '/backoffice/tasks/' + button.data('id'));
        })
    });
</script>
<script src="/js/script_tasks.js" charset="utf-8"></script>
@endsection

@endsection
