<!-- Modal Add Task -->
<div class="modal fade" id="addTaskModal" tabindex="-1" role="dialog" aria-labelledby="addTaskModal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header align-items-center">
                <h6 class="modal-title text-dark font-weight-bold">Adicionar Tarefa</h6>
                <button type="button" name="button" class="d-none d-sm-inline-block btn btn-sm close-modal-button" data-dismiss="modal">
                    <span>
                        <i class="fas fa-times"></i>
                    </span>
                </button>
            </div>
            <span class="line"></span>
                <div class="modal-body">
                  <form method="POST" id="form_add" action="{{route('tasks.store')}}" class="form-group" enctype="multipart/form-data" autocomplete="off">
                    @csrf
                    <h4 class="small font-weight-bold text-dark task-title">Título da Tarefa</h4>
                    <input class="task-title-input" type="text" name="title" placeholder="Adicione um título" maxlength="150">
                </div>
                <div class="modal-footer">
                    <button href="#" id="button_add" class="d-none d-sm-inline-block btn btn-sm btn-success shadow-sm" type="submit">
                        <span class="text">Adicionar</span>
                    </button>
                    <a href="#" class="d-none d-sm-inline-block btn btn-sm btn-secondary shadow-sm" data-dismiss="modal">
                        <span class="text">Fechar</span>
                    </a>
                </div>
            </form>
        </div>
    </div>
</div>
