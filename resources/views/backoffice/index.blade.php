@extends('layout.masterbackoffice')

@section('title', 'Dashboard')

@section('styleLinks')
<link href="{{asset('css/style_index_backoffice.css')}}" rel="stylesheet">
@endsection

@section('content')
<!-- Page Heading -->
<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
</div>

<!-- Content Row -->
<div class="row">

    <!-- List all the posts -->
    <div class="col-xl-3 col-md-6 mb-4" data-sal-duration="1000" data-sal="slide-down" data-sal-delay="50" data-sal-easing="ease-out-bounce">
        <div class="card border-left-primary shadow h-100 py-2">
            <a href="{{route('posts.index')}}" style="text-decoration: none;">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Publicações</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                @if ($postsPublished == 1)
                                {{$postsPublished}} Publicação
                                @else
                                {{$postsPublished}} Publicações
                                @endif
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>

    <!-- List all the drafts -->
    <div class="col-xl-3 col-md-6 mb-4" data-sal-duration="1000" data-sal="slide-down" data-sal-delay="100" data-sal-easing="ease-out-bounce">
        <div class="card border-left-success shadow h-100 py-2">
            <a href="{{route('drafts.index')}}" style="text-decoration: none;">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Rascunhos</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                @if ($postsDraft == 1)
                                {{$postsDraft}} Rascunho
                                @else
                                {{$postsDraft}} Rascunhos
                                @endif
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-archive fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>

    <!-- List all the newsletters -->
    <div class="col-xl-3 col-md-6 mb-4" data-sal-duration="1000" data-sal="slide-down" data-sal-delay="150" data-sal-easing="ease-out-bounce">
        <div class="card border-left-warning shadow h-100 py-2">
            <a href="{{route('newsletters.index')}}" style="text-decoration: none;">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Newsletters</div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">
                                @if ($newsletters == 1)
                                {{$newsletters}} Newsletter
                                @else
                                {{$newsletters}} Newsletters
                                @endif
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-envelope fa-2x text-gray-300" style="font-size:24pt;"></i>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </div>

    <!-- List all the tasks -->
    <div class="col-xl-3 col-md-6 mb-4" data-sal-duration="1000" data-sal="slide-down" data-sal-delay="200" data-sal-easing="ease-out-bounce">
        <div class="card border-left-info shadow h-100 py-2">
            <a href="{{route('tasks.index')}}" style="text-decoration: none;">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Tarefas</div>
                            <div class="row no-gutters align-items-center">
                                @if ($tasks == 0)
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">Sem Tarefas</div>
                                </div>
                                @elseif ($subtasksDone == 0)
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">0%</div>
                                </div>
                                <div class="col">
                                    <div class="progress progress-sm mr-2">
                                        <div class="progress-bar bg-info" role="progressbar" style="width:0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                @else
                                <div class="col-auto">
                                    <div class="h5 mb-0 mr-3 font-weight-bold text-gray-800">
                                        @php $subtasksComplete = floor(($subtasksDone/$subtasks)*100); echo $subtasksComplete;
                                        @endphp%</div>
                                    </div>
                                    <div class="col">
                                        <div class="progress progress-sm mr-2">
                                            <div class="progress-bar bg-info" role="progressbar" style="width: @php echo $subtasksComplete;@endphp%" aria-valuenow="
                                            @php echo $subtasksComplete;
                                            @endphp" aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                </div>
                                @endif
                            </div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-tasks fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
        </div>
        </a>
    </div>
</div>

<!-- Content Row -->

<div class="row">

    <!-- Area Chart -->
    <div class="col-xl-8 col-lg-7">
        <div class="card shadow mb-4">
            <!-- Card Header - Dropdown -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Publicações Por Mês (2020)</h6>
                <div class="dropdown no-arrow">
                    <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fas fa-ellipsis-v fa-sm fa-fw text-gray-400"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right shadow animated--fade-in" aria-labelledby="dropdownMenuLink">
                        <div class="dropdown-header">Exportar Gráfico</div>
                        <a class="dropdown-item" href="#">Excel</a>
                        <a class="dropdown-item" href="#">PDF</a>
                        <a class="dropdown-item" href="#">JPEG</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="#">Partilhar</a>
                    </div>
                </div>
            </div>
            <!-- Card Body -->
            <div class="card-body">
                <div class="chart-area">
                    <canvas id="myAreaChart"></canvas>
                </div>
            </div>
        </div>
    </div>

    <!-- Notes -->
    <div class="col-xl-4 col-lg-5">
        <div class="card shadow mb-4">
            <!-- Card Header -->
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Notas</h6>
            </div>
            <!-- Card Body -->
            <div class="card-body card-notes">
                @if (count($notes))
                @foreach ($notes as $note)
                <div class="note">
                    <div class="row d-sm-flex align-items-center">
                        <div class="col-md-10">
                            <h4 class="small font-weight-bold note-title">{{$note->title}}</h4>
                            <form class="note-update-form" action="{{route('notes.update', $note)}}" method="post" autocomplete="off">
                                @csrf
                                @method('PUT')
                            </form>
                        </div>
                        <div class="col-md-2">
                            <form class="note-delete-form" action="{{route('notes.destroy', $note)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="d-none d-sm-inline-block btn btn-sm note-delete-button" type="submit">
                                    <span class="text">
                                        <i class="fas fa-trash-alt"></i>
                                    </span>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
                @endforeach
                @else
                <div class="without-note text-center">
                    <h6 class="font-weight-bold">Sem Notas Registadas</h6>
                </div>
                @endif
            </div>
            <div class="card-footer align-items-center">
                <form class="note-add-form" action="{{route('notes.store')}}" method="post" autocomplete="off">
                    @csrf
                    <div class="row">
                        <div class="col-md-10">
                            <input class="note-add-input" type="text" name="title" placeholder="Adicione uma nota">
                        </div>
                        <div class="col-md-2">
                            <button class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm note-add-button" type="submit">
                                <span class="text">Adicionar</span>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- Content Row -->
<div class="row">
    <div class="col-lg-7 mb-4">
        <!-- Illustrations -->
        <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Bem Vindo ao Portal de Divulgações!</h6>
                <a href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-times fa-sm fa-fw text-gray-400"></i>
                </a>
            </div>
            <div class="card-body">
                <div class="text-center">
                    <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;" src="/img/undraw_posting_photo.svg" alt="">
                </div>
                <p>Bem Vindo ao backoffice do Portal de Divulgações! Nesta nova versão pode encontrar todas as funcionalidades que precisa para gerir as suas publicações de uma forma mais simples e moderna.</p>
            </div>
        </div>
    </div>

    <div class="col-lg-5 mb-4">
        <!-- Illustrations -->
        <div class="card shadow mb-4">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h6 class="m-0 font-weight-bold text-primary">Precisa de ajuda?</h6>
                <a href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-times fa-sm fa-fw text-gray-400"></i>
                </a>
            </div>
            <div class="card-body">
                <div class="text-center">
                    <img class="img-fluid px-3 px-sm-4 mt-3 mb-4" style="width: 25rem;" src="/img/undraw_observations_mejb.svg" alt="">
                </div>
                <p>Para qualquer dúvida ou questão visite as <a target="_blank" rel="nofollow" href="/backoffice/faqs"> FAQ's do Portal de Divulgações </a>. Se a sua dúvida não constar na lista, não hesite em contactar um dos desenvolvedores do
                    projeto.</p>
            </div>
        </div>
    </div>
</div>

@section('scripts')

<!-- Plugin Charts -->
<script src="{{asset('/vendor/chart.js/Chart.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.bundle.js"></script>

<!-- Charts -->
<script src="{{asset('/js/charts/chart-area-demo.js')}}"></script>
<script src="{{asset('/js/charts/chart-pie-demo.js')}}"></script>

<script type="text/javascript">
    sal();
</script>

<script src="/js/script_index_backoffice.js" charset="utf-8"></script>
@endsection

@endsection
