{{-- ALerta de Erro --}}
<div class="alert alert-danger" role="alert" id="error_alert_{{$action}}">
     <h6 style="font-size: 15px; margin-top: 7px">erro</h6>
</div>

<div class="form-group">
    <label for="targetaudience_{{$action}}_name">Designação</label>
    <input type="text" class="form-control" name="name" id="targetaudience_{{$action}}_name">
  </div>
  <div class="form-group">
    <label for="targetaudience_{{$action}}_email">Email</label>
    <input type="email" class="form-control" name="email" id="targetaudience_{{$action}}_email">
  </div>
