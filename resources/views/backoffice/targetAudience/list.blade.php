@extends('layout.masterbackoffice')

@section('title', 'Público Alvo')

@section('styleLinks')
<link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endsection

@section('content')

  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Público Alvo</h1>
    <button id="btn_add_targetAudience" class="btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#ModalTargetAudienceAdd"><i class="fas fa-plus text-white-50 mr-2"></i>Adicionar Público Alvo</button>
  </div>

  <!-- Content Row -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Lista de Público Alvo</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="tableTargetAudience" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Designação</th>
              <th>Email</th>
              <th>Opções</th>
            </tr>
          </thead>

            <tbody>
              @foreach($targetAudiences as $targetAudience)
                <tr id="{{$targetAudience->id}}">
                  <td>{{$targetAudience->name}}</td>
                  <td>{{$targetAudience->email}}</td>
                    <td class="text-center align-middle">
                      <button class="btn btn-sm btn-outline-warning mb-1" data-toggle="modal" data-target="#ModalTargetAudienceEdit" data-id='{{$targetAudience->id}}' data-name='{{$targetAudience->name}}' data-email='{{$targetAudience->email}}' title="Editar Público Alvo"><i class="fas fa-pen"></i></button>
                      <button class="btn btn-sm btn-outline-danger btn-delete mb-1" data-toggle="modal" data-target="#ModalTargetAudienceDelete" data-id='{{$targetAudience->id}}' data-name='{{$targetAudience->name}}' data-email='{{$targetAudience->email}}' title="Eliminar Público Alvo"><i class="fas fa-trash-alt"></i></button>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <!-- Modal Add TargetAudience-->
      <div class="modal fade" id="ModalTargetAudienceAdd" role="dialog">
        <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <form class="" id="form_add" action="{{route('targetaudiences.store')}}" method="post">
              @csrf
              <div class="modal-header">
                <h5 class="modal-title">Adicionar Público Alvo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                @include('backoffice.targetAudience.partials.add-edit', ['action' => 'add'])
              </div>
              <div class="modal-footer">
                <button type="submit"class="btn btn-sm btn-primary">Guardar</button>
                <button type="cancel" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- Modal Edit TargetAudience-->
      <div class="modal fade" id="ModalTargetAudienceEdit" role="dialog">
        <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <form class="" id="form_edit" action="" method="post">
              @method('PUT')
              @csrf
              <div class="modal-header">
                <h5 class="modal-title">Editar Público Alvo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                @include('backoffice.targetAudience.partials.add-edit', ['action' => 'edit'])
                <input type="hidden" id="targetaudience_edit_id" name="id" value="">
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-sm btn-primary">Guardar</button>
                <button type="cancel" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- Modal Delete TargetAudience-->
      <div class="modal fade" id="ModalTargetAudienceDelete" role="dialog">
        <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <form class="" id="form_delete" action="" method="post">
              @method('DELETE');
              @csrf
              <div class="modal-header">
                <h5 class="modal-title">Eliminar Público Alvo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <p>Tem a certeza que pretende eliminar o Público Alvo "<b><span id="designacaoPublico"></span></b>"?</p>
                <input type="hidden" id="targetaudience_delete_id" name="id" value="">
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-sm btn-danger">Confirmar</button>
                <button type="cancel" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    @endsection



    @section("scripts")
      <script>
      $(document).ready(function() {

        $('#error_alert_add').hide();
        $('#error_alert_edit').hide();

        var table=$('#tableTargetAudience').DataTable( {
          "columnDefs": [
            { targets:-1, 'width': '70px', 'orderable':false, 'className': 'text-center align-middle'},
          ],
          "language": {
            "lengthMenu":  "Mostrar _MENU_ registos por página",
            "search":      "Procurar:",
            "zeroRecords": "Sem registos",
            "paginate": {
              "first":      "Primeiro",
              "last":       "Ultimo",
              "next":       "Proximo",
              "previous":   "Anterior"
            },

            "info": "A mostrar página _PAGE_ de _PAGES_",
            "infoEmpty": "Sem registos disponiveis",
            "infoFiltered": "(filtrado de um total de _MAX_ registos)"
          },
          "order": [0, 'asc']
        } );


        $('#form_add').submit(function(){
          var data=$(this).serialize();
          $.ajax({
            url: "{{route('targetaudiences.store')}}",
            data: data,
            method: 'POST',
            success: function(data){
              table.row.add([data['name'], data['email'], "<button class='btn btn-sm btn-outline-warning mb-1' data-toggle='modal' data-target='#ModalTargetAudienceEdit' data-id='"+data['id']+"' data-name='"+data['name']+"' data-email='"+data['email']+"' title='Editar Público Alvo'><i class='fas fa-pen'></i></button> <button class='btn btn-sm btn-outline-danger btn-delete mb-1' data-toggle='modal' data-target='#ModalTargetAudienceDelete' data-id='"+data['id']+"' data-name='"+data['name']+"' data-email='"+data['email']+"' title='Eliminar Público Alvo'><i class='fas fa-trash-alt'></i></button>"]).node().id = data['id'];
              table.draw();
              $("#ModalTargetAudienceAdd").modal('hide');
            },
            error: function(data){
              var errors=data['responseJSON']['errors']
              var string="";
              $.each(errors, function( index, value ) {
                string+=value+"<br>";
              });;
              $("#error_alert_add").find('h6').html(string);
              $("#error_alert_add").fadeTo(4000, 500).slideUp();
            }
          });
          return false;
        });

        $('#form_edit').submit(function(){
          var data=$(this).serialize();
          var idTargetAudience=$("#targetaudience_edit_id").val();
          $.ajax({
            url: "/backoffice/targetaudiences/"+idTargetAudience,
            data: data,
            method: 'POST',
            success: function(data){
              table.row($('#'+data['id'])).data([data['name'], data['email'], "<button class='btn btn-sm btn-outline-warning mb-1' data-toggle='modal' data-target='#ModalTargetAudienceEdit' data-id='"+data['id']+"' data-name='"+data['name']+"' data-email='"+data['email']+"' title='Editar Público Alvo'><i class='fas fa-pen'></i></button> <button class='btn btn-sm btn-outline-danger btn-delete mb-1' data-toggle='modal' data-target='#ModalTargetAudienceDelete' data-id='"+data['id']+"' data-name='"+data['name']+"' data-email='"+data['email']+"' title='Eliminar Público Alvo'><i class='fas fa-trash-alt'></i></button>"]).draw();
              $("#ModalTargetAudienceEdit").modal('hide');
            },
            error: function(data){
              var errors=data['responseJSON']['errors']
              var string="";
              $.each(errors, function( index, value ) {
                string+=value+"<br>";
              });;
              $("#error_alert_edit").find('h6').html(string);
              $("#error_alert_edit").fadeTo(4000, 500).slideUp();
            }
          });
          return false;
        });

        $('#form_delete').submit(function(){
          var data=$(this).serialize();
          var idTargetAudience=$("#targetaudience_delete_id").val();
          $.ajax({
            url: "/backoffice/targetaudiences/"+idTargetAudience,
            data: data,
            method: 'POST',
            success: function(data){
              table.row($('#'+idTargetAudience)).remove().draw();
              $("#ModalTargetAudienceDelete").modal('hide');
            }
          });
          return false;
        });

        $('#ModalTargetAudienceEdit').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget) // Button that triggered the modal
          var modal = $(this)
          modal.find('#targetaudience_edit_name').val(button.data('name'));
          modal.find('#targetaudience_edit_email').val(button.data('email'));
          modal.find('#targetaudience_edit_id').val(button.data('id'));
        });

        $('#ModalTargetAudienceDelete').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget) // Button that triggered the modal
          var modal = $(this)
          modal.find('#designacaoPublico').html(button.data('name'));
          modal.find('#targetaudience_delete_id').val(button.data('id'));
        })

        $("#btn_add_targetAudience").click(function(){
          var modal = $('#ModalTargetAudienceAdd');
          modal.find('#targetaudience_add_name').val('');
          modal.find('#targetaudience_add_email').val('');
        });

      });

      </script>
    @endsection
