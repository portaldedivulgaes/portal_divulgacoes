
{{-- ALerta de Erro --}}
<div class="alert alert-danger" role="alert" id="error_alert_{{$action}}">
     <h6 style="font-size: 15px; margin-top: 7px">erro</h6>
</div>
<div class="form-group">
    <label for="eventtype_name">Designação</label>
      <input type="text" class="form-control" name="name" id="eventtype_{{$action}}_name">
  </div>
  <div class="form-group">
    <label for="eventtype_icon">Ícone</label>
    <br>
    <div class="btn-group">
        <button data-selected="graduation-cap" type="button" class="icp demo btn btn-secondary dropdown-toggle iconpicker-component" data-toggle="dropdown">
            <i id="eventtype_{{$action}}_icon" class="fas fa-puzzle-piece"></i>
            <span class="caret"></span>
        </button>
        <div class="dropdown-menu"></div>
    </div>
  </div>
  @if(Auth::user()->isAdmin())
    <div class="form-group">
      <label for="eventtype_service">Gabinete</label>
      <select class="form-control" id="eventtype_{{$action}}_service" name="service_id">
        @foreach ($services as $service)
          <option value="{{$service->id}}">{{$service->initial}}</option>
        @endforeach
      </select>
    </div>
  @else
    <input type="hidden" id="eventtype_{{$action}}_service" name="service_id" value="{{ Auth::user()->service_id }}">
  @endif
