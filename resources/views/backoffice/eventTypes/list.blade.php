@extends('layout.masterbackoffice')

@section('title', 'Tipos de Publicação')

@section('styleLinks')
<link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
<link rel="stylesheet" href="{{asset('vendor/iconpicker/dist/css/fontawesome-iconpicker.min.css')}}">
@endsection

@section('content')

  <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Tipos de Publicação</h1>
    <button id="btn_add_eventType" class="btn btn-sm btn-primary shadow-sm" data-toggle="modal" data-target="#ModalEventTypesAdd"><i class="fas fa-plus text-white-50 mr-2"></i>Adicionar Tipo de Publicação</button>
  </div>

  <!-- Content Row -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary">Lista de Tipos de Publicação</h6>
    </div>
    <div class="card-body">
      <div class="table-responsive">
        <table class="table table-bordered" id="tableEventyTypes" width="100%" cellspacing="0">
          <thead>
            <tr>
              <th>Designação</th>
              <th>Ícone</th>
              @if(Auth::user()->isAdmin())<th>Gabinete</th>@endif
                <th>Opções</th>
              </tr>
            </thead>

            <tbody>
              @foreach($eventTypes as $eventType)
                <tr id="{{$eventType->id}}">
                  <td>{{$eventType->name}}</td>
                  <td><i class="{{$eventType->icon}}"></i> <span style="font-size:12px;">({{$eventType->icon}})</span></td>
                  @if(Auth::user()->isAdmin())<td>{{$eventType->service->initial}}</td>@endif
                    <td class="text-center align-middle">
                      <button class="btn btn-sm btn-outline-warning mb-1" data-toggle="modal" data-target="#ModalEventTypesEdit" data-id='{{$eventType->id}}' @if(Auth::user()->isAdmin()) data-service-id='{{$eventType->service_id}}' @endif data-name='{{$eventType->name}}' data-icon='{{$eventType->icon}}' title="Editar Tipo de Publicação"><i class="fas fa-pen"></i></button>
                      <button class="btn btn-sm btn-outline-danger btn-delete mb-1" data-toggle="modal" data-target="#ModalEventTypesDelete" data-id='{{$eventType->id}}' @if(Auth::user()->isAdmin()) data-service-id='{{$eventType->service_id}}' @endif data-name='{{$eventType->name}}' data-icon='{{$eventType->icon}}' title="Eliminar Tipo de Publicação"><i class="fas fa-trash-alt"></i></button>
                    </td>
                  </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
      </div>

      <!-- Modal Add EventType-->
      <div class="modal fade" id="ModalEventTypesAdd" role="dialog">
        <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <form class="" id="form_add" action="{{route('eventtypes.store')}}" method="post">
              @csrf
              <div class="modal-header">
                <h5 class="modal-title">Adicionar Tipo de Publicação</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                @include('backoffice.eventTypes.partials.add-edit', ['action' => 'add'])
              </div>
              <div class="modal-footer">
                <button type="submit"class="btn btn-sm btn-primary">Guardar</button>
                <button type="cancel" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- Modal Edit EventType-->
      <div class="modal fade" id="ModalEventTypesEdit" role="dialog">
        <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <form class="" id="form_edit" action="" method="post">
              @method('PUT')
              @csrf
              <div class="modal-header">
                <h5 class="modal-title">Editar Tipo de Publicação</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                @include('backoffice.eventTypes.partials.add-edit', ['action' => 'edit'])
                <input type="hidden" id="eventtype_edit_id" name="id" value="">
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-sm btn-primary">Guardar</button>
                <button type="cancel" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- Modal Delete EventType-->
      <div class="modal fade" id="ModalEventTypesDelete" role="dialog">
        <div class="modal-dialog modal-md" role="document">
          <div class="modal-content">
            <form class="" id="form_delete" action="" method="post">
              @method('DELETE')
              @csrf
              <div class="modal-header">
                <h5 class="modal-title">Eliminar Tipo de Publicação</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <p>Tem a certeza que pretende eliminar o Tipo de Publicação "<b><span id="designacaoTipo"></span></b>"?</p>
                <input type="hidden" id="eventtype_delete_id" name="id" value="">
              </div>
              <div class="modal-footer">
                <button type="submit" class="btn btn-sm btn-danger">Confirmar</button>
                <button type="cancel" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    @endsection



    @section("scripts")

      <script src="{{asset('vendor/iconpicker/dist/js/fontawesome-iconpicker.js')}}"></script>

      <script>
      $(document).ready(function() {

        $('.demo').iconpicker();
        $('#error_alert_add').hide();
        $('#error_alert_edit').hide();

        var table=$('#tableEventyTypes').DataTable( {
          "columnDefs": [
            { targets:-1, 'width': '70px', 'orderable':false, 'className': 'text-center align-middle'},
          ],
          "language": {
            "lengthMenu":  "Mostrar _MENU_ registos por página",
            "search":      "Procurar:",
            "zeroRecords": "Sem registos",
            "paginate": {
              "first":      "Primeiro",
              "last":       "Ultimo",
              "next":       "Próximo",
              "previous":   "Anterior"
            },

            "info": "A mostrar página _PAGE_ de _PAGES_",
            "infoEmpty": "Sem registos disponiveis",
            "infoFiltered": "(filtrado de um total de _MAX_ registos)"
          },
          @if(Auth::user()->isAdmin()) "order": [[2, 'asc'],[0, 'asc']]
          @else "order": [0, 'asc'] @endif

        } );


        $('#form_add').submit(function(){
          var data=$(this).serialize();
          data+=('&icon='+$('#eventtype_add_icon').attr('class'));
          $.ajax({
            url: "{{route('eventtypes.store')}}",
            data: data,
            method: 'POST',
            success: function(data){
              table.row.add([data['name'], "<i class='"+data['icon']+"'></i> <span style='font-size:12px;'>("+data['icon']+")</span>", @if(Auth::user()->isAdmin()) data['service']['initial'], @endif "<button class='btn btn-sm btn-outline-warning mb-1' data-toggle='modal' data-target='#ModalEventTypesEdit' data-id='"+data['id']+"' @if(Auth::user()->isAdmin()) data-service-id='"+data['service_id']+"' @endif data-name='"+data['name']+"' data-icon='"+data['icon']+"' title='Editar Tipo de Publicação'><i class='fas fa-pen'></i></button> <button class='btn btn-sm btn-outline-danger btn-delete mb-1' data-toggle='modal' data-target='#ModalEventTypesDelete' data-id='"+data['id']+"' data-name='"+data['name']+"' data-icon='"+data['icon']+"' title='Eliminar Tipo de Publicação'><i class='fas fa-trash-alt'></i></button>"]).node().id = data['id'];
              table.draw();
              $("#ModalEventTypesAdd").modal('hide');
            },
            error: function(data){
              var errors=data['responseJSON']['errors']
              var string="";
              $.each(errors, function( index, value ) {
                string+=value+"<br>";
              });;
              $("#error_alert_add").find('h6').html(string);
              $("#error_alert_add").fadeTo(4000, 500).slideUp();
            }
          });
          return false;
        });

        $('#form_edit').submit(function(){
          var data=$(this).serialize();
          var idEventType=$("#eventtype_edit_id").val();
          data+=('&icon='+$('#eventtype_edit_icon').attr('class'));
          $.ajax({
            url: "/backoffice/eventtypes/"+idEventType,
            data: data,
            method: 'POST',
            success: function(data){
              console.log(data);
              table.row($('#'+data['id'])).data([data['name'], "<i class='"+data['icon']+"'></i> <span style='font-size:12px;'>("+data['icon']+")</span>", @if(Auth::user()->isAdmin()) data['service']['initial'], @endif "<button class='btn btn-sm btn-outline-warning mb-1' data-toggle='modal' data-target='#ModalEventTypesEdit' data-id='"+data['id']+"' @if(Auth::user()->isAdmin()) data-service-id='"+data['service_id']+"' @endif data-name='"+data['name']+"' data-icon='"+data['icon']+"' title='Editar Tipo de Publicação'><i class='fas fa-pen'></i></button> <button class='btn btn-sm btn-outline-danger btn-delete mb-1' data-toggle='modal' data-target='#ModalEventTypesDelete' data-id='"+data['id']+"' data-name='"+data['name']+"' data-icon='"+data['icon']+"' title='Eliminar Tipo de Publicação'><i class='fas fa-trash-alt'></i></button>"]).draw();
              $("#ModalEventTypesEdit").modal('hide');
            },
            error: function(data){
              var errors=data['responseJSON']['errors']
              var string="";
              $.each(errors, function( index, value ) {
                string+=value+"<br>";
              });;
              $("#error_alert_edit").find('h6').html(string);
              $("#error_alert_edit").fadeTo(4000, 500).slideUp();
            }
          });
          return false;
        });

        $('#form_delete').submit(function(){
          var data=$(this).serialize();
          var idEventType=$("#eventtype_delete_id").val();
          $.ajax({
            url: "/backoffice/eventtypes/"+idEventType,
            data: data,
            method: 'POST',
            success: function(data){
              table.row($('#'+idEventType)).remove().draw();
              $("#ModalEventTypesDelete").modal('hide');
            },
            error: function(){
              alert('error!');
            }
          });
          return false;
        });

        $('#ModalEventTypesEdit').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget) // Button that triggered the modal
          var modal = $(this)
          modal.find('#eventtype_edit_name').val(button.data('name'));
          modal.find('#eventtype_edit_icon').attr('class', button.data('icon'));
          modal.find('#eventtype_edit_id').val(button.data('id'));
          @if(Auth::user()->isAdmin())
            modal.find('#eventtype_edit_service').val(button.data('service-id'));
          @endif
        });

        $('#ModalEventTypesDelete').on('show.bs.modal', function (event) {
          var button = $(event.relatedTarget) // Button that triggered the modal
          var modal = $(this)
          modal.find('#designacaoTipo').html(button.data('name'));
          modal.find('#eventtype_delete_id').val(button.data('id'));
        })

        $("#btn_add_eventType").click(function(){
          var modal = $('#ModalEventTypesAdd');
          modal.find('#eventtype_add_name').val('');
        });

      });

      </script>
    @endsection
