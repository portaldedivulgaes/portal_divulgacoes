@extends('layout.masterbackoffice')

@section('title', 'Detalhe '.($menuPostType=='Post'?'da Divulgação':$menuPostType=='Evdent'?'do Evento':'do Rascunho').' | Divulgações')

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        @if($post->type === 'Post' && $post->status !== 'Draft')
            <h1 class="h3 mb-0 text-gray-800">Visualização da Divulgação</h1>
            <div>
                <a href="{{route('posts.index')}}" class="btn btn-sm btn-primary shadow-sm mr-2"><i
                        class="fas fa-chevron-left text-white-50 mr-2"></i>Voltar à listagem</a>
                <a class="btn btn-sm btn-warning shadow-sm" href="{{route('posts.edit',$post)}}"
                   title='Editar Divulgação'><i class="fas fa-pen text-white-50 mr-2"></i>Editar Divulgação</a>
            </div>
        @elseif($post->status === 'Draft')
            <h1 class="h3 mb-0 text-gray-800">Visualização do Rascunho | <small style="font-size: 13pt"> Não Publicado</small></h1>
            <div>
                <a href="{{route('drafts.index')}}" class="btn btn-sm btn-primary shadow-sm mr-2"><i
                        class="fas fa-chevron-left text-white-50 mr-2"></i>Voltar à listagem</a>
                <a class="btn btn-sm btn-warning shadow-sm" href="{{route('posts.edit',$post)}}"
                   title='Editar Divulgação'><i class="fas fa-pen text-white-50 mr-2"></i>Editar Rascunho</a>
            </div>
        @else
            <h1 class="h3 mb-0 text-gray-800">Visualização do Evento</h1>
            <div>
                <a href="{{route('events.index')}}" class="btn btn-sm btn-primary shadow-sm mr-2"><i
                        class="fas fa-chevron-left text-white-50 mr-2"></i>Voltar à listagem</a>
                <a class="btn btn-sm btn-warning shadow-sm" href="{{route('posts.edit',$post)}}"
                   title='Editar Divulgação'><i class="fas fa-pen text-white-50 mr-2"></i>Editar Evento</a>
            </div>

        @endif
    </div>

    <div class="card shadow mb-4">
        <div class="card-header py-3 font-weight-bold">
            @if($post->type === 'Post' && $post->status !== 'Draft')
                <h6 class="m-0 font-weight-bold text-primary">Divulgação detalhada </h6>
            @elseif($post->status === 'Draft')
                <h6 class="m-0 font-weight-bold text-primary">Rascunho detalhado </h6>
            @else
                <h6 class="m-0 font-weight-bold text-primary">Evento detalhado </h6>
            @endif
        </div>
        <div class="card-body">
            <div class="row">
                <div class="form-group col-12" style="min-width: 50px">
                    @if($post->type === 'Post')
                        <div class="col-sm"><label class="font-weight-bold">Tipo de
                                Divulgação: </label> {{$post->EventType->name}}</div>
                    @else
                        <div class="col-sm"><label class="font-weight-bold">Tipo de
                                Evento: </label> {{$post->EventType->name}}</div>
                    @endif
                </div>

                <div class="form-group col-12">
                    <div class="col-sm"><label class="font-weight-bold">Titulo: </label> {{$post->title}}</div>
                </div>

                <div class="form-group col-12">
                    @if($post->location === null)

                    @else
                        <div class="col-sm"><label class="font-weight-bold">Local: </label> {{$post->location}}</div>
                    @endif
                </div>

                @if($post->type === 'Post')
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <div class="col-md"><label class="font-weight-bold">Validade: </label> {{$post->validity}}
                        </div>
                    </div>
                @else
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <div class="col-md"><label class="font-weight-bold">Data de
                                Início: </label> {{$post->startDate}}</div>
                    </div>
                    <div class="form-group col-12 col-sm-6 col-md-4">
                        <div class="col-md"><label class="font-weight-bold">Data de Fim: </label> {{$post->endDate}}
                        </div>
                    </div>
                @endif

                <div class="form-group col-12 col-sm-6 col-md-4">
                    @if($post->link === null)

                    @else
                        <div class="col-sm"><label class="font-weight-bold">Link: </label> {{$post->link}} </div>
                    @endif
                </div>

                <div class="form-group col-12 col-sm-6 col-md-4">
                    @if($post->tags === null)

                    @else
                        <div class="col-md"><label class="font-weight-bold">Tags: </label> {{$post->tags}}</div>
                    @endif
                </div>

                <div class="form-group col-12 col-sm-6 col-md-4">
                    <div class="col-md"><label class="font-weight-bold">Importante: </label>
                        @switch($post->important)
                            @case(0)
                            Não
                            @break
                            @case(1)
                            Sim
                            @break
                            @default
                            Não
                        @endswitch
                    </div>
                </div>
                <br> <br>
                <div class="form-group col-md-12">
                    <div class="col-sm"><label class="font-weight-bold">Texto Curto: </label> {{$post->resume}}</div>
                </div>
                <div class="form-group col-md-12">
                    <div class="col-md"><label class="font-weight-bold">Descrição: </label>{!! $post->description !!}
                    </div>
                </div>
                <br>
                @if ($post->image)
                    <div class="form-group col-md-6">
                        <img alt="Imagem da divulgação" style="height: 40%; width: auto"
                             src="{{Storage::disk('public')->url('posts_images/').$post->image}}">
                        <br>
                    </div>
                @endif
                @if ($post->poster)
                    <div class="form-group col-md-6">
                        <img alt="Poster da divulgação" style="height: 40%; width: auto"
                             src="{{Storage::disk('public')->url('posts_posters/').$post->poster}}">
                    </div>
                @endif

            </div>
        </div>
    </div>

@endsection
