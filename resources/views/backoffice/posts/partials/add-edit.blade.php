<div class="container-fluid">
    <div class="row">
        <div class="form-group col-12" style="min-width: 50px">
            @if(Auth::user()->isAdmin())
                <div class="form-group">
                    <label for="eventtype_service">Gabinete</label>
                    <select class="form-control" id="eventtype_service" name="service_id">
                        @foreach ($services as $service)
                            <option value="{{$service->id}}"
                                    @if (old('service_id',$post->service_id)==$service->id) selected @endif
                            >{{$service->name}}</option>
                        @endforeach
                    </select>
                </div>
            @else
                <input type="hidden" id="eventtype_service" name="service_id"
                       value="{{ Auth::user()->service_id }}">
            @endif
        </div>

        @if($menuPostType === 'Post')
            <div class="form-group col-12 col-sm-6 col-md-4" style="min-width: 50px">
                <label for="tipoDivulgacao"><span class="campo_obrigatorio text-danger small">&#10033;</span>Tipo de Divulgação</label>
                <select class="custom-select" name="eventType" id="tipoDivulgacao" required>
                    <option value="0" hidden selected>Escolha o Tipo de Divulgação</option>
                    @foreach($eventTypes as $type)
                        <option value="{{$type->id}}"
                                @if (old('eventType',$post->eventtype_id)==$type->id) selected @endif>
                            @if(Auth::user()->isAdmin()) {{$type->service->initial}} - @endif {{$type->name}}
                        </option>
                    @endforeach
                </select>
            </div>
        @else
            <div class="form-group col-12 col-sm-6 col-md-4" style="min-width: 50px">
                <label for="tipoDivulgacao"><span class="campo_obrigatorio text-danger small">&#10033;</span>Tipo de
                    Evento</label>
                <select class="custom-select" name="eventType" id="tipoDivulgacao" required>
                    <option value="0" hidden selected>Escolha o Tipo de Evento</option>
                    @foreach($eventTypes as $type)
                        <option value="{{$type->id}}"
                                @if (old('eventType',$post->eventtype_id)==$type->id) selected @endif>
                            @if(Auth::user()->isAdmin()) {{$type->service->initial}} - @endif {{$type->name}}
                        </option>
                    @endforeach
                </select>
            </div>
        @endif
        <div class="form-group col-12 col-sm-6 col-md-8">
            <label for="inputLink">Link</label>
            <input type="text" class="form-control" name="link" id="inputLink" maxlength="250"
                   value="{{old('link',$post->link)}}">
            <h6 class="pull-right text-right small mt-2" id="count_message2"></h6>
        </div>

        <div class="form-group col-12 col-md-6">
            <label for="inputTitle"><span
                    class="campo_obrigatorio text-danger small">&#10033;</span>Título</label>
            <input type="text" class="form-control" name="title" id="inputTitle" maxlength="150"
                   value="{{old('title',$post->title)}}"/>
            <h6 class="pull-right text-right small mt-2" id="count_message1"></h6>
        </div>

        <div class="form-group col-12 col-md-6">
            <label for="inputLocation">Local</label>
            <input type="text" class="form-control" name="location" id="inputLocation"
                   maxlength="150" value="{{old('location',$post->location)}}">
            <h6 class="pull-right text-right small mt-2" id="count_message4"></h6>
        </div>

        <div class="form-group col-md-12">
            <span class="campo_obrigatorio text-danger small">&#10033;</span><label
                for="inputResume">Texto Curto</label>
            <textarea class="form-control" name="resume" id="inputResume"
                      placeholder="Insira aqui um pequeno texto acerca desta Divulgação..."
                      rows="2" style="resize: none" maxlength="150"
                      fixed>{{old('resume',$post->resume)}}</textarea>
            <h6 class="pull-right text-right small mt-2" id="count_message3"></h6>
        </div>


        @if($menuPostType === 'Post')
            <div class="form-group col-12 col-sm-4 col-md-5" id="inputTags_div">
                <label for="inputTags">Tags</label>
                <input type="text" class="form-control" name="tags" id="inputTags"
                       value="{{old('tags',$post->tags)}}">
            </div>
            <div class="form-group col-12 col-sm-4 col-md-4">
                <label for="inputValidity">Validade</label>
                <!--Poderá prologar a validade -->
                <input type="datetime-local" class="form-control" name="validity" id="inputValidity"
                       value="{{old('validity', date('Y-m-d\TH:i', strtotime((isset($post->validity)?$post->validity: date("Y-m-d")))))}}"/>
                <br>
            </div>
            <div class="form-group col-12 col-sm-4 col-md-3">
                <label for="inputImportant">Importante</label> {{-- base de dados--}}
                <select class="custom-select form-control" name="important" id="inputImportant">
                    <option value="0" selected>Não</option>
                    <option value="1" >Sim</option>
                </select>
            </div>
        @else
            <div class="form-group col-12 col-sm-5 col-md-4">
                <label for="inputTags">Tags</label>
                <input type="text" class="form-control" name="tags" id="inputTags"
                       value="{{old('tags',$post->tags)}}">
            </div>
            <div class="form-group col-12 col-sm-3 col-md-2">
                <label for="inputImportant">Importante</label> {{-- base de dados--}}
                <select class="custom-select form-control" name="important" id="inputImportant">
                    <option value="0" selected>Não</option>
                    <option value="1" >Sim</option>
                </select>
            </div>
            <div class="form-group col-12 col-sm-3 col-md-3">
                <label for="inputStartDate">Data de Início do Evento</label>
                <input type="datetime-local" class="date form-control" name="startDate" id="inputStartDate"
                       value="{{old('startDate', date('Y-m-d\TH:i', strtotime((isset($post->startDate)?$post->startDate: date("Y-m-d")))))}}"/>
                <br>
            </div>
            <div class="form-group col-12 col-sm-3 col-md-3">
                <label for="inputEndDate">Data de Fim do Evento</label>
                <input type="datetime-local" class="form-control" name="endDate" id="inputEndDate"
                       value="{{old('endDate', date('Y-m-d\TH:i', strtotime((isset($post->startDate)?$post->startDate: date("Y-m-d")))))}}"/>
                <br>
            </div>
        @endif

        <div class="form-group col-md-6">
            <label for="inputImage">Imagem</label>
            <input type="file" class="form-control-file" name="image" id="inputImage"
                   aria-describedby="fileHelp"/>
            <small id="fileHelp" class="form-text text-muted"> O tamanho da Imagem não deve ultrapassar os 2MB </small>
        </div>

        <div class="form-group col-md-6">
            <label for="inputPoster">Cartaz</label>
            <input type="file" class="form-control-file" name="poster" id="inputPoster"
                   aria-describedby="filePosterHelp"/>
            <small id="filePosterHelp" class="form-text text-muted">O tamanho do Cartaz não deve ultrapassar os
                2MB </small>
        </div>

        <div class="form-group col-md-12">
            <br>
            <span class="campo_obrigatorio text-danger small">&#10033;</span><label
                for="inputDescription">Descrição</label>
            <textarea name="description" id="inputDescription"
                      required>{{old('description',$post->description)}}</textarea>
            <br>
            <small class="campo_obrigatorio text-danger">&#10033; Campo de preenchimento obrigatório</small>
        </div>

    </div>
</div>
