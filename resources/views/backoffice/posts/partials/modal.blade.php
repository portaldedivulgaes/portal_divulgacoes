{{--
<!-- Modal para eliminar publicação -->
@if (count($posts))
@foreach($posts as $post)
    @if($post->type === 'Post' && $post->status !== 'Draft')
        <div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Eliminar Divulgação</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Tem a certeza que pretende eliminar a divulgação "<span class="font-weight-bold" id="titulo"></span>" ?</p>
                    </div>
                    <div class="modal-footer">
                        <button id="eliminarDivulgacao" type="button" class="btn btn-danger btn-sm mr-2">Eliminar
                        </button>
                        <button type="button" class="btn btn-secondary btn-sm mr-2" data-dismiss="modal">Cancelar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    @elseif($post->status === 'Draft')
        <div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Eliminar Rascunho</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Tem a certeza que pretende eliminar o rascunho "<span class="font-weight-bold" id="titulo"></span>" ?</p>
                    </div>
                    <div class="modal-footer">
                        <button id="eliminarDivulgacao" type="button" class="btn btn-danger btn-sm mr-2">Eliminar
                        </button>
                        <button type="button" class="btn btn-secondary btn-sm mr-2" data-dismiss="modal">Cancelar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Eliminar Evento</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Tem a certeza que pretende eliminar a evento "<span class="font-weight-bold" id="titulo"></span>" ?</p>
                    </div>
                    <div class="modal-footer">
                        <button id="eliminarDivulgacao" type="button" class="btn btn-danger btn-sm mr-2">Eliminar
                        </button>
                        <button type="button" class="btn btn-secondary btn-sm mr-2" data-dismiss="modal">Cancelar
                        </button>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endforeach
    @endif
--}}

<!-- Modal para eliminar publicação -->
@if (count($posts))
    @foreach($posts as $post)
            <div class="modal fade" id="modalEliminar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
                 aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Eliminar</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <p>Tem a certeza que pretende eliminar "<span class="font-weight-bold" id="titulo"></span>" ?</p>
                        </div>
                        <div class="modal-footer">
                            <button id="eliminarDivulgacao" type="button" class="btn btn-danger btn-sm mr-2">Eliminar
                            </button>
                            <button type="button" class="btn btn-secondary btn-sm mr-2" data-dismiss="modal">Cancelar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
    @endforeach
@endif
