@extends('layout.masterbackoffice')

@section('styleLinks')
    <link href="{{asset('css/dataTables.bootstrap4.min.css')}}" rel="stylesheet">
@endsection

@section('title', 'Divulgações')

@section('content')
    @include('backoffice.posts.partials.modal')

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        <h1 class="h3 mb-0 text-gray-800">Divulgações</h1>
        <a href="{{route('posts.create','Post')}}" class="btn btn-sm btn-primary shadow-sm"><i
                class="fas fa-plus text-white-50 mr-2"></i>Adicionar Divulgação</a>
    </div>

    <!-- Content Row -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Lista de Divulgações</h6>
        </div>
        <div class="card-body">
            @if (count($posts))
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                        <tr>
                            <th>Título</th>
                            <th>Data de Validade</th>
                            <th>Estado</th>
                            @if(Auth::user()->isAdmin())<th>Gabinete</th>@endif
                            <th style="width: 50px">Opções</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($posts as $post)
                            <tr>
                                <td>{{$post->title}}</td>
                                <td>{{\Carbon\Carbon::parse($post->validity)->format('d/m/Y') }}</td>
                                <td>@switch($post->status)
                                        @case('Published')
                                        Publicado
                                        @break
                                        @case('Draft')
                                        Rascunho
                                        @break
                                        @case('Archived')
                                        Arquivado
                                        @break
                                        @default
                                        Publicado
                                    @endswitch
                                </td>
                                @if(Auth::user()->isAdmin())<td>{{$post->service->initial}}</td>@endif
                                <td nowrap>
                                    <a class="btn btn-sm btn-outline-primary" href="{{route('posts.show',$post)}}"
                                       title='Visualizar'><i class="fas fa-eye"></i></a>

                                    <a class="btn btn-sm btn-outline-warning" href="{{route('posts.edit',$post)}}"
                                       title='Editar'><i class="fas fa-pen"></i></a>

                                    <form method="POST" action="{{route('posts.destroy',$post)}}" role="form"
                                          class="d-inline-block form_post_id" title='Eliminar'>
                                        @csrf
                                        @method("DELETE")

                                        <button type="button" class="btn btn-sm btn-outline-danger" data-toggle="modal"
                                                data-target="#modalEliminar" data-title="{{$post->title}}"><i
                                                class="fas fa-trash-alt"></i></button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            @else
                <h6 style="text-align: center">Sem Divulgações Disponíveis</h6>
            @endif
        </div>
    </div>
@endsection

@section("scripts")
    <script>
        $('#dataTable').dataTable({
            "order": [[1, 'desc']],
            'language': {'url': '//cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese.json'}
        });

        $(document).ready(function () {
            $('#modalEliminar').on('show.bs.modal', function (e) {
                $("#eliminarDivulgacao").click(function () {
                    $(e.relatedTarget).parent().submit();
                });
                $("#titulo").text($(e.relatedTarget).data("title"));
            });
        });

        $('#modalEliminar').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget);
            var recipient = button.data('whatever');
            var modal = $(this);
            modal.find('.modal-body input').val(recipient);
        });
    </script>
@endsection
