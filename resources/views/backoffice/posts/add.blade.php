@extends('layout.masterbackoffice')

@section('styleLinks')
    <link href="{{asset('css/style_add_post.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/bootstrap-tokenfield.min.css">
@endsection

@section('title', 'Adicionar '.($menuPostType=='Post'?'Divulgação':'Evento').' | Divulgações')

@section('content')

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        @if($menuPostType === 'Post')
            <h1 class="h3 mb-0 text-gray-800">Adicionar Divulgação</h1>
            <a href="{{route('posts.index')}}" class="btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-chevron-left text-white-50 mr-2"></i>Voltar à listagem</a>
        @else
            <h1 class="h3 mb-0 text-gray-800">Adicionar Evento</h1>
            <a href="{{route('events.index')}}" class="btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-chevron-left text-white-50 mr-2"></i>Voltar à listagem</a>
        @endif
    </div>

    <!-- Content Row -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            @if($menuPostType === 'Post')
                <h6 class="m-0 font-weight-bold text-primary">Nova Divulgação</h6>
            @else
                <h6 class="m-0 font-weight-bold text-primary">Novo Evento</h6>
            @endif
        </div>
        <div class="card-body">
            <form method="POST" action="{{route('posts.store')}}" class="form-group" enctype="multipart/form-data">
                @csrf
                @include('backoffice.posts.partials.add-edit')
                <input type="hidden" id="type" name="type"
                       value="{{ $menuPostType }}">
                <div class="text-right">
                    <div class="form-group col-md-12 text-right mt-2">
                        <button type="submit" class="btn btn-sm btn-info text-white mr-1" name="draft"
                                title='Guardar nos Rascunhos'><i class="fas fa-archive mr-2"></i>Guardar nos Rascunhos
                        </button>
                        <button type="submit" class="btn btn-sm btn-primary text-white mr-1" name="ok"
                                title='Publicar divulgação'><i class="fas fa-share-alt mr-2"></i>Publicar
                        </button>
                        <a href="{{route('posts.index')}}" class="btn btn-sm btn-secondary text-white mr-1 my-1"
                           name="btn_cancelar" id="btn_cancelar" title='Cancelar divulgação'>Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Contagem de carateres -->
    <script src="{{asset('/js/script_count_characters.js')}}"></script>

    <!--CKeditor - Ficheiros do CKeditor-->
    <script src="/vendor/ckeditor/ckeditor.js"></script>
    <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>

    <script> CKEDITOR.replace('inputDescription');</script>

    <script>
        $(function () {
            $('input[type=text], textarea').keyup();
        });
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.js"></script>

    <script>
        $(document).ready(function(){
            $('#inputTags').tokenfield({
                autocomplete:{
                    delay:100
                },
                showAutocompleteOnFocus: true
            });
            $('#inputTags_div').on('submit', function(event){
                event.preventDefault();
                if($.trim($('#inputTags').val()).length == 0)
                {
                    alert("Please Enter Atleast one Skill");
                    return false;
                }
            });
        });
    </script>
@endsection
