@extends('layout.masterbackoffice')

@section('styleLinks')
    <link href="{{asset('css/style_add_post.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/css/bootstrap-tokenfield.min.css">
@endsection

@section('title', 'Editar '.($menuPostType=='Post'?'Divulgação':$menuPostType=='Evdent'?'Evento':'Rascunho').' | Divulgações')

@section('content')
    <div class="d-sm-flex align-items-center justify-content-between mb-4">
        @if($post->type === 'Post' && $post->status !== 'Draft')
            <h1 class="h3 mb-0 text-gray-800">Editar Divulgação</h1>
            <a href="{{route('posts.index')}}" class="btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-chevron-left text-white-50 mr-2"></i>Voltar à listagem</a>
        @elseif($post->status === 'Draft')
            <h1 class="h3 mb-0 text-gray-800">Editar Rascunho | <small style="font-size: 13pt"> Não Publicado </small></h1>
            <a href="{{route('drafts.index')}}" class="btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-chevron-left text-white-50 mr-2"></i>Voltar à listagem</a>
        @else
            <h1 class="h3 mb-0 text-gray-800">Editar Evento</h1>
            <a href="{{route('events.index')}}" class="btn btn-sm btn-primary shadow-sm"><i
                    class="fas fa-chevron-left text-white-50 mr-2"></i>Voltar à listagem</a>
        @endif
    </div>

    <div class="card shadow mb-4">
        @if($post->type === 'Post' && $post->status !== 'Draft')
            <div class="card-header py-3 font-weight-bold text-primary">Editar Divulgação</div>
        @elseif($post->status === 'Draft')
            <div class="card-header py-3 font-weight-bold text-primary">Editar Rascunho</div>
        @else
            <div class="card-header py-3 font-weight-bold text-primary">Editar Evento</div>
        @endif
        <div class="card-body">
            <form method="POST" action="{{route('posts.update',$post)}}" class="form-group"
                  enctype="multipart/form-data">
                @csrf
                @method("PUT")
                @include('backoffice.posts.partials.add-edit')
                <div class="text-right">
                    @if($post->status === 'Draft')
                        <div class="form-group col-md-12 text-right mt-2">
                            <button type="submit" class="btn btn-sm btn-success text-white mr-1" name="#"><i
                                    class="far fa-save mr-2"></i> Guardar
                            </button>
                            <button type="submit" class="btn btn-sm btn-primary text-white mr-1" name="ok"
                                    title='Publicar'><i class="fas fa-share-alt mr-2"></i>Publicar
                            </button>
                            <a href="{{route('posts.index')}}" class="btn btn-sm btn-secondary text-white mr-1 my-1">
                                Cancelar</a>
                        </div>
                    @else
                        <div class="form-group col-md-12 text-right mt-2">
                            <button type="submit" class="btn btn-sm btn-warning text-white mr-1" name="reverseDraft"
                                    title='Reverter para Rascunhos'><i class="fas fa-archive mr-2"></i>Reverter para
                                Rascunhos
                            </button>
                            <button type="submit" class="btn btn-sm btn-success text-white mr-1" name="ok"><i
                                    class="far fa-save mr-2"></i> Guardar
                            </button>
                            <a href="{{route('posts.index')}}" class="btn btn-sm btn-secondary text-white mr-1 my-1">
                                Cancelar</a>
                        </div>
                    @endif
                </div>
            </form>
        </div>
    </div>
@endsection

@section('scripts')
    <!-- Contagem de carateres -->
    <script src="{{asset('/js/script_count_characters.js')}}"></script>

    <!--CKeditor - Ficheiros do CKeditor-->
    <script src="/vendor/ckeditor/ckeditor.js"></script>
    <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>

    <script> CKEDITOR.replace('inputDescription');</script>

    <script>
        $(function () {
            $('input[type=text], textarea').keyup();
        });
    </script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-tokenfield/0.12.0/bootstrap-tokenfield.js"></script>

    <script>
        $(document).ready(function(){
            $('#inputTags').tokenfield({
                autocomplete:{
                    delay:100
                },
                showAutocompleteOnFocus: true
            });
            $('#inputTags_div').on('submit', function(event){
                event.preventDefault();
                if($.trim($('#inputTags').val()).length == 0)
                {
                    alert("Please Enter Atleast one Skill");
                    return false;
                }
            });
        });
    </script>
@endsection
