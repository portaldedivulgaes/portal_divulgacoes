@extends('layout.masterbackoffice')




@section('title', 'FAQs')

@section('content')


<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Tópicos de ajuda</h1>

</div>

<!-- Content Row -->
<div class="card shadow mb-4">

    <div class="card-body">


        <div class="accordion" id="accordionExample">


            <!-- FAQ Criar divulgação -->
            <div class="card">
                <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                            <i class="fas fa-angle-down rotate-icon mr-2"></i>Criar uma divulgação
                        </button>
                    </h2>
                </div>
                <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                        <p>A criação de uma nova divulgação deverá ser feita de acordo com os seguintes passos:</p>
                        <p>- Escolher a opção " <i class="fas fa-calendar fa-fw"></i> Publicações " através do menu de navegação lateral</p>
                        <p>- Escolher a opção " Divulgações "</p>

                        <p>- Pressionar o botão “ <i class="fas fa-plus mr-1"></i> Adicionar divulgação”</p>
                        <p>- Preencher os campos obrigatórios, assinalados com asterisco &#10033;</p>
                        <p>- Para publicar a divulgação, clique em " <i class="fas fa-share-alt"></i> Publicar "<br><br></p>
                        <p>Caso pretenda guardar o evento, sem o publicar de imediato clique em " <i class="far fa-save"></i>
                            Guardar em rascunho".<br>A divulgação ficará guardado na secção " <i class="fa fa-file fa-fw"></i>
                            Rascunhos " <br></p>
                        </div>
                    </div>
                </div>

                <!-- FAQ Criar Evento -->
                <div class="card">
                    <div class="card-header" id="headingThree">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                <i class="fas fa-angle-down rotate-icon mr-2"></i>Criar um evento
                            </button>
                        </h2>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                        <div class="card-body">
                            <p>A criação de um novo evento deverá ser feita de acordo com os seguintes passos:</p>
                            <p>- Escolher a opção " <i class="fas fa-calendar fa-fw"></i> Publicações " através do menu de navegação lateral</p>
                            <p>- Escolher a opção " Eventos "</p>

                            <p>- Pressionar o botão “ <i class="fas fa-plus mr-1"></i> Adicionar evento”</p>
                            <p>- Preencher os campos obrigatórios, assinalados com asterisco &#10033;</p>
                            <p>- Para publicar o evento, clique em " <i class="fas fa-share-alt"></i> Publicar "<br><br></p>
                            <p>Caso pretenda guardar o evento, sem o publicar de imediato clique em " <i class="fas fa-archive mr-2"></i>Guardar nos rascunhos".<br>O evento ficará guardado na secção " <i class="fas fa-archive mr-2"></i>Rascunhos " <br></p>
                        </div>
                    </div>
                </div>

                <!-- FAQ Criar Newsletter -->
                <div class="card">
                    <div class="card-header" id="headingTwoo">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwoo" aria-expanded="false" aria-controls="collapseTwoo">
                                <i class="fas fa-angle-down rotate-icon mr-2"></i>Criar uma newsletter
                            </button>
                        </h2>
                    </div>
                    <div id="collapseTwoo" class="collapse" aria-labelledby="headingTwoo" data-parent="#accordionExample">
                        <div class="card-body">
                            <p>A criação de uma nova divulgação deverá ser feita de acordo com os seguintes passos:</p>
                            <p>- Aceder à página " <i class="fa fa-envelope"></i> Newsletters " através do menu de navegação lateral</p>
                            <p>- Escreva o assunto da Newsletter</p>
                            <p>- Insira/escolha os emails que identificam o publico alvo </p>
                            <p>- No botão " <i class="fas fa-plus mr-1"></i>Selecionar Divulgações ", selecione as publicações que pretende incluir na newsletter </p>
                            <p>- Clique em " <i class="fas fa-search mr-1"></i>Pré-visualizar " e confirme todas as informações
                                <p>- Para enviar a newsletter, clique em " <i class="fas fa-paper-plane"></i> Enviar ", no canto superior direito<br><br></p>
                            </div>
                        </div>
                    </div>



                <!-- Tipos de Publicação -->
                <div class="card">
                    <div class="card-header" id="headingPubTypes">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapsePubType" aria-expanded="false" aria-controls="collapsePubType">
                                <i class="fas fa-angle-down rotate-icon mr-2"></i>Gerir lista de tipos de publicações
                            </button>
                        </h2>
                    </div>
                    <div id="collapsePubType" class="collapse" aria-labelledby="headingPubType" data-parent="#accordionExample">
                        <div class="card-body">
                            <p>Para adicionar/editar ou remover tipos de publicações: </p>
                            <p>- Aceder à página " <i class="fas fa-clone fa-fw"></i> Tipo de Divulgação "</p>
                            <p>- Para criar um novo tipo de divulgação, clique no botão que está no canto superior direito " <i class="fas fa-plus mr-2"></i>Adicionar Tipo de Publicação ", <br>
                                escreva a designação pretendida, selecione um icon representativo e clique em " Guardar " </p>
                            <p>-Para editar ou eliminar tipos de publicações, encontre na lista o item pretendido, e no lado direito encontrará as opções correspondentes</p>
                            </div>
                        </div>
                    </div>


                <!-- Público Alvo -->
                <div class="card">
                    <div class="card-header" id="headingTargets">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTargets" aria-expanded="false" aria-controls="collapseTargets">
                                <i class="fas fa-angle-down rotate-icon mr-2"></i>Gerir lista de Público Alvo
                            </button>
                        </h2>
                    </div>
                    <div id="collapseTargets" class="collapse" aria-labelledby="headingTargets" data-parent="#accordionExample">
                        <div class="card-body">
                            <p>Para adicionar/editar a lista de Público Alvo: </p>
                            <p>- Aceder à página " <i class="fas fa-users-cog fa-fw"></i> Público Alvo"</p>
                            <p>- Para criar um novo tipo de Público Alvo, clique no botão que está no canto superior direito " <i class="fas fa-plus mr-2"></i>Adicionar Público Alvo ", <br>
                                escreva a designação pretendida, insira um e-mail válido e clique em " Guardar " </p>
                            <p>-Para editar ou eliminar itens da lista de Público Alvo, encontre na lista o item pretendido, e no lado direito encontrará as opções correspondentes</p>

                            </div>
                        </div>
                    </div>








                    <!-- FAQ Consultar registo de actividade -->
                    <div class="card">
                        <div class="card-header" id="headingActivity">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseActivity" aria-expanded="false" aria-controls="collapseActivity">
                                    <i class="fas fa-angle-down rotate-icon mr-2"></i>Consultar registo de actividade
                                </button>
                            </h2>
                        </div>
                        <div id="collapseActivity" class="collapse" aria-labelledby="headingActivity" data-parent="#accordionExample">
                            <div class="card-body">
                                <p>Para consultar o registo de actividade siga os seguintes passos:</p>
                                <p>- No canto superior direito clique no seu nome/imagem de utilizador</p>
                                <p>- No menu, escolha " <i class="fas fa-cogs fa-sm fa-fw mr-2"></i>Definições " </p>
                                <p>- Clique sobre o separador " Histórico de actividade "</p>
                            </div>
                        </div>
                    </div>



                    <!-- FAQ Mudar informações de conta -->
                    <div class="card">
                        <div class="card-header" id="headingAccount">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseAccount" aria-expanded="false" aria-controls="collapseAccount">
                                    <i class="fas fa-angle-down rotate-icon mr-2"></i>Modificar a minha imagem de apresentação
                                </button>
                            </h2>
                        </div>
                        <div id="collapseAccount" class="collapse" aria-labelledby="headingAccount" data-parent="#accordionExample">
                            <div class="card-body">
                                <p>Para aceder às informações do seu perfil siga os seguintes passos:</p>
                                <p>- No canto superior direito clique no seu nome/imagem de utilizador</p>
                                <p>- No menu, escolha " <i class="fas fa-cogs fa-sm fa-fw mr-2"></i>Definições " </p>
                                <p>- No separador "Opções", clique sobre a opção " <i class="fas fa-user mr-2"></i>Editar informações do perfil "</p>
                                <p><small>Nota: apenas um administrador pode alerar o seu gabinete / perfil de utilizador</small></p>
                                <p>- Para mudar a sua imagem de apresentação clique no botão "Procurar" e escolha a imagem pretendida (a imagem deverá ter menos de 2mb)</p>
                                <p>- Para guardar a alteração, clique em " <i class="far fa-save mr-2"></i>Guardar Dados " </p>

                            </div>
                        </div>
                    </div>


                    <!-- FAQ Mudar informações de GABINETES -->
                    <div class="card">
                        <div class="card-header" id="headingGab">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseGab" aria-expanded="false" aria-controls="collapseGab">
                                    <i class="fas fa-angle-down rotate-icon mr-2"></i>Mudar as informações de gabinete
                                </button>
                            </h2>
                        </div>
                        <div id="collapseGab" class="collapse" aria-labelledby="headingGab" data-parent="#accordionExample">
                            <div class="card-body">
                                <p>Para aceder às informações do seu perfil siga os seguintes passos:</p>
                                <p>- No canto superior direito clique no seu nome/imagem de utilizador</p>
                                <p>- No menu, escolha " <i class="fas fa-cogs fa-sm fa-fw mr-2"></i>Definições " </p>
                                <p>- No separador "Opções", clique sobre a opção " <i class="fas fa-cog mr-1"></i>Editar informações do gabinete "</p>
                                <p>- Modifique / insira as informações obrigatórias (assinaladas com um asterisco &#10033; )</p>
                                <p><small>Clique em " <i class="fas fa-search mr-2"></i>Prever " para visualizar o aspeto final que estará disponivel no website</small>
                                    <p>- Para guardar a alteração, clique em " <i class="far fa-save mr-2"></i>Guardar Dados " </p>

                                </div>
                            </div>
                        </div>

                    <!-- Criar uma tarefa -->
                    <div class="card">
                        <div class="card-header" id="headingTask">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTask" aria-expanded="false" aria-controls="collapseTask">
                                    <i class="fas fa-angle-down rotate-icon mr-2"></i>Adicionar / Remover uma tarefa
                                </button>
                            </h2>
                        </div>
                        <div id="collapseTask" class="collapse" aria-labelledby="headingTask" data-parent="#accordionExample">
                            <div class="card-body">
                                <p>Para adicionar uma tarefa prossiga do seguinte modo:</p>
                                <p>- Aceder à página "Tarefas" através do menu de navegação lateral</p>
                                <p>- Clicar no botão " <i class="fas fa-plus mr-1"></i> Adicionar Tarefa " no canto superior direito da página</p>
                                <p>- Preencher o campo designado por “Título da Tarefa” com o nome que pretende e de seguida clicar em “Adicionar”</p>
                                <p>- Aceder à página "Tarefas" através do menu de navegação lateral</p>
                                <hr>
                                <p>Caso pretenda apagar uma tarefa proceda do seguinte modo:</p>
                                <p>- Clicar no botão com "<i class="fas fa-ellipsis-v fa-sm fa-fw"></i>" que se encontra no canto inferior esquerdo da checklist referente a tarefa</p>
                                <p>- Clicar na opção “Eliminar Tarefa” e de seguida clicar no botão “Eliminar”</p>


                                </div>
                            </div>
                        </div>



                    <!-- Criar uma nota pessoal -->
                    <div class="card">
                        <div class="card-header" id="headingNote">
                            <h2 class="mb-0">
                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseNote" aria-expanded="false" aria-controls="collapseNote">
                                    <i class="fas fa-angle-down rotate-icon mr-2"></i>Adicionar / Remover / Editar um Nota
                                </button>
                            </h2>
                        </div>
                        <div id="collapseNote" class="collapse" aria-labelledby="headingNote" data-parent="#accordionExample">
                            <div class="card-body">
                                <p>Para adicionar uma nota basta proceder do modo abaixo explicado:</p>
                                <p>- Aceder a “Dashboard” (Página Inicial do BackOffice)</p>
                                <p>- No cartão “Notas”, preencher o campo que têm como título “Adicione uma nota”</p>
                                <p>- Clicar no botão “Adicionar” para registar a sua nota.</p>
                                <hr>
                                <p>Se pretender editar a nota inserida anteriormente deve proceder do seguinte modo:</p>
                                <p>- Clicar sobre o título da nota</p>
                                <p>- Inserir um novo título</p>
                                <p>- Clicar no botão “Alterar”</p>
                                <hr>
                                <p>Caso queira eliminar uma nota, deve:</p>
                                <p>- Selecione a nota pretendida</p>
                                <p>- Clicar no botão " <i class="fas fa-times"></i> " ou no botão do lado direito " <i class="fas fa-trash-alt"></i> " </p>
                                </div>
                            </div>
                        </div>





                        @if (Auth::user()->role=="Admin" )
                        <!-- FAQ Criar Utilizador -->
                        <div class="card">
                            <div class="card-header" id="AdminOne">
                                <h2 class="mb-0">
                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseAdminOne" aria-expanded="false" aria-controls="collapseAdminOne">
                                        <i class="fas fa-angle-down rotate-icon mr-2"></i>Adicionar utilizadores
                                    </button>
                                </h2>
                            </div>
                            <div id="collapseAdminOne" class="collapse" aria-labelledby="AdminOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    <p>A adição de um novo utilizador deverá ser feita de acordo com os seguintes passos:</p>
                                    <p>- Aceder à página " <i class="fas fa-users"></i> Utilizadores " através do menu de navegação lateral</p>
                                    <p>- Pressionar o botão “ <i class="fas fa-plus mr-1"></i> Adicionar utilizador”</p>
                                    <p>- Preencher o campo "Identificação do utilizador" com uma identificação válida forncecida pelos serviços
                                        do IPL</p>
                                        <p>- Selecionar o perfil de utilizador ao utilizador que está a ser criado</p>
                                        <p>- Selecionar o gabinete que deseja atribuir ao utilizador<br><small><strong>Nota:</strong> caso o
                                            utilizador a ser criado seja administrador, o atributo "gabinete" não se aplica</small></p>
                                            <p>- Clique em no botão " <i class="fas fa-plus mr-1"></i> Adicionar " se pretender guardar as alterações e voltar à área de gestão de
                                                utilizadores<br><small><strong>Nota:</strong> Será notificado após a operação ter sido concluida com sucesso</small></p>
                                            </div>
                                        </div>
                                    </div>





                                    <!-- FAQ Remover Utilizador -->
                                    <div class="card">
                                        <div class="card-header" id="AdmingTwo">
                                            <h2 class="mb-0">
                                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseAdmingTwo" aria-expanded="false" aria-controls="collapseAdmingTwo">
                                                    <i class="fas fa-angle-down rotate-icon mr-2"></i>Remover utilizadores
                                                </button>
                                            </h2>
                                        </div>
                                        <div id="collapseAdmingTwo" class="collapse" aria-labelledby="AdmingTwo" data-parent="#accordionExample">
                                            <div class="card-body">
                                                <p>Para remover um utilizador, deverá proceder-se do seguinte modo:</p>
                                                <p>- Aceder à página " <i class="fas fa-users"></i> Utilizadores " através do menu de navegação lateral</p>
                                                <p>- Na lista que é apresentada, localizar o utilizador que pretende eliminar</p>
                                                <p>- Na coluna do lado direito correspondente ao utilizador pretendido, clicar no botão com o icon <i class="fas fa-trash-alt ml-2"></i> (remover utilizador) </p>
                                                <p>- Será mostrada uma mensagem de confirmação, verifique que a identidade do utilizador é a pretendida</p>
                                                <p>- Clique no botão "Sim, eliminar utilizador"</p>
                                                <p><small><strong>Nota:</strong> Será notificado após a operação ter sido concluida com sucesso</small></p>
                                            </div>
                                        </div>
                                    </div>



                                    <!-- FAQ Recupar Utilizador -->
                                    <div class="card">
                                        <div class="card-header" id="Recoveruser">
                                            <h2 class="mb-0">
                                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseRecoveruser" aria-expanded="false" aria-controls="collapseRecoveruser">
                                                    <i class="fas fa-angle-down rotate-icon mr-2"></i>Recuperar utilizadores
                                                </button>
                                            </h2>
                                        </div>
                                        <div id="collapseRecoveruser" class="collapse" aria-labelledby="Recoveruser" data-parent="#accordionExample">
                                            <div class="card-body">
                                                <p>Para recoperar um utilizadores eliminados, deverá proceder-se do seguinte modo:</p>
                                                <p>- Aceder à página " <i class="fas fa-users"></i> Utilizadores " através do menu de navegação lateral</p>
                                                <p>- Clicar no botão " <i class="fas fa-trash-restore mr-2"></i> Recuperar utilizador ", localizado no canto superior direito</p>
                                                <p>- Na lista que é apresentada, localizar o utilizador que pretende recuperar</p>
                                                <p>- Na coluna do lado direito correspondente ao utilizador pretendido, clicar no botão com o icon " <i class="fas fa-undo"></i> "</p>
                                                <p>Será mostrada uma mensagem de confirmação, e o utilizador estará novamente na lista de utilizadores com autorização de utilização da plataforma</p>
                                            </div>
                                        </div>
                                    </div>





                                    <!-- FAQ Editar Utilizador -->
                                    <div class="card">
                                        <div class="card-header" id="AdminThree">
                                            <h2 class="mb-0">
                                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseAdminThree" aria-expanded="false" aria-controls="collapseAdminThree">
                                                    <i class="fas fa-angle-down rotate-icon mr-2"></i>Alterar dados de utilizadores já existentes
                                                </button>
                                            </h2>
                                        </div>
                                        <div id="collapseAdminThree" class="collapse" aria-labelledby="AdminThree" data-parent="#accordionExample">
                                            <div class="card-body">
                                                <p>A edição dos dados de um utilizador poderá ser efectuada do seguinte modo:</p>
                                                <p>- Aceder à página " <i class="fas fa-users"></i> Utilizadores " através do menu de navegação lateral</p>
                                                <p>- Na lista que é apresentada, localizar o utilizador que pretende editar</p>
                                                <p>- Na coluna do lado direito correspondente ao utilizador pretendido, clicar no botão com o icon <i class='fas fa-pen'></i> (editar dados do utilizador) </p>
                                                <p>- Editar os campos que pretende actualizar</p>
                                                <p>- Clique no botão " <i class="far fa-save mr-1"></i> Guardar " para guardar as alterações e voltar à área de gestão de utilizadores<br>
                                                    <small><strong>Nota:</strong> Será notificado após a operação ter sido concluida com sucesso</small></p>
                                                </div>
                                            </div>
                                        </div>




                                        <!-- FAQ Alterar / remover administrador -->
                                        <div class="card">
                                            <div class="card-header" id="AdminFour">
                                                <h2 class="mb-0">
                                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseAdminFour" aria-expanded="false" aria-controls="collapseAdminFour">
                                                        <i class="fas fa-angle-down rotate-icon mr-2"></i>Alterar / remover dados de administrador
                                                    </button>
                                                </h2>
                                            </div>
                                            <div id="collapseAdminFour" class="collapse" aria-labelledby="AdminFour" data-parent="#accordionExample">
                                                <div class="card-body">
                                                    <p>Um utilizador com perfil de administrador só poderá ter os seus dados alterados ou removidos por outro utilizador com perfil de administrador. </p>
                                                    <p><small><strong>Nota:</strong> É obrigatório estar registado na base de dados pelo menos um utilizador com perfil de administrador</small></p>
                                                </div>
                                            </div>
                                        </div>





                                        <!-- FAQ Backup Base de Dados -->
                                        {{--                                             <div class="card">
                                            <div class="card-header" id="AdminFive">
                                                <h2 class="mb-0">
                                                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseAdminFive" aria-expanded="false" aria-controls="collapseAdminFive">
                                                        <i class="fas fa-angle-down rotate-icon mr-2"></i>Backup da base de dados atual
                                                    </button>
                                                </h2>
                                            </div>
                                            <div id="collapseAdminFive" class="collapse" aria-labelledby="AdminFive" data-parent="#accordionExample">
                                                <div class="card-body">
                                                    <p>O backup da base de dados atual poderá ser efectuada do seguinte modo:</p>
                                                    <p>- Na barra de navegação superior, clique no botão de identificação do utilizador atual</p>
                                                    <p>- No menu que é mostrado, clique sobre a opção " <i class="fas fa-cog"></i> Configuraçoes " </p>
                                                    <p>- Escolha a opção " <i class="fas fa-download "></i> Iniciar Backup da Base de Dados "</p>
                                                    <p>- Aguarde até que seja mostrada uma notificação de que a operação foi concluida com sucesso</p>
                                                    <p>- O botão do lado direito " <i class="fas fa-download"></i> Download do ficheiro de backup da base de dados " permite que guarde uma cópia da base de dados em armazenamento local<br>
                                                        <small><strong>Nota:</strong> O ficheiro de backup tambem será mantido no servidor na pasta "back-office / backups"</small>
                                                    </div>
                                                </div>
                                            </div> --}}




                                            <!-- FAQ RESET à base de dados -->
                                            <div class="card">
                                                <div class="card-header" id="AdminSix">
                                                    <h2 class="mb-0">
                                                        <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseAdminSix" aria-expanded="false" aria-controls="collapseAdminSix">
                                                            <i class="fas fa-angle-down rotate-icon mr-2"></i>Repor a base de dados com os valores de origem
                                                        </button>
                                                    </h2>
                                                </div>
                                                <div id="collapseAdminSix" class="collapse" aria-labelledby="AdminSix" data-parent="#accordionExample">
                                                    <div class="card-body">
                                                        <p>Para repor a base de dados com os valores de origem proceda do seguinte modo:</p>
                                                        <p>- Na barra de navegação superior, clique no botão de identificação do utilizador atual</p>
                                                        <p>- No menu que é mostrado, clique sobre a opção " <i class="fas fa-cog"></i> Configurações " </p>
                                                        <p>- Escolha a opção " <i class="fas fa-recycle "></i> Repor Base de Dados com os valores iniciais "</p>
                                                        <p>- Será mostrada uma mensagem de confirmação. Verifique que pretende realmente repor os valores de origem.<br><strong>Nota: Esta é uma ação destrutiva e irreversivel</strong></p>
                                                        <p>- Aguarde até que seja mostrada uma notificação de que a operação foi concluida com sucesso</p>
                                                    </div>
                                                </div>
                                            </div>

                                            @endif




                                        </div>
                                    </div>










                                </div>



                                @section('scripts')



                                @endsection

                                @endsection
