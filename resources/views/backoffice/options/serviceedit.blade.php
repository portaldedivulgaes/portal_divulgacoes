@extends('layout.masterbackoffice')

@section('title', 'Editar informações de gabinete | Definições')

@section('styleLinks')
<link href="{{asset('css/style_options.css')}}" rel="stylesheet">
@endsection


@section('content')

    <div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Informações do gabinete {{$service->initial}}</h1>
        <a href="javascript:history.go(-1)" class="btn btn-sm btn-primary shadow-sm "><i class="fas fa-chevron-left text-white-50 mr-2"></i>Voltar</a>
    </div>

<!-- Basic Card Example -->
<div class="card shadow main-card">
    <div class="card-body ">

        <div class="row">

            <div class="col pr-3">
                <form method="POST" action="{{route('services.update',$service)}}" class="form-group" id="form_user" enctype="multipart/form-data">
                    @csrf
                    @method("PUT")

                    <div class="row">

                        <div class="col col-md-6">
                            <label for="name"><span class="text-danger"><small>&#10033;</small></span> Nome do gabinete</label>
                            <input type="text" class="form-control" name="name" id="name" value="{{old('name',$service->name)}}" maxlength="120" title="Nome do gabinete" placeholder="Insira o nome do gabinete">
                        </div>
                        <div class="col col-md-6">
                            <label for="initial"><span class="text-danger"><small>&#10033;</small></span> Iniciais</label>
                            <input type="text" class="form-control" style="width:180px;text-transform: uppercase;" name="initial" id="initial" value="{{old('name',$service->initial)}}" maxlength="15" title="Iniciais do gabinete">
                        </div>

                    </div>


                        <br>

                        <label for="description"><span class="text-danger"><small>&#10033;</small></span> Descrição do gabinete</label>
                        <input type="text" class="form-control" name="description" id="description" value="{{old('description',$service->description)}}" maxlength="150" title="Descrição do gabinete" placeholder="Insira a descrição do gabinete">

                        <br>

                        <div class="row">

                            <div class="col">
                                <label for="email"><span class="text-danger"><small>&#10033;</small></span> E-mail</label>
                                <input type="email" class="form-control" name="email" id="email" value="{{old('email',$service->email)}}"  maxlength="250" title="exemplto@servidor.pt" placeholder="Insira o email do gabinete" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$">
                            </div>
                            <div class="col">
                                <label for="phone"><span class="text-danger"></span> Telefone</label>
                                <input type="text" class="form-control" name="phone" id="phone" value="{{old('name',$service->phone)}}" maxlength="15" title="Telefone do gabinete" placeholder="Insira o telefone do gabinete">
                            </div>

                        </div>

                        <br><br>
                        <small><span class="campo_obrigatorio text-danger">&#10033;</span> Campos de preenchimento campo obrigatório</small><br>

                <div class="form-group text-right">
                    <br><br>
                <a href="javascript:history.go(-1)" class="btn btn-sm btn-secondary shadow-sm mr-2"><i class="fas fa-chevron-left text-white-50 mr-2"></i>Cancelar</a>
                <button type="submit" class="btn btn-sm btn-success mr-1" name="ok" id="buttonSubmit"><i class="far fa-save mr-2"></i>Guardar dados</button>
                <a href="#" class="btn btn-sm btn-info shadow-sm mr-2" id="buttonPreview"><i class="fas fa-search text-white-50 mr-2"></i> Prever</a>
                </form>

                </div>

            </div>



            <div id="preview" class="col col-md-5 text-center ml-4 p-4 card bg-light preview">
                <h4>Aparência atual</h4>
                <small>Clique no botão "Prever" para atualizar</small><br>
                <div class="card text-center shadow-sm mx-auto" style="width: 75%">

                    <div class="card-body ">
                        <h5 class="card-title" style="text-transform: uppercase" id="mockupInitial">{{ $service->initial }}</h5>
                        <small class="text-muted" id="mockupName">{{ $service->name }}</small>
                        <p class="card-text mt-3" id="mockupDescription">{{ $service->description }}</p>
                    </div>
                    <div class="card-footer border-light">
                        <a href="#" id="mockupEmail">{{ $service->email }}</a>
                        <br>
                        <div id="mockupPhone">Telefone: {{ $service->phone }}</div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>


@endsection


@section('scripts')

<script src="{{asset('/js/script_edit_service.js')}}"></script>

@endsection
