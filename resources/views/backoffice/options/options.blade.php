@extends('layout.masterbackoffice')




@section('title', 'Definições')


@section('styleLinks')
<link href="{{asset('css/style_options.css')}}" rel="stylesheet">
@endsection

@section('content')


<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Definições</h1>

</div>


 <div class="card shadow mb-4">

    <div class="card-body">
        <div class="row mb-3">

                {{-- Fotografia --}}
                <div class="col col-2 col-photo ">
                    @if (Auth::user()->photo==null)
                    <img src="{{asset('img/default_user.png')}}" id="userphoto" class="img-thumbnail rounded-circle shadow ml-4" alt="Imagem de apresentação" title="Imagem de apresentação" />
                    @else
                    <img class="img-thumbnail shadow text-center ml-4" src="{{Storage::disk('public')->url('users_photos/').Auth::user()->photo}}">
                    @endif
                </div>


                {{-- Dados do utilizador --}}
                <div class="col">
                    <div class="m-3"><strong>Nome:</strong>
                        @if (Auth::user()->name == null)
                        <span class="text-primary">Aguarda primeiro acesso</span>
                        @else
                        {{ Auth::user()->name }}
                        @endif
                    </div>

                    <div class="m-3" ><strong>Email:</strong>
                        @if (Auth::user()->email == null)
                        <span>Aguarda primeiro acesso</span>
                        @else
                        {{ Auth::user()->email }}
                        @endif
                    </div>


                    <div class="m-3"><strong>Perfil:</strong>
                        {{Auth::user()->roleToStr()}}
                    </div>


                    <div class="m-3"><strong>Gabinete:</strong>
                        {{Auth::user()->serviceToStr()}}
                    </div>


                    <div class="m-3" ><strong>Utilizador criado em:</strong>
                        {{Auth::user()->created_at->format("d/m/Y")}}
                    </div>
                </div>

        </div>

        {{-- resumo e actividade --}}

        <div class="row ">

            <div class="col mt-3">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                      <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Opções</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Resumo</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Histórico de atividade</a>
                    </li>
                  </ul>
                  <div class="tab-content p-2" id="myTabContent">

                    {{-- Opções --}}
                    <div class="tab-pane fade show p-3 active" id="home" role="tabpanel" aria-labelledby="home-tab">


                        <p><a class="btn btn-sm btn-outline-warning mr-2" href="{{ route('users.edit', Auth::user()->user) }}" title="Editar informações de perfil"><i class="fas fa-user "></i></a> Editar informações do perfil</p>


                    @if (Auth::user()->role=="Admin" )

                    <br>
                    @foreach($services as $service)
                        <p><a class="btn btn-sm btn-outline-success mb-1 mr-2" href="{{ route('services.edit',$service->id) }}" title="Editar informações do gabinete"><i class="fas fa-cog"></i></a> Editar informações do gabinete <strong>{{$service->initial}}</strong><br></p>
                    @endforeach

                    {{-- <p class="mt-5"><a class="btn btn-sm btn-outline-danger mb-1 mr-2" href="{{ route('backupdatabase') }}" title="Fazer um backup da base de dados"><i class="fas fa-database"></i></a> Cópia de segurança da base de dados<br></p> --}}
                    <br><p><a class="btn btn-sm btn-outline-danger mb-1 mr-2" href="{{ route('resetpage') }}" title="Repor valores de origem"><i class="fas fa-recycle"></i></a> Repor valores de origem<br></p>

                    @else
                    <a class="btn btn-sm btn-outline-success mb-1 mr-2" href="{{ route('services.edit',Auth::User()->service_id) }}" title="Editar informações do gabinete"><i class="fas fa-cog"></i></a> Editar informações do gabinete <strong>{{Auth::user()->serviceToStr()}}</strong><br>
                    @endif


                    </div>

                    {{-- Resumo --}}
                    <div class="tab-pane fade p-2" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <p>{{Auth::user()->name}} utiliza esta plataforma com o perfil de {{Auth::user()->roleToStr()}}, e tem permissão para:</p>

                        @if (Auth::user()->service_id == null)
                            <ul>
                                <li>Criar novos eventos / divulgaçoes para <strong>qualquer gabinete</strong></li>
                                <li>Editar / modificar quais quer eventos e divulgaçoes já existentes</li>
                                <li>Criar / modificar / eliminar contas de utilizadores</li>
                                <li>Gerir as listas dos públicos alvos</li>
                                <li>Gerir os tipos de divulgações</li>
                                <li>Consultar as estatísticas referentes a todos os gabinetes</li>
                                <li>Criar um backup da base de dados</li>
                                <li>Repor os dados de origem da base de dados</li>
                            </ul>

                        @else
                            <ul>
                                <li>Criar novos eventos / divulgaçoes para o gabinete <strong>{{Auth::user()->serviceToStr()}}</strong></strong></li>
                                <li>Editar / modificar quais quer eventos e divulgaçoes já existentes</li>
                                <li>Criar e enviar newsletters em nome do gabinete <strong>{{Auth::user()->serviceToStr()}}</strong></li>
                                <li>Gerir as listas dos públicos alvos</li>
                                <li>Gerir os tipos de divulgações</li>
                                <li>Consultar as estatísticas referentes ao gabinete <strong>{{Auth::user()->serviceToStr()}}</strong></li>
                                <li>Personalizar a sua imagem de apresentação</strong></li>
                            </ul>

                        @endif

                        <a class="btn btn-sm btn-outline-primary rounded-circle mr-2" href="{{route('faqs')}}" title="Editar informações do gabinete"><i class="fas fa-question"></i></a> Para mais informações poderá consultar os tópicos de ajuda<br>
                    </div>

                    {{-- Histórico de actividade --}}
                    <div class="tab-pane fade p-2" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                        <div class="table-responsive " style="overflow:hidden;">
                            <table class="table table-bordered" id="dataTable" style="width:100%" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th class="text-center"># ID</th>
                                        <th>Tipo de publicação</th>
                                        <th>Titulo</th>
                                        <th>Gabinete</th>
                                        <th>Data de criação</th>
                                        <th class="text-center">Opções</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($posts as $post)
                                    <tr>
                                        {{-- ID do post --}}
                                        <td class="align-middle text-center">
                                            {{ $post->id}}
                                        </td>

                                        {{-- Tipo de publicação --}}
                                        <td class="align-middle">
                                            {{ $post->type}} @if ($post->deleted_at!=null)<span class="text-danger">(eliminado)</span>@endif
                                        </td class="align-middle">


                                        {{-- Titulo do post --}}
                                        <td class="align-middle">
                                            {{ $post->title}}
                                        </td>

                                        {{-- Gabinete --}}
                                        <td class="align-middle">
                                            @if ($post->service_id==null)
                                            Não atribuido
                                            @elseif ($post->service_id==1)
                                            GAI&D
                                            @elseif ($post->service_id==2)
                                            GIRE
                                            @else
                                            Internacional
                                            @endif
                                        </td>

                                        {{-- Data de criação --}}
                                        <td class="align-middle">
                                            {{ $post->created_at}}
                                        </td>

                                        {{-- Opções --}}
                                        <td class="text-center align-middle">
                                            @if ($post->deleted_at!=null)
                                                <span class="text-danger">(eliminado)</span>
                                            @else
                                                <a class="btn btn-sm btn-outline-primary mb-1" href="{{ route('posts.show',$post->id) }}" title="Ver publicação"><i class="fas fa-eye"></i></a>
                                                <a class="btn btn-sm btn-outline-warning mb-1" href="{{ route('posts.edit',$post->id) }}" title="Editar informações"><i class="fas fa-pen"></i></a>
                                            @endif

                                        </td>


                                    </tr>

                                    @endforeach



                                    @foreach($newsletters as $newsletter)
                                    <tr>
                                        {{-- ID do post --}}
                                        <td class="align-middle text-center">
                                            {{$newsletter->id}}
                                        </td>

                                        {{-- Tipo de publicação --}}
                                        <td class="align-middle">
                                            Newsletter @if ($newsletter->deleted_at!=null)<span class="text-danger">(eliminado)</span>@endif
                                        </td>

                                        {{-- Titulo --}}
                                        <td class="align-middle">
                                            {{$newsletter->subject}}
                                        </td>

                                        {{-- Gabinete --}}
                                        <td class="align-middle">
                                            @if ($newsletter->service_id==null)
                                            Não atribuido
                                            @elseif ($newsletter->service_id==1)
                                            GAI&D
                                            @elseif ($newsletter->service_id==2)
                                            GIRE
                                            @else
                                            Internacional
                                            @endif
                                        </td>


                                        {{-- Data de criação --}}
                                        <td class="align-middle">
                                            {{ $newsletter->created_at}}
                                        </td>


                                        {{-- Opções --}}
                                        <td class="align-middle text-center">

                                        @if ($newsletter->deleted_at!=null)
                                            <span class="text-danger">(eliminado)</span>
                                        @else
                                            <a class="btn btn-sm btn-outline-primary mb-1" href="{{ route('newsletters.index') }}" title="Página das Newsletters"><i class="fas fa-external-link-alt"></i></a>
                                        @endif

                                        </td>



                                    </tr>

                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>




                  </div>
            </div>

        </div>

    </div>
  </div>

@endsection


@section('scripts')

<script src="{{asset('/js/script_details_users.js')}}"></script>

@endsection
