
@extends('layout.masterbackoffice')




@section('title', 'Repor valores de origem | Definições')


@section('styleLinks')
<link href="{{asset('css/style_add_user.css')}}" rel="stylesheet">
@endsection

@section('content')


<div class="d-sm-flex align-items-center justify-content-between mb-4">
    <h1 class="h3 mb-0 text-gray-800">Repor valores de origem</h1>
    <a href="javascript:history.go(-1)" class="btn btn-sm btn-primary shadow-sm mr-2"><i class="fas fa-chevron-left text-white-50 mr-2"></i>Página anterior</a>
</div>


 <div class="card shadow">

    <div class="card-body ">

        <div class="alert alert-danger mb-4" role="alert">
           <i class="fas fa-exclamation-triangle mr-3"></i><strong>Atenção:</strong> Esta ação é irreversivel. Ao repor os valores de origem irá perder os todos dados registados atualmente.
          </div>


         {{--  Repor cópia de segurança --}}
          <div class="row">

                 {{-- Repor dados de origem --}}
            <div class="col">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                      <h6 class="m-0 font-weight-bold text-primary">Repor dados de origem</h6>
                    </div>
                    <div class="card-body">
                        <p>Antes de utilizar esta função, é recomendado fazer um backup da base de dados, pois todos os dados serão perdidos.</p>
                        <p style="line-height:28px">
                        - Publicações / eventos <br>
                        - Registo de envio das Newsletters <br>
                        - Rascunhos <br>
                        - Tipos de divulgações personalizados <br>
                        - Publicações / eventos <br>
                        - Listas de emails de publico alvo<br>
                        - Utilizadores registados (exepto administrador predefinido) <br>
                        - Lista de tarefas / notas pessoais <br>
                        - Dados estatísticos<br>
                        </p>


                    <div class="text-right">
                    <button type="button" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#exampleModal">
                        <i class="fas fa-recycle mr-2"></i>Repor dados de origem
                    </button>
                    </div>

                </div>
                </div>
            </div>


{{--              <div class="col col-md-6">
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                      <h6 class="m-0 font-weight-bold text-primary">Repor cópia de segurança</h6>
                    </div>
                    <div class="card-body">

                        <p>Esta opção permite repor os dados através de um ficheiro de cópia de segurança criado anteriormente.</p>

                        <p>- Selecione o ficheiro da cópia de segurança</p>

                        <div class="input-group" >
                            <div class="custom-file">
                              <input type="file" class="custom-file-input" id="inputGroupFile02">
                              <label class="custom-file-label" for="inputGroupFile02" aria-describedby="inputGroupFileAddon02">Escolher ficheiro</label>
                            </div>
                          </div>

                          <div class="text-right">
                          <a class="mt-4 btn btn-sm btn-primary text-white">Repor cópia de segurança</a>
                        </div>

                    </div>
                  </div>
            </div> --}}


        </div>


  </div>
 </div>


<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Repor valores de origem</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <p>Tem a certeza que quer repor os valores de origem?</p>
          <i class="fas fa-exclamation-triangle mr-2"></i><strong>Aviso: Esta ação é irreversivel. Todos os dados guardados atualmente serão perdidos!</strong>
        </div>
        <div class="modal-footer">
          <a class="btn btn-sm btn-danger" href="{{route('resetdatabase')}}">Sim! Repor valores de origem</a>
          <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Cancelar</button>
        </div>
      </div>
    </div>
  </div>


@endsection

{{-- Repor através de backup |  Repor através de backup --}}
