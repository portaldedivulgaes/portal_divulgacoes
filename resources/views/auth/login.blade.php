<!DOCTYPE html>
<html lang="pt-PT">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Login | Backoffice | Portal de Divulgações</title>

  <!-- Favicon -->
  <link rel="shortcut icon" href="/img/icon_logo.png" type="image/x-icon">

  <!-- Fonts -->
  <link href="{{asset('/vendor/fontawesome-free/css/all.min.css')}}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- CSS Links -->
  <link href="{{asset('/css/sb-admin-2.css')}}" rel="stylesheet">

  @yield('styleLinks')

  <style>
    body,html{height: 100%;}
  </style>

</head>

<body class="bg-gradient-primary">

  <div class="container h-100">

    <!-- Outer Row -->
    <div class="row h-100 justify-content-center align-items-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <img style="width:75%; margin-bottom:30px;"  src="{{asset('/img/logotipo-portal-de-divulgacoes_para-fundo-preto.png')}}" alt="">
                    <h1 class="h4 text-gray-900 mb-4"></h1>
                  </div>
@if(session('error')!=null) <h6 class="text-danger" style="font-size: 15px; text-align:center;position:relative; top:4px;">{{session('error')}} </h6><br> @endif
                  <form class="user" method="post" action="{{route('login')}}">
                    @csrf
                    <div class="form-group">
                      <input id="username" type="text" class="form-control form-control-user @error('email') is-invalid @enderror" name="username" value="{{ old('username') }}" placeholder="Utilizador" required autocomplete="username" autofocus>
                    </div>
                    <div class="form-group">
                      <input id="password" type="password" class="form-control form-control-user @error('password') is-invalid @enderror" name="password" placeholder="Password" required autocomplete="current-password">
                    </div>
                    <button type="submit" class="btn btn-primary btn-user btn-block">
                      {{ __('Login') }}
                    </button>
                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>


    <!-- Bootstrap core JavaScript-->
    <script src="{{asset('/vendor/jquery/jquery.min.js')}}"></script>
    <script src="{{asset('/vendor/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

    <!-- Core plugin JavaScript-->
    <script src="{{asset('/vendor/jquery-easing/jquery.easing.min.js')}}"></script>

    <!-- Custom scripts for all pages-->
    <script src="{{asset('/js/sb-admin-2.min.js')}}"></script>

    <!-- Data Tables-->
    <script src="{{asset('/vendor/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('/vendor/datatables/dataTables.bootstrap4.min.js')}}"></script>

    <!-- Individual Scripts -->
    @yield('scripts')

  </body>

  </html>
