
@extends('layout.master')


@section('title', 'Contactos')

@section('titleHeader', 'Contactos')

<link href="{{asset('css/style_contactos.css')}}" rel="stylesheet">


@section('content')

@include('layout.partials.frontoffice.header')

<div class="container mb-5" >


    <h5 class="text-center mb-2">Nesta página encontra as informações de contacto dos gabinetes da ESTG</h5>
    <p class="text-center text-muted mb-5">Clique no gabinete pretendido para ser direcionado para o formulário de contacto</p>


    <div class="row" data-sal-duration="1000" data-sal="fade" data-sal-delay="100" data-sal-easing="ease-out-bounce">



    @foreach($services as $service)

        <div class="col-sm-12 col-md-4">
            <div class="card border-light mb-5 bg-white rounded text-center">
                <div class="card-body">
                    <h5 class="card-title">{{ $service->initial }}</h5>
                    <small class="text-muted">{{ $service->name }}</small>
                    <p class="card-text mt-3">{{ $service->description }}</p>
                </div>
                <div class="card-footer border-light">
                    <a href="#contact_form" service_id="{{ $service->id }}" class="stretched-link gabinfo">{{ $service->email }}</a>
                    <br><small>
                        @if ($service->phone!=null)
                        Telefone: {{ $service->phone }}
                        @endif
                        </small>
                </div>
            </div>
        </div>

        @endforeach

    </div>



    <div id="contact_div" class="row mt-3 tituloContacto text-center" data-sal="fade" data-sal-delay="100" data-sal-easing="ease-out-bounce">
        <div class="col">
            <h2>Contacta-nos!</h2>
            <p>Seja para alguma dúvida ou pedido de divulgação, entre em contacto com os gabinetes existentes.</p>
        </div>
    </div>



    <div class="row pt-5 row_contacto mx-auto" >


        <div class="col-sm-12 col-md-4 ">
            <iframe class="div_mapa"
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d6136.49696502719!2d-8.824750336452187!3d39.734068902835894!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xcfaf619f4450fa76!2sPolit%C3%A9cnico%20de%20Leiria%20%7C%20ESTG%20-%20Escola%20Superior%20de%20Tecnologia%20e%20Gest%C3%A3o_Edif%C3%ADcio%20D!5e0!3m2!1sen!2spt!4v1575136084164!5m2!1sen!2spt"></iframe>

        </div>



        <div class="col-sm-12 col-md-8">
          @if (!empty(session('success')))
          @include ('backoffice.partials.success')
          @endif
            <form action="{{route('contactPost')}}" method="POST" id="contact_form">
                @csrf <!-- CSRF Protection -->

                <div class="form-group col-sm">

                    <label for="gabinete">Gabinete <span class="text-danger">* </span></label>
                    <select id="gabinete" name="service" class="form-control" title="Gabinete destinatário da mensagem" required>
                        <option value="" hidden selected>Por favor escolha o gabinete</option>
                        @foreach($services as $service)
                        <option value={{ $service->id }}>{{ $service->initial }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="container">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            <label for="nome">Nome <span class="text-danger">* </span></label>
                            <input id="nome" maxlength="100" name="name" type="text" class="form-control" title="O seu nome: Apenas letras" placeholder="Insira o seu nome" required>
                        </div>

                        <div class="form-group col-sm-6">
                            <label for="email">Email <span class="text-danger">* </span></label>
                            <input id="email" maxlength="200" name="email" type="email" class="form-control" placeholder="Insira o seu endereço de email" title="O seu endereço de e-mail. Exemplo: meu_nome@servidor.pt"
                            required pattern="[\w-]+@([\w-]+\.)+[\w-]+">
                        </div>
                    </div>
                </div>

                <div class="form-group col-sm">
                    <label for="assunto">Assunto <span class="text-danger">* </span></label>
                    <input id="assunto" maxlength="200" name="subject" type="text" class="form-control" title="Insira o assundo da mensagem" placeholder="Insira o assunto da mensagem" required>
                </div>

                <div class="form-group col-sm">
                    <label for="Mensagem">Em que te podemos ajudar? <span class="text-danger">* </span></label>
                    <textarea id="Mensagem" class="form-control" rows="6" name="message" placeholder="Escreva a sua mensagem" title="Texto da mensagem para enviar" required></textarea>
                </div>

                <div class="form-group col-sm text-danger">* campos de preenchimento obrigatório</div>

                <div class="form-group text-right">
                    <button id="btn_reset" type="reset" class="btn btn-secondary mr-3"><i class="fas fa-eraser text-white-50 mr-2"></i> Limpar</button>
                    <button id="btn_submit" type="submit" name="enviar" class="btn btn-success px-5"><i class="fas fa-paper-plane mr-2"></i> Enviar</button>
                </div>
            </form>
        </div>
    </div>
</div>


<br><br>



<script src="{{asset('js/script_contactos.js')}}"></script>
<script>
    sal();
</script>

@endsection
<!-- Script JS -->
