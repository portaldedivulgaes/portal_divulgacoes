@extends('layout.master')

@section('title', 'Sobre')

@section('titleHeader', 'Sobre')

<link href="{{ asset('css/style_sobre.css') }}" rel="stylesheet">


@section('content')

    @include('layout.partials.frontoffice.header')

    {{-- 1º section of text + img social_girl--}}
    <div class="container" data-sal-duration="1000" data-sal="slide-up" data-sal-delay="100" data-sal-easing="ease-out-bounce">
        <div class="row">
            <div class="col-sm-12 col-md-7 sec1">
                <h2 class="mb-4">Todas as divulgações num só local!</h2>
                <p>Tudo o que precisas de saber acerca de divulgações e eventos que decorrem
                    na Escola Superior de Tecnologia e Gestão encontra-se aqui! Desfruta ao máximo deste portal!
                </p>
            </div>
            <div class="col-md-1 sec1div"></div>
            <div class="col-md-4 imgGirl">
                <img src="{{asset('img/social_girl_sobre.png')}}" alt="Imagem ilustrativa de uma menina a olhar para o telemóvel">
            </div>
        </div>
    </div>

    {{-- 2º section of text --}}
    <div class="bg-light backF">
        <div class="container">
            <div class="row" data-sal-duration="1000" data-sal="slide-left" data-sal-delay="100" data-sal-easing="ease-out-bounce">
                <div class="col-12 text-center">
                    <h2 class="mb-4">Um portal feito a pensar em ti!</h2>
                    <p>Tu, que és estudante, tens a vida muito ocupada? Esta solução de transmissão de informação foi
                        feita
                        a pensar principalmente em ti. Tudo o que é aqui apresentado está organizado de forma simples e
                        rápida
                        de se aceder para que nunca percas as novidades da tua escola!
                    </p>
                </div>
            </div>
        </div>
    </div>

    {{-- 3º section of text + card's --}}
    <div class="container">
        <div class="row"  data-sal-duration="1000" data-sal="slide-up" data-sal-delay="100" data-sal-easing="ease-out-bounce">
            <div class="col-12 text-center">
                <h2 class="mb-4">Gabinetes para toda a comunidade!</h2>
                <p>Os Gabinetes estão sempre à alerta para tudo o que se passa tanto na escola como no exterior, afim de
                    te
                    poderem dar informações que te possam interessar!
                </p>
            </div>
        </div>

        {{-- Card's --}}


        <div class="row mt-5 mb-5" data-sal-duration="1000" data-sal="slide-up" data-sal-delay="80" data-sal-easing="ease-out-bounce">
        @foreach ($services as $key=>$service)

        <div class="col-sm-12 col-md-4 text-center">
            <div class="card bg-light mb-4">
                <a href="{{route('divulgacoes', $service->initial)}}">
                    <img class="card-img-top" style="width:100%" src="{{asset('img/gab_'.$service->id.'.png')}}" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-text text-center">{{$service->initial}}</h5>
                    </div>
                </a>
            </div>
        </div>

        @endforeach

        </div>


    </div>

    <br><br>

    <script>
        sal();
    </script>




@endsection
