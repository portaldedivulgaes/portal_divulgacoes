<?php
use App\Service;
$services=Service::all();
?>

@extends('layout.master')



@section('title', 'Erro de servidor')


@section('content')


<link href="{{asset('css/style_errors.css')}}" rel="stylesheet">


<div class="container">

        <div class="row">
                <div class="col">
                 <a href="/"><img src="/img/logotipo-portal-de-divulgacoes_para-fundo-branco.png" alt="Lógotipo - Portal de Divulgações" class="logo404"></a>
                </div>
        </div>

        <div class="row">
                <div class="col">
                        <p class="txt_404">500</p>
                </div>
        </div>

        <div class="row">
                <div class="col">
                        <p class="txt_info1">Erro de servidor!</p>
                        <p class="txt_info2">O servidor encontrou problemas inesperados. Se o problema persistir, contacte o administrador do sistema</p>
                </div>
        </div>

        <div class="row">
                <div class="col">
                        <a href="javascript:history.go(-1)" class="btn btn-dark mt-5" role="button" aria-pressed="true"><i class="fas fa-arrow-left mr-2"></i>Voltar à página anterior</a>
                </div>
        </div>
</div>




@endsection
