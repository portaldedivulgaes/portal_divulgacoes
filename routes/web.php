<?php

use Illuminate\Contracts\Console\Kernel;

// Frontoffice -> Routes

// Index
Route::get('/', 'HomeIndexController@index')->name('index');

//Sobre
Route::get('/sobre', 'PageController@sobre')->name('sobre');

//Contactos
Route::get('/contactos', 'ContactController@contact')->name('contact');
Route::post('/contactos', 'ContactController@contactPost')->name('contactPost');

//Divulgações - Listagem
Route::get('/divulgacoes/{service}', 'PostListController@index')->name('divulgacoes');

//Divulgações - Detalhe
Route::get('/divulgacoes/{service}/divulgacao/{post}', 'PostDetailController@index')->name('divulgacao');

Route::get('/divulgacoes/svgImage/{post}', 'SvgController@getSvg')->name('divulgacoes.svg');
Route::get('/divulgacoes/svgThumb/{post}', 'SvgController@getSvgThumb')->name('divulgacoes.svgThumb');

// Backoffice -> Routes

// Auth
Route::get('/backoffice/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::get('/backoffice/autologin/{number}', 'Auth\LoginController@autoLogin')->name('autologin');
Route::get('/backoffice/logout', 'Auth\LoginController@logout')->name('logout');
Route::post('/backoffice/login', 'Auth\LoginController@postLogin');


Route::group(['prefix' => 'backoffice', 'middleware' => ['auth','PreventBackButton']], function () {

    //Home
    Route::get('/', 'HomeController@index')->name('home');

    //Notes
    Route::resource('/notes', 'NoteController')->only([
        'store', 'update', 'destroy'
    ]);

    //Statistics
    Route::resource('/statistics', 'StatisticsController');

    //Users
    Route::get('/users//restore/{user}', 'UserController@restore')->name('users.restore');
    Route::get('/users/restore', 'UserController@show_trashed')->name('users.trashed');
    Route::resource('/users', 'UserController');

    //Services: EDIT info
    Route::resource('/services', 'ServicesController');

    //FAQs
    Route::get('/faqs', 'FaqsController@show')->name('faqs');

    //Options
    Route::get('/options/reset', 'OptionsController@show_reset')->name('resetpage');


    //Reset à base de dados : migrate:refresh
    Route::get('/options/resetdata', function () {
        $exitCode = Artisan::call('migrate:refresh');
        return redirect()->route('options', Auth::user())->with('success', 'Valores de origem repostos com sucesso');
    })->name('resetdatabase');

/*     //backup database. php artisan backup:mysql-dump
    Route::get('/options/backup/makebackup', function () {
        $exitCode = Artisan::call('backup:mysql-dump');
        return redirect()->route('options', Auth::user())->with('success', 'Cópia de segurança da base de dados efectuada com sucesso');
    })->name('makebackup'); */





    Route::get('/options/backup', 'OptionsController@show_backup')->name('backupdatabase');
    Route::get('/options/{user}', 'OptionsController@index')->name('options');


    //Drafts
    Route::resource('/drafts', 'DraftController');

    // Newsletter
    Route::get('/newsletters', 'NewsletterController@index')->name('newsletters.index');
    Route::get('/newsletters/create', 'NewsletterController@create')->name('newsletters.create');
    Route::post('/newsletters/preview', 'NewsletterController@preview')->name('newsletters.preview');
    Route::post('/newsletters/store', 'NewsletterController@store')->name('newsletters.store');
    Route::delete('/newsletters/{newsletter}', 'NewsletterController@destroy')->name('newsletters.destroy');


    //Drafts
    Route::resource('/drafts', 'DraftController')->only(['index', 'show', 'edit', 'destroy']);

    //Events
    Route::resource('/events', 'EventController')->only([
        'index', 'show', 'edit', 'destroy'
    ]);

    //Posts
    Route::get('/posts/create/{tipo}','PostController@create')->name('posts.create');
    Route::resource('/posts', 'PostController')->except(['create']);

    //Tasks
    Route::resource('/tasks', 'TaskController')->only([
        'index', 'store', 'update', 'destroy'
    ]);

    //SubTasks
    Route::resource('/subtasks', 'SubTaskController')->only([
        'index', 'store', 'update', 'destroy'
    ]);
    Route::put('/updatesubtaskstatusdone/{id}',[
      'as' => 'subtasks.updateStatusDone',
      'uses' => 'SubTaskController@updateStatusDone'
    ]);
    Route::put('/updatesubtaskstatustodo/{id}',[
      'as' => 'subtasks.updateStatusTodo',
      'uses' => 'SubTaskController@updateStatusTodo'
    ]);

    //EventTypes
    Route::resource('/eventtypes', 'EventTypeController')->only([
        'index', 'store', 'update', 'destroy'
    ]);

    //TargetAudience
    Route::resource('/targetaudiences', 'TargetAudienceController')->only([
        'index', 'store', 'update', 'destroy'
    ]);
    Route::get('/getEmails', 'TargetAudienceController@getEmails');

    Route::put('/notifications/markAsRead/{id}', 'NotificationController@markAsRead');
});
